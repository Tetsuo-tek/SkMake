#include "skmakeconstructor.h"
#include "Core/System/Filesystem/skfileinfoslist.h"
#include "Core/System/Time/skdatetime.h"

ConstructorImpl(SkMakeConstructor, SkObject)
{
    makefile = new SkFile(this);
}

bool SkMakeConstructor::setup(CStr *filePath)
{
    SkString s = filePath;

    SkFileInfo f;
    SkFsUtils::fillFileInfo(s.c_str(), f);

    if (!f.exists)
    {
        ObjectError("SkMake file NOT found: " << s);
        return false;
    }

    skMakePathDir = f.absoluteParentPath;
    SkFsUtils::chdir(skMakePathDir.c_str());

    setObjectName(f.name.c_str());

    if (!skMakeConfig.fromFile(f.completeAbsolutePath.c_str()))
    {
        ObjectError("SkMake file has errors: " << s);
        return false;
    }

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = skMakeConfig.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();

        if (k == "target")
            target = itr->item().value().data();

        else if (k == "buildPath")
            buildPath = itr->item().value().data();

        else if (k == "buildOptions")
            itr->item().value().copyToStringList(buildOptions);

        else if (k == "sourcesPaths")
            itr->item().value().copyToStringList(sourcePaths);

        else if (k == "dynamicDeps")
            itr->item().value().copyToStringList(dynDeps);

        else if (k == "includeOptPaths")
            itr->item().value().copyToStringList(includeOptPaths);

        else if (k == "defines")
            itr->item().value().copyToStringList(defines);

        else if (k == "libs")
            itr->item().value().copyToStringList(libs);
    }

    delete itr;

    for(uint64_t i=0; i<sourcePaths.count(); i++)
    {
        SkFileInfo f;
        SkFsUtils::fillFileInfo(sourcePaths[i].c_str(), f);

        if (!f.exists)
        {
            ObjectError("sourcePath NOT found: " << sourcePaths[i]);
            return false;
        }

        sourcePaths[i] = f.completeAbsolutePath;
        sourcePaths[i] = SkFsUtils::adjustPathEndSeparator(sourcePaths[i].c_str());
    }

    return true;
}

bool SkMakeConstructor::build()
{
    SkElapsedTime chrono;
    chrono.start();

    SkFsUtils::chdir(skMakePathDir.c_str());

    SkString absSrcPath;

    if (!checkSourcePath("main.cpp", absSrcPath))
    {
        ObjectError("'main.cpp' NOT found (aborting)");
        return false;
    }

    SkCodeFile *src = new SkCodeFile;
    SkFsUtils::fillFileInfo(absSrcPath.c_str(), *src, true);

    for(uint64_t i=0; i<dynDeps.count(); i++)
        if (!checkInclude(src, dynDeps[i].c_str()))
        {
            ObjectError("Dynamic dependencies NOT found (aborting): " << dynDeps[i]);
            return false;
        }

    stack.push(src);

    while(!stack.isEmpty())
        if (!parseAndGrabHeaders())
            return false;

    //sources.reverse();
    SkFsUtils::chdir(skMakePathDir.c_str());

    writeMakeFile();

    ObjectMessage("Makefile successfully created [" << chrono.stop() << " seconds]");
    return true;
}

bool SkMakeConstructor::parseAndGrabHeaders()
{
    SkCodeFile *current = stack.pop();

    if (!sources.contains(current->checkSum))
    {
        sources[current->checkSum] = current;

        if (current->suffix == "cpp" || current->suffix == "c")
        {
            if (!buildPath.isEmpty())
            {
                current->o = buildPath;
                current->o.append("/");
                current->o.append(current->name);
                current->o.append(".o");
            }
            else 
            {
                current->o = current->name;
                current->o.append(".o");
            }

            objects << current->o;
        }

        ObjectMessage("INSERT " << sources.count() << ": [" << current->checkSum << "] -> " << current->completeAbsolutePath);
    }

    else
    {
        ObjectWarning("Already inserted (discarding): " << current->completeAbsolutePath);
        return true;
    }

    SkFsUtils::chdir(current->absoluteParentPath.c_str());

    SkString code;
    SkFsUtils::readTEXT(current->completeAbsolutePath.c_str(), code);

    SkStringList splitted;
    code.split("\n", splitted);

    for(uint64_t i=0; i<splitted.count(); i++)
    {
        SkString &s = splitted[i];

        s.trim();

        if (!s.isEmpty() && s.c_str()[0] == '#' && s.length() > 12 && s.startsWith("#include "))
            if (!parseIncludeLine(current, s, i+1))
                return false;
    }

    if (current->suffix == "h" || current->suffix == "hh")
    {
        SkString tmp = current->absoluteParentPath;
        tmp.append("/");
        tmp.append(current->name);
        tmp.append(".");

        SkString tmpCPP = tmp;
        tmpCPP.append("cpp");

        SkString tmpC = tmp;
        tmpC.append("c");

        if (!SkFsUtils::exists(tmpCPP.c_str()) && !SkFsUtils::exists(tmpC.c_str()))
            return true;

        SkCodeFile *src = new SkCodeFile;

        src->dependencies = current->dependencies;

        if (SkFsUtils::exists(tmpCPP.c_str()))
            SkFsUtils::fillFileInfo(tmpCPP.c_str(), *src, true);

        else if (SkFsUtils::exists(tmpC.c_str()))
            SkFsUtils::fillFileInfo(tmpC.c_str(), *src, true);

        stack.push(src);
    }

    return true;
}

bool SkMakeConstructor::parseIncludeLine(SkCodeFile *current, SkString &include, uint64_t line)
{
    SkStringList splitted;
    include.split("#include ", splitted);

    if (splitted.count() < 2)
    {
        ObjectError("Code error on line "<< line << " on file: " << current->completeAbsolutePath);
        return false;
    }

    splitted[1].chop(1);
    CStr *relSrcPath = &splitted[1].c_str()[1];

    if (checkInclude(current, relSrcPath))
        ObjectMessage("Header CHECKED: " << include);

    else
        ObjectWarning("File NOT found (discarding): " << include);

    return true;
}

bool SkMakeConstructor::checkInclude(SkCodeFile *current, CStr *includePath)
{
    SkString absSrcPath;

    if (checkSourcePath(includePath, absSrcPath))
    {
        SkCodeFile *src = new SkCodeFile;
        SkFsUtils::fillFileInfo(absSrcPath.c_str(), *src, true);
        current->dependencies << src;
        stack.push(src);
        return true;
    }

    return false;
}

bool SkMakeConstructor::checkSourcePath(CStr *relSrcPath, SkString &absSrcPath)
{
    if (SkFsUtils::exists(relSrcPath))
    {
        absSrcPath = SkFsUtils::pwd();
        absSrcPath.append(relSrcPath);
        return true;
    }

    for(uint64_t i=0; i<sourcePaths.count(); i++)
    {
        absSrcPath = sourcePaths[i].c_str();
        absSrcPath.append(relSrcPath);

        if (SkFsUtils::exists(absSrcPath.c_str()))
            return true;
    }

    absSrcPath.clear();
    return false;
}

void SkMakeConstructor::writeMakeFile()
{
    if (SkFsUtils::exists("Makefile") && !SkFsUtils::rm("Makefile"))
    {
        ObjectError("Cannot remove last Makefile");
        exit(1);
    }

    makefile->setFilePath("Makefile");

    if (!makefile->open(SkFileMode::FLM_ONLYWRITE))
    {
        ObjectWarning("Cannot create Makefile (aborting)");
        exit(1);
    }

    writeLine("################################################################################");

    SkString tmp = "# Makefile for building: ";
    tmp.append(target);
    writeLine(tmp.c_str());

    tmp = "# GMT-Time: ";
    tmp.append(SkDateTime::currentDateTimeISOString(true));
    writeLine(tmp.c_str());

    writeLine("################################################################################");
    writeEmptyLine();

    writePairLine("TARGET", target.c_str());
    writePairLine("CXX", skMakeConfig["compiler"].data());
    writePairLine("LINK", skMakeConfig["compiler"].data());
    writePairLine("BUILD_PATH", buildPath.c_str());

    tmp = defines.join(" ");
    writePairLine("DEFINES", tmp.c_str());

    tmp = buildOptions.join(" ");
    tmp.append(" $(DEFINES)");
    writePairLine("CXXFLAGS", tmp.c_str());

    writePairLine("DEL_FILE", "rm -vf");
    writePairLine("DEL_DIR", "rmdir -v");
    writePairLine("CHK_DIR_EXISTS", "test -d");
    writePairLine("MKDIR", "mkdir -p");
    writePairLine("COPY_FILE", "cp -vf");
    writePairLine("COPY_DIR", "cp -vf -R");
    writePairLine("INSTALL_FILE", "install -m 644 -vp");
    writePairLine("INSTALL_PROGRAM", "install -m 755 -vp");
    writePairLine("SYMLINK", "ln -vf -s");
    writePairLine("MOVE", "mv -vf");
    writePairLine("TAR", "tar -cvf");
    writePairLine("COMPRESS", "gzip -9f");
    writePairLine("SED", "sed");
    writePairLine("STRIP", "strip");

    tmp.clear();

    for(uint64_t i=0; i<sourcePaths.count(); i++)
    {
        tmp.append("-I");
        tmp.append(sourcePaths[i]);
        tmp.append(" ");
    }

    tmp.append(includeOptPaths.join(" "));
    writePairLine("INCPATH", tmp.c_str());

    tmp = libs.join(" ");
    writePairLine("LIBS", tmp.c_str());

    tmp = "OBJECTS = \\\n		";
    objects.reverse();
    tmp.append(objects.join(" \\\n		"));
    writeLine(tmp.c_str());

    writeEmptyLine();

    tmp = "all: Makefile ";
    tmp.append(target);
    writeLine(tmp.c_str());

    writeEmptyLine();

    tmp = "clean: \n\t-$(DEL_FILE) $(OBJECTS)";
    writeLine(tmp.c_str());

    tmp = "distclean: clean \n\t-$(DEL_FILE) $(OBJECTS) \n\t-$(DEL_FILE) $(TARGET) \n\t-$(DEL_FILE) ./Makefile";
    writeLine(tmp.c_str());

    writeEmptyLine();

    tmp = target;
    tmp.append(": $(OBJECTS)\n\t$(LINK) $(OBJECTS) $(LIBS) -o $(TARGET)");
    writeLine(tmp.c_str());

    SkBinaryTreeVisit<SkString, SkCodeFile *> *itr = sources.iterator();

    while(itr->next())
    {
        SkCodeFile *src = itr->item().value();

        if (src->suffix == "cpp" || src->suffix == "c")
        {
            writeEmptyLine();

            tmp = src->o;
            tmp.append(": \\\n		");
            tmp.append(src->completeAbsolutePath);

            SkAbstractListIterator<SkCodeFile *> *itr = src->dependencies.iterator();

            while(itr->next())
            {
                SkCodeFile *dep = itr->item();
                tmp.append(" \\\n		");
                tmp.append(dep->completeAbsolutePath);
            }

            delete itr;

            writeLine(tmp.c_str());

            tmp = "\t@mkdir -p $(dir $@)\n";
            tmp.append("\t$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ");
            tmp.append(src->o);
            tmp.append(" ");
            tmp.append(src->completeAbsolutePath);

            writeLine(tmp.c_str());
        }
    }

    delete itr;

    writeEmptyLine();

    makefile->close();
}

void SkMakeConstructor::writeLine(CStr *s)
{
    SkString str(s);
    str.append("\n");
    makefile->write(str);
}

void SkMakeConstructor::writePairLine(CStr *k, CStr *v)
{
    SkString str(k);
    str.append(" = ");
    str.append(v);
    writeLine(str.c_str());
}

void SkMakeConstructor::writeEmptyLine()
{
    writeLine("");
}

SkArgsMap &SkMakeConstructor::skMake()
{
    return skMakeConfig;
}
