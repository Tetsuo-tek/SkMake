#ifndef SKMAKEAPP_H
#define SKMAKEAPP_H

#include <Core/App/skapp.h>
#include "skmakeconstructor.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkMakeApp extends SkObject
{
    public:
        Constructor(SkMakeApp, SkObject);

        Slot(init);
        Slot(exitFromKernelSignal);

    private:
        SkCli *cli;
        SkString skSrcPath;

        uint8_t indicatorSteps;
        int lastIndicatorSize;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        SkTreeMap<SkString, unsigned> templatesIndexes;
        void grabTemplates();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        bool checkForMakefileGeneration();

        bool checkForFsToHeaderGeneration();
        void generateFsToHeaderItem(SkAbstractDevice *in, SkFile *out, bool isBinGen, SkFileInfo &info, ULong index, ULong total, int level);

        bool printTemplatesList(CStr *lsModeCliArg);
        bool printTemplateFile();
        bool printTemplateFilesList();

        bool checkForCppClassGeneration();
        bool checkForCppProgramGeneration();

        bool checkForPyClassGeneration();
        bool checkForPyProgramGeneration();
        bool checkForPySketchGeneration();

        void checkOutputDirPath(CStr *outputItemName, SkString &outputDirPath);
        void checkOutputItemName(CStr *itemName, SkString &outputItemName);
        void buildFromTemplate(CStr *templatePath, CStr *templateFileName, SkVector<SkPair<SkString, SkString> > *vars, SkVector<SkPair<SkString, SkString> > *blocks, SkString &output);
        void writeOutputTextFile(CStr *outputDirPath, CStr *name, CStr *suffix, SkString &content);
        void copyTemplateDir(CStr *templatePath, CStr *outputDirPath);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        void indicatorClear();
        void indicatorNextStep(CStr *msg);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        //void buildCLI(SkStringList &acceptedKeys);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKMAKEAPP_H
