void checkIrReceiver_$ADDON_NAME()
{
    if (!isStreamActive)
        return;

    if (irRecvTimer_$ADDON_NAME.stop() >= IRRECV_CHECKMS_$ADDON_NAME)
    {
        decode_results results;

        if (irRecvAddon_$ADDON_NAME->decode(&results))
        {
            if (results.value != 0xFFFFFFFF)
            {
                float val = results.value;

                MANAGER->getCustomFloats()->get(irRecvSubID_$ADDON_NAME)->lastValue = 0;
                MANAGER->getCustomFloats()->setValue(irRecvSubID_$ADDON_NAME, val);

#if defined(MCU_LOGDBG)
                MCU_LOGDEV.print("IR-CMD selected: ");
                MCU_LOGDEV.println(results.value, HEX);
#endif
            }

            irRecvAddon_$ADDON_NAME->resume(); // Receive the next value
        }

        irRecvTimer_$ADDON_NAME.start();
    }
}
