void initIrReceiver_$ADDON_NAME()
{
    irRecvAddon_$ADDON_NAME = new IRrecv($DATAPIN_$ADDON_NAME);
    irRecvAddon_$ADDON_NAME->enableIRIn();
    irRecvTimer_$ADDON_NAME.start();
    irRecvSubID_$ADDON_NAME = MANAGER->addCustomFLOAT_FLOW(SatFlowModes::FM_INPUT true);
}
