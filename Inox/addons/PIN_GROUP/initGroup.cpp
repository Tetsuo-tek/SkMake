void initGroup_$ADDON_NAME()
{
    group_$ADDON_NAME = new GPIO;

    for(int i=0; i<GROUPCOUNT$ADDON_NAME; i++)
    {

#if GROUPPINTYPE_$ADDON_NAME == T_DGT
        group_$ADDON_NAME->add_DGT(groupPinIDs_$ADDON_NAME[i], GROUPPINMODE_$ADDON_NAME);
#endif

#if GROUPPINTYPE_$ADDON_NAME == T_ADC
        group_$ADDON_NAME->add_ADC(groupPinIDs_$ADDON_NAME[i]);
#endif

#if GROUPPINTYPE_$ADDON_NAME == T_DAC
        group_$ADDON_NAME->add_DAC(groupPinIDs_$ADDON_NAME[i]);
#endif

#if GROUPPINTYPE_$ADDON_NAME == T_PWM
        group_$ADDON_NAME->add_PWM(groupPinIDs_$ADDON_NAME[i]);
#endif

    }

#if (GROUPPINMODE_$ADDON_NAME == INPUT)
    groupSubID_$ADDON_NAME = MANAGER->addGroup_FLOW(group_$ADDON_NAME, GROUP_CHECKMS_$ADDON_NAME);
#else
    groupSubID_$ADDON_NAME = MANAGER->addGroup_FLOW(group_$ADDON_NAME);
#endif
}
