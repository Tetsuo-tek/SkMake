void checkDht_$ADDON_NAME()
{
    if (!isStreamActive)
        return;

    if (dhtTimer_$ADDON_NAME.stop() >= DHT_CHECKMS_$ADDON_NAME)
    {
        float h = dhtAddon_$ADDON_NAME->readHumidity();
        float t = dhtAddon_$ADDON_NAME->readTemperature();

        if (isnan(h) || isnan(t))
        {
#if defined(MCU_LOGDBG)
            MCU_LOGDEV.println("Failed to read from DHT sensor!");
#endif
        }

        else
        {
            float hic = dhtAddon_$ADDON_NAME->computeHeatIndex(t, h, false);

            MANAGER->getCustomFloats()->setValue(dhtTempSubID_$ADDON_NAME, t);
            MANAGER->getCustomFloats()->setValue(dhtHumSubID_$ADDON_NAME, h);
            MANAGER->getCustomFloats()->setValue(dhtHicSubID_$ADDON_NAME, hic);

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print("T:");
            MCU_LOGDEV.print(t);
            MCU_LOGDEV.print(" °C");

            MCU_LOGDEV.print(" - H:");
            MCU_LOGDEV.print(h);
            MCU_LOGDEV.print(" %");

            MCU_LOGDEV.print(" - HIC:");
            MCU_LOGDEV.print(hic);
            MCU_LOGDEV.println(" °C");
#endif
        }

        dhtTimer_$ADDON_NAME.start();
    }
}
