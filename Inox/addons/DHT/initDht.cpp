void initDht_$ADDON_NAME()
{
    dhtAddon_$ADDON_NAME = new DHT(DATAPIN_$ADDON_NAME, DHTTYPE_$ADDON_NAME);
    dhtAddon_$ADDON_NAME->begin();

    dhtTempSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtHumSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtHicSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtTimer_$ADDON_NAME.start();
}
