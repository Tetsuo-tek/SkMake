void initSingle_$ADDON_NAME()
{
#if SINGLEPINTYPE_$ADDON_NAME == T_DGT
    singleSubID_$ADDON_NAME =
            MANAGER->addSingleDGT_FLOW(DATAPIN_$ADDON_NAME,
                                       SINGLEPINMODE_$ADDON_NAME,
                                       SINGLE_DGTINVERT_$ADDON_NAME);

#if defined(SINGLE_SENDPULSEONHIGH_$ADDON_NAME)
    pulseSubID_$ADDON_NAME = MANAGER->addCustomPULSE_FLOW(SatFlowModes::FM_INPUT);
#endif

#endif

#if SINGLEPINTYPE_$ADDON_NAME == T_ADC
    singleSubID_$ADDON_NAME = MANAGER->addSingleADC_FLOW(DATAPIN_$ADDON_NAME);
#if defined(SINGLE_AVERAGE_$ADDON_NAME)
    singleAdcAverage_$ADDON_NAME = new IntAverage(SINGLE_AVERAGE_SAMPLES_$ADDON_NAME,
                                                  SINGLE_AVERAGE_TOLLERANCE_$ADDON_NAME);
#endif
#endif

#if SINGLEPINTYPE_$ADDON_NAME == T_DAC
    singleSubID_$ADDON_NAME = MANAGER->addSingleDAC_FLOW(DATAPIN_$ADDON_NAME);
#endif

#if SINGLEPINTYPE_$ADDON_NAME == T_PWM
    singleSubID_$ADDON_NAME = MANAGER->addSinglePWM_FLOW(DATAPIN_$ADDON_NAME);
#endif

#if (SINGLEPINMODE_$ADDON_NAME == INPUT)
    singleTimer_$ADDON_NAME.start();
#endif
}
