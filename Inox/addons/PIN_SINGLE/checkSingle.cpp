void checkSingle_$ADDON_NAME()
{
    if (!isStreamActive)
        return;

    if (singleTimer_$ADDON_NAME.stop() >= SINGLE_CHECKMS_$ADDON_NAME)
    {
        SatSinglePinProps *single = MANAGER->getSingles()->get(singleSubID_$ADDON_NAME);

#if defined(SINGLE_AVERAGE_$ADDON_NAME)
        GPIO::readPin(single);
        singleAdcAverage_$ADDON_NAME->addValue(single->lastValue, singleAvgIsReady_$ADDON_NAME);

        if (singleAvgIsReady_$ADDON_NAME)
        {
            long long avg = singleAdcAverage_$ADDON_NAME->getAverage();

            if (avg != singleLastAvg_$ADDON_NAME)
            {
                singleLastAvg_$ADDON_NAME = avg;
                MANAGER->getSingles()->get(singleSubID_$ADDON_NAME)->sending = true;
            }

            singleAvgIsReady_$ADDON_NAME = false;
        }
#else
        GPIOWORD lastValue = single->lastValue;
        GPIO::readPin(single);

        if (lastValue != single->lastValue)
        {
            if (single->lastValue == HIGH)
                MANAGER->getCustomPulses()->sendPulse(pulseSubID_$ADDON_NAME);

            MANAGER->getSingles()->get(singleSubID_$ADDON_NAME)->sending = true;
        }
#endif

        singleTimer_$ADDON_NAME.start();
    }
}
