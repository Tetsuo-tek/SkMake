void checkRTC()
{
    if (!isStreamActive)
        return;

    if (isRtcAlarmed)
    {
        long long epoch = rtc.getEpoch();

        MANAGER->getCustomInt64s()->setValue(rtc_SubID, epoch);

        dateTime.set(rtc.getDay(),
                     rtc.getMonth(),
                     rtc.getYear(),
                     rtc.getHours(),
                     rtc.getMinutes(),
                     rtc.getSeconds());

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print("RTC-PULSED: ");
        MCU_LOGDEV.print(dateTime.toISO());
        MCU_LOGDEV.print("; Epoch: ");
        MCU_LOGDEV.println((unsigned long) epoch);
#endif

        isRtcAlarmed = false;
    }

#if defined(RTC_SECONDS_PULSE_ENABLED)
    if (rtcSecondsPulseTimer.stop() >= 1000)
    {
        MANAGER->getCustomPulses()->sendPulse(rtcPulse_SubID);
        rtcSecondsPulseTimer.start();
    }
#endif
}
