void initRTC()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing SAMD-RTC ..");
#endif

    rtc.begin(true);

#if defined(RTCNTP_ENABLED)
    unsigned long epoch = 0;
    NtpRequest ntp;

    tries = 0;

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("Setting up date-time from NTP service [");
    MCU_LOGDEV.print(ntpSvrIP);
    MCU_LOGDEV.print(":");
    MCU_LOGDEV.print(NTPSVRPORT);
    MCU_LOGDEV.print("->");
    MCU_LOGDEV.print(NTPLOCPORT);
    MCU_LOGDEV.println("] ..");
#endif

    while(!ntp.makeNtpRequest(ntpSvrIP, NTPSVRPORT, NTPLOCPORT, epoch) && tries < 100)
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print('.');
#endif
        tries++;
        delay(1000);
    }

#if defined(MCU_LOGDBG)
    if (tries)
        MCU_LOGDEV.println();
#endif

    tries = 0;

    if (epoch)
    {
        rtc.setEpoch(epoch);
        dateTime.set(rtc.getDay(), rtc.getMonth(), rtc.getYear(), rtc.getHours(), rtc.getMinutes(), rtc.getSeconds());

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print("RTC TIME INITIALIZED: ");
        MCU_LOGDEV.println(dateTime.toISO());
#endif
        unixTime = epoch;
    }

#if defined(MCU_LOGDBG)
    else
        MCU_LOGDEV.println("Cannot get time from NTP");
#endif

#endif

//PULSEON_MIN
#if RTCALARM_MODE == 0
    rtc.setAlarmSeconds(0);
    rtc.enableAlarm(rtc.MATCH_SS);
#endif

//PULSEON_HOUR
#if RTCALARM_MODE == 1
    rtc.setAlarmMinutes(0);
    rtc.enableAlarm(rtc.MATCH_MMSS);
#endif

//PULSEON_DAY
#if RTCALARM_MODE == 2
    rtc.setAlarmHours(0);
    rtc.enableAlarm(rtc.MATCH_HHMMSS);
#endif

//PULSEON_MONTH
#if RTCALARM_MODE == 3
    rtc.enableAlarm(rtc.MATCH_DHHMMSS);
#endif

//PULSEON_YEAR
#if RTCALARM_MODE == 4
    rtc.enableAlarm(rtc.MATCH_MMDDHHMMSS);
#endif

    rtc.attachInterrupt(rtcInterruption);
    isRtcAlarmed = true;

    rtc_SubID = MANAGER->addCustomINT64_FLOW(SatFlowModes::FM_INPUT);

#if defined(RTC_SECONDS_PULSE_ENABLED)
    rtcPulse_SubID = MANAGER->addCustomPULSE_FLOW(SatFlowModes::FM_INPUT);
    rtcSecondsPulseTimer.start();
#endif
}
