void initInterrupt_$ADDON_NAME()
{
    interrupt_$ADDON_NAME.setup(DATAPIN_$ADDON_NAME,
                                INTMODE_$ADDON_NAME,
                                DEBOUNCE_$ADDON_NAME);

    ATTACH(&interrupt_$ADDON_NAME, INTID_$ADDON_NAME);

    MANAGER->addInterrupt_FLOW(&interrupt_$ADDON_NAME);
}
