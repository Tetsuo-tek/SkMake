void initDht_$ADDON_NAME()
{
    dhtAddon_$ADDON_NAME = new DHTesp;
    dhtAddon_$ADDON_NAME->setup(DATAPIN_$ADDON_NAME, DHTesp::DHTTYPE_$ADDON_NAME);

    dhtTempSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtHumSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtHicSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                DHT_ONLYONCHANGE_$ADDON_NAME);

    dhtTimer_$ADDON_NAME.start();
}
