void initDS_$ADDON_NAME()
{
    dallas_$ADDON_NAME = new OneWire(DATAPIN_$ADDON_NAME);
    dsTimer_$ADDON_NAME.start();
    dsSubID_$ADDON_NAME = MANAGER->addCustomFLOAT_FLOW(SatFlowModes::FM_INPUT, DS_ONLYONCHANGE_$ADDON_NAME);
}
