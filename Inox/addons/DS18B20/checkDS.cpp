void checkDS_$ADDON_NAME()
{
    if (!isStreamActive)
        return;

    if (dsTimer_$ADDON_NAME.stop() >= (DS_CHECKMS_$ADDON_NAME - 1250))
    {
        float temperature = getDallasTemperature(dallas_$ADDON_NAME);

        if (temperature == -2000)
        {
          dsTimer_$ADDON_NAME.start();
          return;
        }

        MANAGER->getCustomFloats()
                ->setValue(dsSubID_$ADDON_NAME, temperature);

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print("T:");
        MCU_LOGDEV.print(temperature);
        MCU_LOGDEV.println(" °C");
#endif

        dsTimer_$ADDON_NAME.start();
    }
}
