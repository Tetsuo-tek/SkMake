float getDallasTemperature(OneWire *dallas)
{
    byte addr[8];

    if (!dallas->search(addr))
    {

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("Sensor dallas-18x20 NOT found!");
#endif

        dallas->reset_search();
        delay(250);
        return -2000;
    }

/*#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("DS ROM =");

    for(int i = 0; i < 8; i++)
    {
        MCU_LOGDEV.print(' ');
        MCU_LOGDEV.print(addr[i], HEX);
    }

    MCU_LOGDEV.println();
#endif*/

    if (OneWire::crc8(addr, 7) != addr[7])
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("CRC is not valid!");
#endif
        return -2000;
    }

    byte type_s;

    if (addr[0] == 0x10)
    {
        type_s = 1;

/*#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("  Chip = DS-18S20");  // or old DS1820
#endif*/
    }

    else if (addr[0] == 0x28)
    {
        type_s = 0;

/*#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("  Chip = DS-18B20");
#endif*/
    }

    else if (addr[0] == 0x22)
    {
        type_s = 0;

/*#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("  Chip = DS-1822");
#endif*/
    }

    else
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("Device is not a DS-18x20 family device.");
#endif
        return -2000;
    }

    dallas->reset();
    dallas->select(addr);
    dallas->write(0x44, 1);        // start conversion, with parasite power on at the end

    delay(1000);     // maybe 750ms is enough, maybe not
    // we might do a dallas->depower() here, but the reset will take care of it.

    dallas->reset();
    dallas->select(addr);
    dallas->write(0xBE);         // Read Scratchpad

    byte data[12];

    for (int i = 0; i < 9; i++)// we need 9 bytes
        data[i] = dallas->read();

    // Convert the data to actual temperature
    // because the result is a 16 bit signed integer, it should
    // be stored to an "int16_t" type, which is always 16 bits
    // even when compiled on a 32 bit processor.
    int16_t raw = (data[1] << 8) | data[0];

    if (type_s)
    {
      raw = raw << 3; // 9 bit resolution default

      if (data[7] == 0x10)
      {
        // "count remain" gives full 12 bit resolution
        raw = (raw & 0xFFF0) + 12 - data[6];
      }
    }

    else
    {
      byte cfg = (data[4] & 0x60);
      // at lower res, the low bits are undefined, so let's zero them

      if (cfg == 0x00)
        raw = raw & ~7;  // 9 bit resolution, 93.75 ms

      else if (cfg == 0x20)
        raw = raw & ~3; // 10 bit res, 187.5 ms

      else if (cfg == 0x40)
        raw = raw & ~1; // 11 bit res, 375 ms

      //default is 12 bit resolution, 750 ms conversion time
    }

    dallas->reset_search();
    delay(250);

    return (float) raw / 16.0;
}
