void checkUltrasonic_$ADDON_NAME()
{
    if (!isStreamActive)
        return;

    if (ultrasonicTimer_$ADDON_NAME.stop() >= ULTRASONIC_CHECKMS_$ADDON_NAME)
    {
        // Clears the trigPin
        digitalWrite(TRIGGERPIN_$ADDON_NAME, LOW);

        // Sets the trigPin on HIGH state for 10 micro seconds
        digitalWrite(TRIGGERPIN_$ADDON_NAME, HIGH);
        delayMicroseconds(10);
        digitalWrite(TRIGGERPIN_$ADDON_NAME, LOW);

        // Reads the echoPin, returns the sound wave travel time in microseconds
        unsigned long duration = pulseIn(ECHOPIN_$ADDON_NAME, HIGH);

        if (duration > 40000)
        {
#if defined(MCU_LOGDBG)
            MCU_LOGDEV.println("Distance is NOT valid");
#endif
            ultrasonicTimer_$ADDON_NAME.start();
            return;
        }

        // Calculating the distance
        //0,0343 cm/micros (at 20 °C)
        short distance = (duration * 0.034 / 2);

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print("Distance: ");
        MCU_LOGDEV.print(distance);
        MCU_LOGDEV.println(" cm");
        MCU_LOGDEV.print("Ping duration: ");
        MCU_LOGDEV.print(duration);
        MCU_LOGDEV.println(" us");
#endif

        //MANAGER->getCustomFloats()->setValue(ultrasonicSubID_$ADDON_NAME, distance);
        MANAGER->getCustomShorts()->setValue(ultrasonicSubID_$ADDON_NAME, distance);

        ultrasonicTimer_$ADDON_NAME.start();
    }
}
