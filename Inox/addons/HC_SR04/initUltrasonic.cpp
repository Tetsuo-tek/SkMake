void initUltrasonic_$ADDON_NAME()
{
    pinMode(TRIGGERPIN_$ADDON_NAME, OUTPUT);
    pinMode(ECHOPIN_$ADDON_NAME, INPUT);

    ultrasonicTimer_$ADDON_NAME.start();

    /*ultrasonicSubID_$ADDON_NAME =
            MANAGER->addCustomFLOAT_FLOW(SatFlowModes::FM_INPUT,
                                         ULTRASONIC_ONLYONCHANGE_$ADDON_NAME);*/
    ultrasonicSubID_$ADDON_NAME =
                MANAGER->addCustomSHORT_FLOW(SatFlowModes::FM_INPUT,
                                             ULTRASONIC_ONLYONCHANGE_$ADDON_NAME);
}
