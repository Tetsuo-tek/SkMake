void initManager()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.begin(MCU_LOGTTYSPEED);

#if defined(MCU_LOGTTYWAIT)
    while(!MCU_LOGDEV);
#endif

    MCU_LOGDEV.println();
    MCU_LOGDEV.println();
    MCU_LOGDEV.println();
    MCU_LOGDEV.println();

    MCU_LOGDEV.print("Booting SatManager [Model: ");
    MCU_LOGDEV.print("$MODEL");
    MCU_LOGDEV.print("; Arch: ");
    MCU_LOGDEV.print("$ARCH");
#if defined(ARDUINO_BOARD)
    MCU_LOGDEV.print("] -> ");
    MCU_LOGDEV.println(ARDUINO_BOARD);
#else
    MCU_LOGDEV.println("]");
#endif

    MCU_LOGDEV.print("GPIO word size: ");
    MCU_LOGDEV.print((int) sizeof(GPIOWORD));
    MCU_LOGDEV.println(" B");
    MCU_LOGDEV.print("Memory usage: ");
    MCU_LOGDEV.print(RAM_SZ-freeMemory());
    MCU_LOGDEV.print(" / ");
    MCU_LOGDEV.print(RAM_SZ);
    MCU_LOGDEV.println(" B");

#endif

    MANAGER = new SatManager;
    MANAGER->setup(&STREAM, SATNAME, SATPASSWD);

#if defined(MCU_ADC_MODE)
    analogReadResolution(MCU_ADC_MODE);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("ADC-RES: ");
    MCU_LOGDEV.print((int) MCU_ADC_MODE);
    MCU_LOGDEV.println(" b");
#endif

#endif

#if defined(MCU_DAC_MODE)
    analogWriteResolution(MCU_DAC_MODE);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("DAC-RES: ");
    MCU_LOGDEV.print((int) MCU_DAC_MODE);
    MCU_LOGDEV.println(" b");
#endif

#endif

#if defined(MCU_ANALOGREF_MODE)
    analogReference(MCU_ANALOGREF_MODE);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("AREF-MODE: ");
    MCU_LOGDEV.println("$AREF_MODE");
#endif

#endif

#if defined(SYSMEM_ENABLED)
    MANAGER->enableSystemMem_FLOW(true, SYSMEM_CHECKMS);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("SysMem is ON [int: ");
    MCU_LOGDEV.print((int) SYSMEM_CHECKMS);
    MCU_LOGDEV.println(" ms]");
#endif

#endif

#if defined(SYSTICK_ENABLED)
    MANAGER->enableSystemTick_FLOW(true, SYSTICK_CHECKMS);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("SysTick is ON [int: ");
    MCU_LOGDEV.print((int) SYSTICK_CHECKMS);
    MCU_LOGDEV.println(" ms]");
#endif

#endif

#if defined(SATEVENTS)
    onSetup();
#endif

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("SatManager STARTED");
    MCU_LOGDEV.print("SkName: ");
    MCU_LOGDEV.println("$SKNAME");
    MCU_LOGDEV.print("Build-DateTime: ");
    MCU_LOGDEV.println("$BUILD_DATETIME");
#endif
}
