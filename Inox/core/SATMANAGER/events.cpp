#if defined(SATEVENTS)

void onSetup()
{
    //INSERT YOUR CUSTOM-CODE HERE
}

void onLogin()
{
    //INSERT YOUR CUSTOM-CODE HERE
}

void onStart()
{
    //INSERT YOUR CUSTOM-CODE HERE
}

void onLoop()
{
    //INSERT YOUR CUSTOM-CODE HERE
}

//WHEN FLOW-DATA COME FROM NETWORK
void onDataCome(SatFlowTypes type, byte flowID, byte subIndex)
{
    //INSERT YOUR CUSTOM-CODE HERE
}

//WHEN UPLOADING FLOW-DATA ON NETWORK
void onDataSent(SatFlowTypes type, byte flowID, byte subIndex)
{
    //INSERT YOUR CUSTOM-CODE HERE
}

//WHEN RAW-DATA COME FROM NETWORK
void onStreamDataCome(int bytes)
{
    //INSERT YOUR CUSTOM-CODE HERE
}

//WHEN UPLOADING RAW-DATA ON NETWORK
void onStreamDataSent(int bytes)
{
    //INSERT YOUR CUSTOM-CODE HERE
}

#endif

