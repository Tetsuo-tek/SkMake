void initAddress()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Using fixed tcp address");
#endif

    ipAddr = new IPAddress(IPADDR);

#if !defined(GWADDR)
    WiFi.config(*ipAddr);
    return;
#endif

#if defined(GWADDR)
    gwAddr = new IPAddress(GWADDR);
#if !defined(NETMASK)
    WiFi.config(*ipAddr, *gwAddr);
    return;
#endif
#endif

#if defined(NETMASK)
    netmask = new IPAddress(NETMASK);
#if !defined(DNSADDR1)
    WiFi.config(*ipAddr, *gwAddr, *netmask);
    return;
#endif
#endif

#if defined(DNSADDR1)
    dnsAddr1 = new IPAddress(DNSADDR1);
#if !defined(DNSADDR2)
    WiFi.config(*ipAddr, *gwAddr, *netmask, *dnsAddr1);
    return;
#endif
#endif

#if defined(DNSADDR2)
    dnsAddr2 = new IPAddress(DNSADDR2);
    WiFi.config(*ipAddr, *gwAddr, *netmask, *dnsAddr1, *dnsAddr2);
#endif
}
