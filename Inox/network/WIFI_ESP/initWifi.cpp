void initWifi()
{
    /*if (WiFi.status() == WL_CONNECTED)
        WiFi.end();*/

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing WIFI-ESP driver");

    MCU_LOGDEV.println("WiFi available networks:");
    int n = WiFi.scanNetworks();
    MCU_LOGDEV.println("Scan done");

    if (n == 0)
        MCU_LOGDEV.println("WiFi Networks NOT FOUND");

    else
    {
        MCU_LOGDEV.print("WiFi Networks FOUND [");
        MCU_LOGDEV.print((int) n);
        MCU_LOGDEV.println(']');
        for (int i = 0; i<n; ++i)
        {
            MCU_LOGDEV.print(i + 1);
            MCU_LOGDEV.print(": ");
            String ssid = WiFi.SSID(i);
            MCU_LOGDEV.print(ssid);
            if (ssid == ESSID)
                MCU_LOGDEV.print(" [*]");
            MCU_LOGDEV.print(" (");
            MCU_LOGDEV.print(WiFi.RSSI(i));
            MCU_LOGDEV.print(" db)");
            MCU_LOGDEV.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : " - ENCRYPTED");
            delay(10);
        }
    }

#endif

    delay(1000);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("Entering on WiFi network: ");
    MCU_LOGDEV.println(ESSID);
#endif

#if defined(DHCP_ENABLED)
    initDhcp();
#else
    initAddress();
#endif

    WiFi.mode(WIFI_STA);
    WiFi.begin(ESSID, WIFIPASS);

    while (WiFi.status() != WL_CONNECTED)
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print('.');
#endif
        delay(1000);
        tries++;
    }

    tries = 0;

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("WiFi network READY");
    MCU_LOGDEV.print("WiFi Signal: ");
    MCU_LOGDEV.print(WiFi.RSSI());
    MCU_LOGDEV.println(" db");
    MCU_LOGDEV.print("ESSID: ");
    MCU_LOGDEV.println(WiFi.SSID());
    MCU_LOGDEV.print("LocalIP: ");
    MCU_LOGDEV.println(WiFi.localIP());
    MCU_LOGDEV.print("EspSDK version: ");
    MCU_LOGDEV.println(system_get_sdk_version());
#endif

    streamCheckTimer.start();
}
