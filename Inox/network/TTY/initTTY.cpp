void initTTY()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing Hardware-SatTTY driver");
#endif

    TTY_STREAM.begin(TTY_STREAMSPEED);

#if defined(TTY_STREAMWAIT)
    while (!TTY_STREAM);
#endif

    isStreamActive = true;
}
