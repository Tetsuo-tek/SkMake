void checkSMS()
{
    if (!isStreamActive)
        return;

#if defined(SMSRECV_ENABLED)
    if (smsRecvCheckTimer.stop() >= SMSRECV_CHECKMS)
    {
        if (sms.available())
        {
            String smsPack;

            {
                char sourceNumber[20];
                memset(sourceNumber, '\0', 20);
                sms.remoteNumber(sourceNumber, 20);

#if defined(MCU_LOGDBG)
                MCU_LOGDEV.print("SMS FROM: ");
                MCU_LOGDEV.println(sourceNumber);
#endif
                smsPack.concat((const char *) sourceNumber);
                smsPack.concat("\n");
            }

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print("SMS CONTENT: ");
#endif

            int c = 0;

            while (true)
            {
                c = sms.read();

                if (c == -1)
                    break;

                else
                {

#if defined(MCU_LOGDBG)
                MCU_LOGDEV.print((char) c);
#endif
                smsPack.concat((char) c);
                }
            }

            sms.flush();

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.println("\nEND");
            MCU_LOGDEV.println("SMS DELETED");
#endif

            const byte *data = (const byte *) smsPack.c_str();
            unsigned short sz = smsPack.length();

            MANAGER->getCustomArrays()->setCustomArray(smsRecvSubID, data, sz);

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print("CONTENT SENT TO FN: ");
            MCU_LOGDEV.print(sz);
            MCU_LOGDEV.println(" B");
#endif
        }

        smsRecvCheckTimer.start();
    }
#endif

#if defined(SMSSEND_ENABLED)
    if (MANAGER->getCustomArrays()->get(smsSendSubID)->ready)
    {
        unsigned short sz = 0;
        const char *data = (const char *) MANAGER->getCustomArrays()->getLastCustomArray(smsSendSubID, sz);

#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print("CONTENT RECEIVED FROM FN: ");
        MCU_LOGDEV.print(sz);
        MCU_LOGDEV.println(" B");

        MCU_LOGDEV.print("DATA: ");
        MCU_LOGDEV.print(data);
        MCU_LOGDEV.println("\nEND");
#endif

        char targetNumber[20];
        memset(targetNumber, '\0', 20);

        int i=0;

        for(i=0; i<sz; i++)
        {
            if (i>19)
            {
#if defined(MCU_LOGDBG)
                MCU_LOGDEV.println("TARGET NUMBER OVERFLOW [>19 chars]; ABORTING!");
#endif
                return;
            }

            else if (data[i] == '\n')
            {

#if defined(MCU_LOGDBG)
                MCU_LOGDEV.print("TARGET NUMBER GRABBED: ");
                MCU_LOGDEV.println(targetNumber);
#endif
                break;
            }

            else
                targetNumber[i] = data[i];
        }

        sms.beginSMS(targetNumber);
        sms.print(&data[i+1]);
        sms.endSMS();

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print("SMS SENT TO TARGET: ");
            MCU_LOGDEV.print(sz - i - 2);
            MCU_LOGDEV.println(" B");
#endif
    }
#endif
}
