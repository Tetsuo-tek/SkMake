void checkLocation()
{
    if (!isStreamActive)
        return;

    if (locationTimer.stop() >= LOCATION_CHECKMS)
    {
        if (location.available())
        {
            float latitude = location.latitude();
            float longitude = location.longitude();
            float altitude = location.altitude();
            float accuracy = location.accuracy();

            MANAGER->getCustomFloats()->setValue(locationLatitudeSubID, latitude);
            MANAGER->getCustomFloats()->setValue(locationLongitudeSubID, longitude);
            MANAGER->getCustomFloats()->setValue(locationAltitudeSubID, altitude);
            MANAGER->getCustomFloats()->setValue(locationAccuracySubID, accuracy);

#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print("Location: ");
            MCU_LOGDEV.print(latitude, 3);
            MCU_LOGDEV.print(", ");
            MCU_LOGDEV.println(longitude, 3);

            MCU_LOGDEV.print("Altitude: ");
            MCU_LOGDEV.print(altitude);
            MCU_LOGDEV.println("m");

            MCU_LOGDEV.print("Accuracy: +/- ");
            MCU_LOGDEV.print(accuracy);
            MCU_LOGDEV.println("m");
#endif
        }

        locationTimer.start();
    }
}
