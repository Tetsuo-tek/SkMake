{
    "name" : "MKRGSM-NETWORK",
    "desc" : "GSM SARAU201 network-module specific for MKRGSM-1400",

    "type" : "NETWORK-CORE",

    "architectures" : [
        "SAMD"
    ],

    "globalDefines" : {
        "MCU_HASGSM" : "",
        "UDPSOCK_CLASS" : "GSMUDP"
    },

    "localDefines" : {
        "STREAM" : "client"
    },

    "includes" : [
        "MKRGSM.h"
    ],

    "globals" : [
        "GSMClient client",
        "GPRS gprs",
        "GSM gsmAccess",
        "IPAddress svrIP( SVRIP )",
        "uint16_t svrPort = SVRPORT",
        "bool isStreamActive = false",
        "ElapsedTimeMillis streamCheckTimer",
        "unsigned long int tries = 0",
        "bool disconnectionNotyfied = false"
    ],

    "functions" : [
        {
            "header" : "void initGSM()",
            "file" : "initGSM.cpp"
        },
        {
            "header" : "void checkGSM()",
            "file" : "checkGSM.cpp"
        }
    ],

    "sketchCalls" : {
        "setup" : "initGSM()",
        "loop" : "checkGSM()"
    },

    "requires" : [
        {
            "label" : "PIN for the SIM card",
            "defName" : "PIN_NUMBER",
            "type" : "const char *"
        },

        {
            "label" : "GSM APN",
            "defName" : "GPRS_APN",
            "type" : "const char *"
        },

        {
            "label" : "GSM userName",
            "defName" : "GPRS_LOGIN",
            "type" : "const char *",
            "canBeEmpty" : true
        },

        {
            "label" : "GSM password",
            "defName" : "GPRS_PASSWORD",
            "type" : "const char *",
            "canBeEmpty" : true
        },

        {
            "label" : "Server IP",
            "defName" : "SVRIP",
            "type" : "ipv4Address"
        },

        {
            "label" : "Server port",
            "type" : "uint16_t",
            "defName" : "SVRPORT",
            "default" : 9999
        },

        {
            "label" : "Enable GSM-LOCATION",
            "defName" : "LOCATION_ENABLED",
            "type" : "bool",
            "default" : true,
            "canBeUndefined" : true,

            "conditions" : {
                "true" : {
                    "globals" : [
                        "GSMLocation location",
                        "byte locationLatitudeSubID = 0",
                        "byte locationLongitudeSubID = 0",
                        "byte locationAltitudeSubID = 0",
                        "byte locationAccuracySubID = 0",
                        "ElapsedTimeMillis locationTimer"
                    ],

                    "functions" : [
                        {
                            "header" : "void initLocation()",
                            "file" : "initLocation.cpp"
                        },

                        {
                            "header" : "void checkLocation()",
                            "file" : "checkLocation.cpp"
                        }
                    ],

                    "requires" : [
                        {
                            "label" : "Location grab check interval",
                            "type" : "uint32_t",
                            "defName" : "LOCATION_CHECKMS",
                            "default" : 5000,
                            "udm" : "ms"
                        },

                        {
                            "label" : "Only on change",
                            "defName" : "LOCATION_ONLYONCHANGE",
                            "type" : "bool",
                            "default" : true
                        }
                    ],

                    "satFlows" : [
                        {
                            "NAME" : "LATITUDE",
                            "FLOW_T" : "CUSTOMFLOAT",
                            "MODE_T" : "INPUT"
                        },

                        {
                            "NAME" : "LONGITUDE",
                            "FLOW_T" : "CUSTOMFLOAT",
                            "MODE_T" : "INPUT"
                        },

                        {
                            "NAME" : "ALTITUDE",
                            "FLOW_T" : "CUSTOMFLOAT",
                            "MODE_T" : "INPUT"
                        },

                        {
                            "NAME" : "ACCURACY",
                            "FLOW_T" : "CUSTOMFLOAT",
                            "MODE_T" : "INPUT"
                        }
                    ]
                }
            }
        },

        {
            "label" : "Enable GSM-SMS receiver",
            "defName" : "SMSRECV_ENABLED",
            "type" : "bool",
            "default" : true,
            "canBeUndefined" : true,

            "conditions" : {
                "true" : {
                    "globals" : [
                        "GSM_SMS sms",
                        "byte smsRecvSubID = 0",
                        "ElapsedTimeMillis smsRecvCheckTimer"
                    ],

                    "functions" : [
                        {
                            "header" : "void initSMS()",
                            "file" : "initSMS.cpp"
                        },

                        {
                            "header" : "void checkSMS()",
                            "file" : "checkSMS.cpp"
                        }
                    ],

                    "requires" : [
                        {
                            "label" : "SMS-Recv grab check interval",
                            "type" : "uint32_t",
                            "defName" : "SMSRECV_CHECKMS",
                            "default" : 5000,
                            "udm" : "ms"
                        }
                    ],

                    "sketchCalls" : {
                        "setup" : "initSMS()",
                        "loop" : "checkSMS()"
                    },

                    "satFlows" : [
                        {
                            "NAME" : "GSM-SMS-RECV",
                            "FLOW_T" : "CUSTOMARRAY",
                            "MODE_T" : "INPUT"
                        }
                    ]
                }
            }
        },

        {
            "label" : "Enable GSM-SMS sender",
            "defName" : "SMSSEND_ENABLED",
            "type" : "bool",
            "default" : true,
            "canBeUndefined" : true,

            "conditions" : {
                "true" : {
                    "globals" : [
                        "GSM_SMS sms",
                        "byte smsSendSubID = 0"
                    ],

                    "functions" : [
                        {
                            "header" : "void initSMS()",
                            "file" : "initSMS.cpp"
                        },

                        {
                            "header" : "void checkSMS()",
                            "file" : "checkSMS.cpp"
                        }
                    ],

                    "sketchCalls" : {
                        "setup" : "initSMS()",
                        "loop" : "checkSMS()"
                    },

                    "satFlows" : [
                        {
                            "NAME" : "GSM-SMS-SEND",
                            "FLOW_T" : "CUSTOMARRAY",
                            "MODE_T" : "OUTPUT"
                        }
                    ]
                }
            }
        },

        {
            "label" : "Network state-check interval",
            "type" : "uint32_t",
            "defName" : "STREAM_CHECKMS",
            "default" : 4000,
            "udm" : "ms"
        },

        {
            "label" : "Enable LowPowerMode",
            "defName" : "LOWPOWERMODE_ENABLED",
            "type" : "bool",
            "default" : true,
            "canBeUndefined" : true
        }
    ]
}
