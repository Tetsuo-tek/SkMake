void initLocation()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing MKRGSM driver ..");
#endif

    location.begin();

    locationLatitudeSubID =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                LOCATION_ONLYONCHANGE);

    locationLongitudeSubID =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                LOCATION_ONLYONCHANGE);

    locationAltitudeSubID =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                LOCATION_ONLYONCHANGE);

    locationAccuracySubID =
            MANAGER->addCustomFLOAT_FLOW(
                SatFlowModes::FM_INPUT,
                LOCATION_ONLYONCHANGE);

    locationTimer.start();
}
