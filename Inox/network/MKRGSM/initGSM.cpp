void initGSM()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing MKRGSM driver ..");
#endif

    //delay(1000);

    bool connected = false;

    while (!connected)
    {
        if (gsmAccess.begin(PIN_NUMBER) == GSM_READY
                && gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)
        {
            connected = true;
        }

        else
        {
#if defined(MCU_LOGDBG)
            MCU_LOGDEV.print('.');
#endif
            delay(1000);
            tries++;
        }
      }

    tries = 0;


#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("GSM network READY");
#endif

#if defined(LOCATION_ENABLED)
    initLocation();
#endif

#if defined(LOWPOWERMODE_ENABLED)
    gsmAccess.lowPowerMode();
#else
    gsmAccess.noLowPowerMode();
#endif

    streamCheckTimer.start();
}
