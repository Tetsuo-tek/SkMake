void checkGSM()
{
    if (streamCheckTimer.stop() < STREAM_CHECKMS)
        return;

#if defined(MCU_LOGDBG)
    GSMScanner scannerNetworks;
    MCU_LOGDEV.print("GSM CURR-CARRIER: ");
    MCU_LOGDEV.println(scannerNetworks.getCurrentCarrier());
    MCU_LOGDEV.print("GSM SIG-STRENGTH: ");
    MCU_LOGDEV.println(scannerNetworks.getSignalStrength());
#endif

    isStreamActive = STREAM.connected();
    streamCheckTimer.start();

    if (isStreamActive)
    {
#if defined(LOCATION_ENABLED)
        checkLocation();
#endif
        return;
    }

    STREAM.stop();

    if (MANAGER->getTicksCount() > 0 && !disconnectionNotyfied)
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("Sat DISCONNECTED! Trying to reconnect ..");
#endif
        disconnectionNotyfied = true;
        delay(1000);
    }

    if (STREAM.connect(svrIP, svrPort))
    {
        disconnectionNotyfied = false;

#if defined(MCU_LOGDBG)
        if (tries > 0)
            MCU_LOGDEV.println();
        MCU_LOGDEV.print("Sat CONNECTED to ");
        MCU_LOGDEV.print(svrIP);
        MCU_LOGDEV.print(':');
        MCU_LOGDEV.println(svrPort);
#endif

        tries = 0;
    }

    if (!STREAM.connected())
    {
        delay(1000);
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print('.');
#endif
        tries++;
    }

    MANAGER->reset();
}
