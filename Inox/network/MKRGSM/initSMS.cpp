void initSMS()
{
#if defined(SMSRECV_ENABLED)

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing SMS receiver ..");
#endif

    smsRecvSubID = MANAGER->addCustomARRAY_FLOW(SatFlowModes::FM_INPUT);

    smsRecvCheckTimer.start();
#endif

#if defined(SMSSEND_ENABLED)

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing SMS sender ..");
#endif

    smsSendSubID = MANAGER->addCustomARRAY_FLOW(SatFlowModes::FM_OUTPUT);

#endif

}
