void initAddress()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Using fixed tcp address");
#endif

    ipAddr = new IPAddress(IPADDR);

#if !defined(DNSADDR)
    WiFi.config(*ipAddr);
    return;
#endif

#if defined(DNSADDR)
    dnsAddr = new IPAddress(DNSADDR);
#if !defined(GWADDR)
    WiFi.config(*ipAddr, *dnsAddr);
    return;
#endif
#endif

#if defined(GWADDR)
    gwAddr = new IPAddress(GWADDR);
#if !defined(NETMASK)
    WiFi.config(*ipAddr, *dnsAddr, *gwAddr);
    return;
#endif
#endif

#if defined(NETMASK)
    netmask = new IPAddress(NETMASK);
    WiFi.config(*ipAddr, *dnsAddr, *gwAddr, *netmask);
#endif
}
