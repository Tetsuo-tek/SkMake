void checkWifi()
{
    if (streamCheckTimer.stop() < STREAM_CHECKMS)
        return;

    isStreamActive = STREAM.connected();
    streamCheckTimer.start();

    if (isStreamActive)
        return;

    STREAM.stop();

    if (WiFi.status() != WL_CONNECTED)
        initWifi();

    if (MANAGER->getTicksCount() > 0 && !disconnectionNotyfied)
    {
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.println("Sat DISCONNECTED! Trying to reconnect ..");
#endif
        disconnectionNotyfied = true;
        delay(1000);
    }

    if (STREAM.connect(svrIP, svrPort))
    {
        disconnectionNotyfied = false;

#if defined(MCU_LOGDBG)
        if (tries > 0)
            MCU_LOGDEV.println();
        MCU_LOGDEV.print("Sat CONNECTED to ");
        MCU_LOGDEV.print(svrIP);
        MCU_LOGDEV.print(':');
        MCU_LOGDEV.println(svrPort);
#endif

        tries = 0;
    }

    if (!STREAM.connected())
    {
        delay(1000);
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print('.');
#endif
        tries++;
    }

    MANAGER->reset();
}
