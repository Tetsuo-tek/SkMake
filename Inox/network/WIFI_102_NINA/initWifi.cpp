void initWifi()
{
    if (WiFi.status() == WL_NO_SHIELD)
    {
        MCU_LOGDEV.println("WiFi-101 NOT FOUND, HANGING UP");
        while(1);
    }

    else if (WiFi.status() == WL_CONNECTED)
        WiFi.end();

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing WIFI-102-NINA driver ..");

    MCU_LOGDEV.print("WiFi Firmware version: ");
    MCU_LOGDEV.println(WiFi.firmwareVersion());

    int n = WiFi.scanNetworks();
    MCU_LOGDEV.println("Scan available networks done");

    if (n == 0)
        MCU_LOGDEV.println("WiFi Networks NOT FOUND");

    else
    {
        MCU_LOGDEV.print("WiFi Networks FOUND: [");
        MCU_LOGDEV.print((int) n);
        MCU_LOGDEV.println(']');
        for (int i = 0; i<n; ++i)
        {
            MCU_LOGDEV.print(i + 1);
            MCU_LOGDEV.print(": ");
            String ssid = WiFi.SSID(i);
            MCU_LOGDEV.print(ssid);
            if (ssid == ESSID)
                MCU_LOGDEV.print(" [*]");
            MCU_LOGDEV.print(" (");
            MCU_LOGDEV.print(WiFi.RSSI(i));
            MCU_LOGDEV.print(" db)");
            MCU_LOGDEV.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : " - ENCRYPTED");
            delay(10);
        }
    }

#endif

    delay(1000);

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("Entering on WiFi network: ");
    MCU_LOGDEV.println(ESSID);
#endif

#if defined(DHCP_ENABLED)
    initDhcp();
#else
    initAddress();
#endif

    while (WiFi.status() != WL_CONNECTED)
    {
        WiFi.begin(ESSID, WIFIPASS);
#if defined(MCU_LOGDBG)
        MCU_LOGDEV.print('.');
#endif
        delay(1000);
        tries++;
    }

    tries = 0;

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("WiFi network READY");
    MCU_LOGDEV.print("WiFi Signal: ");
    MCU_LOGDEV.print(WiFi.RSSI());
    MCU_LOGDEV.println(" db");
    MCU_LOGDEV.print("ESSID: ");
    MCU_LOGDEV.println(WiFi.SSID());

    MCU_LOGDEV.print("Local IP: ");
    IPAddress ip = WiFi.localIP();
    MCU_LOGDEV.println(ip);
#endif

#if defined(LOWPOWERMODE_ENABLED)
    WiFi.lowPowerMode();
#else
    WiFi.noLowPowerMode();
#endif

    streamCheckTimer.start();
}
