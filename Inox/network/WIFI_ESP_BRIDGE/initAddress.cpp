void initAddress()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Using fixed tcp address");
#endif

    ipAddr = new IPAddress(IPADDR);
    WiFi.config(*ipAddr);
}
