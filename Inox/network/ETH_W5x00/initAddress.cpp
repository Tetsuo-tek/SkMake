void initAddress()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Using fixed tcp address");
#endif

    ipAddr = new IPAddress(IPADDR);

#if !defined(DNSADDR)
    Ethernet.begin(macAddr, *ipAddr);
    return;
#endif

#if defined(DNSADDR)
    dnsAddr = new IPAddress(DNSADDR);
#if !defined(GWADDR)
    Ethernet.begin(macAddr, *ipAddr, *dnsAddr);
    return;
#endif
#endif

#if defined(GWADDR)
    gwAddr = new IPAddress(GWADDR);
#if !defined(NETMASK)
    Ethernet.begin(macAddr, *ipAddr, *dnsAddr, *gwAddr);
    return;
#endif
#endif

#if defined(NETMASK)
    netmask = new IPAddress(NETMASK);
    Ethernet.begin(macAddr, *ipAddr, *dnsAddr, *gwAddr, *netmask);
#endif
}
