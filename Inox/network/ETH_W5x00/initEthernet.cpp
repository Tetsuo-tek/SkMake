void initEthernet()
{
#if defined(MCU_LOGDBG)
    MCU_LOGDEV.println("Initializing ETH-W5x00 driver");
#endif

#if defined(DHCP_ENABLED)
    initDhcp();
    dhcpMaintainerTimer.start();
#else
    initAddress();
#endif

#if defined(MCU_LOGDBG)
    MCU_LOGDEV.print("Local IP: ");
    IPAddress ip = Ethernet.localIP();
    MCU_LOGDEV.println(ip);
#endif

    streamCheckTimer.start();
}
