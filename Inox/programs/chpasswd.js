var acceptedCliKeys = null;

var csServiceName = null;
var csClient = null;
var adminName = null;
var adminPasswd = null;

var userName = null;
var oldPasswd = null;
var newPasswd = null;

var sysDB = null;

function main()
{
    buildCli();

    if (checkCliArgmentsToStart() === false)
        return;

    csClient = new RdbmsClient;

    if (csClient.connectToService(csServiceName) === true)
    {
        if (adminName === null && adminPasswd === null)
        {
            if (csClient.loginFromUsername(userName, oldPasswd) === true)
            {
                if (csClient.updateMyPassword(oldPasswd, newPasswd) === false)
                    print("Cannot change password!");
            }

            else
                print("Cannot connect the user to Cuscuta!");
        }

        else
        {
            if (performAdminLogin() === true)
            {
                if (changeUserPassword() === false)
                    print("Cannot change the password for user '" + userName + "!");
            }

            else
                print("Cannot connect as Admin to Cuscuta!");
        }

        csClient.disconnectSocket();
    }

    else
        print("Cannot connect to Cuscuta service!");
}

function buildCli()
{
    acceptedCliKeys =
            [
                "--help",
                "--cs-service",
                "--admin-name",
                "--admin-passwd",
                "--username",
                "--old-passwd",
                "--new-passwd"
            ];

    Engine.osEnv().addCliAppHelp("--help", "print this output and exit");
    Engine.osEnv().addCliAppHelp("--cs-service", "the Cuscuta service name");
    Engine.osEnv().addCliAppHelp("--admin-name", "the admin UserName (required to change on others)");
    Engine.osEnv().addCliAppHelp("--admin-passwd", "the admin Password (required to change on others)");
    Engine.osEnv().addCliAppHelp("--username", "the UserName changing passoword");
    Engine.osEnv().addCliAppHelp("--old-passwd", "the OLD user password (required only when changing by user)");
    Engine.osEnv().addCliAppHelp("--new-passwd", "the NEW user password");
}

function checkCliArgmentsToStart()
{
    if (Engine.osEnv().checkAllowedAppArguments(acceptedCliKeys) === false)
        return false;

    if (Engine.osEnv().existsAppArgument("--help") === true)
    {
        print(Engine.osEnv().cmdLineHelp());
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--cs-service") === true)
        csServiceName = Engine.osEnv().getAppArgument("--cs-service");

    else
    {
        print("The program requires: '--cs-service'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--admin-name") === true
            && Engine.osEnv().existsAppArgument("--admin-passwd") === true)
    {
        adminName = Engine.osEnv().getAppArgument("--admin-name");
        adminPasswd = Engine.osEnv().getAppArgument("--admin-passwd");
    }

    else
    {
        if (Engine.osEnv().existsAppArgument("--old-passwd") === true)
            oldPasswd = Engine.osEnv().getAppArgument("--old-passwd");

        else
        {
            print("The program requires: ''--old-passwd'!")
            return false;
        }
    }

    if (Engine.osEnv().existsAppArgument("--username") === true
            && Engine.osEnv().existsAppArgument("--new-passwd") === true)
    {
        userName = Engine.osEnv().getAppArgument("--username");
        newPasswd = Engine.osEnv().getAppArgument("--new-passwd");
    }

    else
    {
        print("The program requires: '--username' and '--new-passwd'!")
        return false;
    }

    return true;
}

function performAdminLogin()
{
    if (adminName !== null && adminPasswd !== null)
    {
        if (csClient.loginFromUsername(adminName, adminPasswd) === true
                && csClient.amISuperAdmin() === true)
        {
            return true;
        }
    }

    return false;
}

function changeUserPassword()
{
    sysDB = csClient.getDB("System");

    if (sysDB !== null)
    {
        if (sysDB.tableCount("Users", "userName = '" + userName + "'") === 0)
            print("User '" + userName + "' NOT exists!");

        else
        {
            printRaw("Enter NEW password: ");

            var userID = sysDB.returnOneValue("Users", "id", "userName = '" + userName + "'");
            var dbTable = new DbTable;

            if (dbTable.setup(sysDB, "Users") === true)
            {
                dbTable.setKey("userMd5Passwd", Engine.strUtils().getHashHex(newPasswd));
                return dbTable.upd(userID);
            }
        }
    }

    return false;
}
