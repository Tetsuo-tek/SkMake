var acceptedCliKeys = null;

var csServiceName = null;
var csClient = null;
var adminName = null;
var adminPasswd = null;

var sysDB = null;

var userName = null;
var password = null;
var name = null;
var surName = null;
var lang = null;

function main()
{
    buildCli();

    if (checkCliArgmentsToStart() === false)
        return;

    csClient = new RdbmsClient;

    if (csClient.connectToService(csServiceName))
    {
        if (performAdminLogin())
        {
            if (buildUser() === false)
                print("Cannot build the new user!");
        }

        else
            print("Cannot connect as Admin to Cuscuta!");

        csClient.disconnectSocket();
    }

    else
        print("Cannot connect to Cuscuta service!");
}

function buildCli()
{
    acceptedCliKeys =
            [
                "--help",
                "--cs-service",
                "--admin-name",
                "--admin-passwd",
                "--username",
                "--password",
                "--name",
                "--surname",
                "--lang"
            ];

    Engine.osEnv().addCliAppHelp("--help", "print this output and exit");
    Engine.osEnv().addCliAppHelp("--cs-service <SERVICE-NAME>", "the Cuscuta service name");
    Engine.osEnv().addCliAppHelp("--admin-name <USERNAME>", "the admin UserName required to connect on Cuscuta");
    Engine.osEnv().addCliAppHelp("--admin-passwd <PASSWD>", "the admin Password required to connect on Cuscuta");
    Engine.osEnv().addCliAppHelp("--username <USERNAME>", "the Username for the building account");
    Engine.osEnv().addCliAppHelp("--password <PASSWD>", "the Password for the building account");
    Engine.osEnv().addCliAppHelp("--name <NAME>", "the Name for the building account (optional)");
    Engine.osEnv().addCliAppHelp("--surname <SURNAME>", "the SurName for the building account (optional)");
    Engine.osEnv().addCliAppHelp("--lang <LANG>", "the language for the building account (optional)");
}

function checkCliArgmentsToStart()
{
    if (Engine.osEnv().checkAllowedAppArguments(acceptedCliKeys) === false)
        return false;

    if (Engine.osEnv().existsAppArgument("--help") === true)
    {
        print(Engine.osEnv().cmdLineHelp());
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--cs-service") === true)
        csServiceName = Engine.osEnv().getAppArgument("--cs-service");

    else
    {
        print("The program requires: '--cs-service'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--admin-name") === true
             && Engine.osEnv().existsAppArgument("--admin-passwd") === true)
    {
        adminName = Engine.osEnv().getAppArgument("--admin-name");
        adminPasswd = Engine.osEnv().getAppArgument("--admin-passwd");
    }

    else
    {
        print("The program requires: '--admin-name' and '--admin-passwd'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--username") === true
            && Engine.osEnv().existsAppArgument("--password") === true)
    {
        userName = Engine.osEnv().getAppArgument("--username");
        password = Engine.osEnv().getAppArgument("--password");
    }

    else
    {
        print("The program requires: '--username' and '--password'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--name") === true)
        name = Engine.osEnv().getAppArgument("--name");

    if (Engine.osEnv().existsAppArgument("--surname") === true)
        surName = Engine.osEnv().getAppArgument("--surname");

    if (Engine.osEnv().existsAppArgument("--lang") === true)
        surName = Engine.osEnv().getAppArgument("--lang");

    return true;
}

function performAdminLogin()
{
    if (adminName !== null && adminPasswd !== null)
    {
        if (csClient.loginFromUsername(adminName, adminPasswd) === true
                && csClient.amISuperAdmin() === true)
        {
            return true;
        }
    }

    return false;
}

function buildUser()
{
    sysDB = csClient.getDB("System");

    if (sysDB !== null)
    {
        if (sysDB.tableCount("Users", "userName = '" + userName + "'") > 0)
            print("User '" + userName + "' ALREADY exists!");

        else
        {
            var dbTable = new DbTable;

            if (dbTable.setup(sysDB, "Users") === true)
            {
                dbTable.setKey("userName", userName);
                dbTable.setKey("userMd5Passwd", Engine.strUtils().getHashHex(password));

                if (name === null)
                    name = userName;

                dbTable.setKey("name", name);

                if (surName === null)
                    surName = "";

                dbTable.setKey("surname", surName);

                if (lang === null)
                    lang = csClient.defaultLanguage();

                dbTable.setKey("lang", lang);
                return dbTable.ins();
            }
        }
    }

    return false;
}
