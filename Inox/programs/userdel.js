var acceptedCliKeys = null;

var csServiceName = null;
var csClient = null;
var adminName = null;
var adminPasswd = null;

var sysDB = null;

var userName = null;

function main()
{
    buildCli();

    if (checkCliArgmentsToStart() === false)
        return;

    csClient = new RdbmsClient;

    if (csClient.connectToService(csServiceName) === true)
    {
        if (performAdminLogin() === true)
        {
            if (removeUser() === false)
                print("Cannot remove user!");
        }

        else
            print("Cannot connect as Admin to Cuscuta!");

        csClient.disconnectSocket();
    }

    else
        print("Cannot connect to Cuscuta service!");
}

function buildCli()
{
    acceptedCliKeys =
            [
                "--help",
                "--cs-service",
                "--admin-name",
                "--admin-passwd",
                "--username"
            ];

    Engine.osEnv().addCliAppHelp("--help", "print this output and exit");
    Engine.osEnv().addCliAppHelp("--cs-service <SERVICE-NAME>", "the Cuscuta service name");
    Engine.osEnv().addCliAppHelp("--admin-name <USERNAME>", "the admin UserName required to connect on Cuscuta");
    Engine.osEnv().addCliAppHelp("--admin-passwd <PASSWD>", "the admin Password required to connect on Cuscuta");
    Engine.osEnv().addCliAppHelp("--username <USERNAME>", "the Username for the removing account");
}

function checkCliArgmentsToStart()
{
    if (Engine.osEnv().checkAllowedAppArguments(acceptedCliKeys) === false)
        return false;

    if (Engine.osEnv().existsAppArgument("--help") === true)
    {
        print(Engine.osEnv().cmdLineHelp());
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--cs-service") === true)
        csServiceName = Engine.osEnv().getAppArgument("--cs-service");

    else
    {
        print("The program requires: '--cs-service'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--admin-name") === true
             && Engine.osEnv().existsAppArgument("--admin-passwd") === true)
    {
        adminName = Engine.osEnv().getAppArgument("--admin-name");
        adminPasswd = Engine.osEnv().getAppArgument("--admin-passwd");
    }

    else
    {
        print("The program requires: '--admin-name' and '--admin-passwd'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--username") === true)
        userName = Engine.osEnv().getAppArgument("--username");

    else
    {
        print("The program requires: '--username'!")
        return false;
    }

    return true;
}

function performAdminLogin()
{
    if (adminName !== null && adminPasswd !== null)
    {
        if (csClient.loginFromUsername(adminName, adminPasswd) === true
                && csClient.amISuperAdmin() === true)
        {
            return true;
        }
    }

    return false;
}

function removeUser()
{
    sysDB = csClient.getDB("System");

    if (sysDB !== null)
    {
        if (sysDB.tableCount("Users", "userName = '" + userName + "'") === 0)
            print("User '" + userName + "' NOT exists!");

        else
        {
            var userID = sysDB.returnOneValue("Users", "id", "userName = '" + userName + "'");
            var dbTable = new DbTable;

            if (dbTable.setup(sysDB, "Users") === true)
                return dbTable.del(userID);
        }
    }

    return false;
}
