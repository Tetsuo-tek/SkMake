var acceptedCliKeys = null;

var srcDir = null;
var projectDirs = null;

function main()
{
    buildCli();

    if (checkCliArgmentsToStart() === false)
        return;

    for(var i=0; i<projectDirs.length; i++)
    {
        var prjDir = srcDir + Engine.fsUtils().adjustPathEndSeparator(projectDirs[i]);

        if (qMake(prjDir) === false)
            return;

        if (make(prjDir) === false)
            return;
    }
}

function buildCli()
{
    acceptedCliKeys =
            [
                "--help",
                "--src-dir",
                "--projects",
                "--cpu-cores",
                "--install"
            ];

    Engine.osEnv().addCliAppHelp("--help", "print this output and exit");
    Engine.osEnv().addCliAppHelp("--src-dir <SRC-PATH>", "source directory containing project-dirs");
    Engine.osEnv().addCliAppHelp("--projects <PRJDIR_1,..,PRJDIR_X>", "the source project dir");
    Engine.osEnv().addCliAppHelp("--cpu-cores <OPT_1,..,OPT_X>", "the options for 'make' process (es.: -j2)");
    Engine.osEnv().addCliAppHelp("--install", "if you want install produced binary on the system (optionally)");
}

function checkCliArgmentsToStart()
{
    if (Engine.osEnv().checkAllowedAppArguments(acceptedCliKeys) === false)
        return false;

    if (Engine.osEnv().existsAppArgument("--help") === true)
    {
        print(Engine.osEnv().cmdLineHelp());
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--src-dir") === true)
        srcDir = Engine.fsUtils().adjustPathEndSeparator(Engine.osEnv().getAppArgument("--src-dir"));

    else
    {
        print("The program requires: '--src-dir'!")
        return false;
    }

    if (Engine.osEnv().existsAppArgument("--projects") === true)
        projectDirs = Engine.osEnv().getAppArgument("--projects").split(",");

    else
    {
        print("The program requires: '--projects'!")
        return false;
    }

    return true;
}

function qMake(dir)
{
    print("* PREPARING [qmake] =>", dir);
    startProcess(dir, "qmake", null);
}

function make(dir)
{
    print("* COMPILING [make] =>", dir);

    var opts = null;

    if (Engine.osEnv().existsAppArgument("--cpu-cores") === true)
        opts = [ "-j" + Engine.osEnv().getAppArgument("--cpu-cores") ];

    startProcess(dir, "make", ["clean"]);
    startProcess(dir, "make", opts);

    if (Engine.osEnv().existsAppArgument("--install") === true)
        startProcess(dir, "make", ["install", "clean"]);
}

function startProcess(workingDir, binName, options)
{
    var binProc = new BinProcess

    var mergedChannesVal = App.getCostant("QProcess::ProcessChannelMode", "MergedChannels");
    binProc.setProcessChannelMode(mergedChannesVal);

    if (binProc.setWorkingDir(workingDir) === false)
        return false;

    binProc.setProgram(binName);

    if (options === null)
        binProc.start();

    else
        binProc.start(options);

    if (!binProc.waitForStarted())
        return false;

    var procBuf = new DataBuffer;

    while(binProc.properties().state === "Running")
    {
        if (binProc.waitForReadyRead() === true)
        {
            binProc.read(procBuf);
            printRaw(procBuf.toUtf8String());
        }
    }

    return (binProc.properties().exitCode === 0);
}
