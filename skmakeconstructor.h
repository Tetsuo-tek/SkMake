#ifndef SKMAKECONSTRUCTOR_H
#define SKMAKECONSTRUCTOR_H

#include <Core/Object/skobject.h>
#include <Core/System/Filesystem/skfile.h>
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Time/skelapsedtime.h>
#include <Core/Containers/sktreemap.h>

struct SkCodeFile extends SkFileInfo
{
    SkVector<SkCodeFile *> dependencies;
    SkString o;//only on cpp files, otherwise is empty
};

class SkMakeConstructor extends SkObject
{
    public:
        Constructor(SkMakeConstructor, SkObject);

        bool setup(CStr *filePath);
        bool build();
        SkArgsMap &skMake();

    private:
        SkArgsMap skMakeConfig;
        SkString skMakePathDir;

        SkStringList sourcePaths;
        SkStringList includeOptPaths;
        SkStringList dynDeps;
        SkStringList buildOptions;
        SkStringList defines;
        SkStringList libs;

        SkTreeMap<SkString, SkCodeFile *> sources;
        SkStack<SkCodeFile *> stack;
        SkStringList objects;

        SkFile *makefile;
        SkString target;
        SkString buildPath;

        bool parseAndGrabHeaders();
        bool parseIncludeLine(SkCodeFile *current, SkString &include, uint64_t line);
        bool checkInclude(SkCodeFile *current, CStr *includePath);
        bool checkSourcePath(CStr *relSrcPath, SkString &absSrcPath);

        void writeMakeFile();
        void writeLine(CStr *s);
        void writePairLine(CStr *k, CStr *v);
        void writeEmptyLine();
};

#endif // SKMAKECONSTRUCTOR_H
