#include "skmakeapp.h"

#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfileinfoslist.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Scripting/SkCodeTemplate/sktemplatemodeler.h"

#include "templates.h"

ConstructorImpl(SkMakeApp, SkObject)
{
    cli = skApp->appCli();

    indicatorSteps = 0;
    lastIndicatorSize = 0;
    setObjectName("SkMake");

    SlotSet(init);
    SlotSet(exitFromKernelSignal);

    Attach(skApp->started_SIG, pulse, this, init, SkDirect);
    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
}

SlotImpl(SkMakeApp, init)
{
    SilentSlotArgsWarning();

    if (osEnv()->existsEnvironmentVar("SK_SRC"))
        skSrcPath = osEnv()->getEnvironmentVar("SK_SRC");

    SkCli *cli = skApp->appCli();

    bool isPrgBaseGen = cli->isUsed("--create-cpp-program");

    if (isPrgBaseGen)
    {
        if (cli->isUsed("--sk-src-path"))
            skSrcPath = cli->value("--sk-src-path").toString();

        if (skSrcPath.isEmpty())
            ObjectError("SpecialK source-path is REQUIRED, but '$SK_SRC' environment variable is NOT set and argument '--sk-src-path' is NOT found from CLI");

        AssertKiller(skSrcPath.isEmpty());
    }

    if (cli->isUsed("--print-template")
        || cli->isUsed("--create-cpp-class")
        || cli->isUsed("--create-py-sketch")
        || isPrgBaseGen)
    {
        grabTemplates();
    }

    if (checkForMakefileGeneration()
        || checkForFsToHeaderGeneration()
        || printTemplatesList("--ls-templates")
        || printTemplatesList("--ls-templates-with-paths")
        || printTemplateFile()
        || printTemplateFilesList()
        || checkForCppClassGeneration()
        || checkForCppProgramGeneration()
        || checkForPySketchGeneration())
    {
        skApp->quit();
        return;
    }

    logger->enable(true);
    ObjectWarning("No operations performed!");
    exit(1);
}

void SkMakeApp::grabTemplates()
{
    for(unsigned i=0; i<templates_count; i++)
        if (templates[i].size > 0)
        {
            SkString filePath = templates[i].relativePath;
            filePath.append(templates[i].name);
            templatesIndexes[filePath] = i;
            ObjectMessage("ACQUIRED template: " << filePath);
        }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkMakeApp::checkForMakefileGeneration()
{
    if (cli->isUsed("-m"))
    {
        SkMakeConstructor *sk = new SkMakeConstructor(this);

        AssertKiller(!sk->setup(cli->value("-m").data()));
        AssertKiller(!sk->build());

        SkArgsMap &skMake = sk->skMake();

        SkVariant &fsToH = skMake["fsToH"];

        if (!fsToH.isNull())
        {
            /*
            "fsToH" : [{
                "root" : "./templates",
                "binary" : true,
                "arrayName" : "templates",
                "out" : "./templates.h"
            }]*/

            SkVariantVector fsToHList;
            fsToH.copyToList(fsToHList);

            for(ULong i=0; i<fsToHList.count(); i++)
            {
                SkArgsMap fsToHMap;
                fsToHList[i].copyToMap(fsToHMap);

                cli->setValue("--fs-to-h", fsToHMap["root"].data());
                cli->setValue("--out-name", fsToHMap["arrayName"].data());

                if (fsToHMap.contains("excludeSuffixes"))
                {
                    SkStringList suffixes;
                    fsToHMap["excludeSuffixes"].copyToStringList(suffixes);

                    if (!suffixes.isEmpty())
                    {
                        SkString str = suffixes.join(",");
                        cli->setValue("--exclude-ext", str.c_str());
                    }
                }

                if (fsToHMap.contains("binary") && fsToHMap["binary"].toBool())
                    cli->setValue("--out-binary", "");
                else
                {
                    if (cli->isUsed("--out-binary"))
                        cli->resetValue("--out-binary");
                }

                cli->setValue("--out-file", fsToHMap["out"].data());
                checkForFsToHeaderGeneration();
            }
        }

        SkString gui = skMake["gui"].toString();

        if (!gui.isEmpty())
        {
            cli->setValue("--fs-to-h", gui.c_str());
            cli->setValue("--out-name", "gui");

            if (cli->isUsed("--out-binary"))
                cli->resetValue("--out-binary");

            cli->setValue("--out-file", "./gui.h");
            checkForFsToHeaderGeneration();
        }

        if (cli->isUsed("-c"))
        {
            int i = cli->value("-c").toInt();
            SkString mk = "make";

            if (i > 0)
            {
                mk.append(" -j");
                mk.append(cli->value("-c").toString());
            }

            system(mk.c_str());
        }

        sk->destroyLater();
        return true;
    }

    return false;
}

bool SkMakeApp::checkForFsToHeaderGeneration()
{
    if (cli->isUsed("--fs-to-h"))
    {
        /*
        const struct {
            const bool isDir;
            const char *relativePath;
            const char *fileName;
            const bool isCompressed;
            const unsigned size;
            txt:    //const char *data;
            bin:    //const unsigned char *data;

        } fs[1] = {
            {
            false,
            nullptr,
            nullptr,
            false,
            0,
            nullptr}
        };*/

        bool isBinGen = cli->isUsed("--out-binary");//osEnv()->existsAppArgument("--out-binary");

        SkString inputDirName = cli->value("--fs-to-h").toString();//osEnv()->getAppArgument("--fs-to-h");
        SkStringList directories;
        inputDirName.split(",", directories);

        SkFileInfo rootDirInfo;
        SkFsUtils::fillFileInfo(inputDirName.c_str(), rootDirInfo);

        ObjectMessage("Selected fs-directory: " << rootDirInfo.completeAbsolutePath);

        SkString fsArrName("fs");

        if (cli->isUsed("--out-name"))
            fsArrName = cli->value("--out-name").toString();

        SkString fsArrIndexName(fsArrName);
        fsArrIndexName.append("_INDEX");

        SkStringList suffixes;
        SkStringList suffixesExcluding;

        bool filterSuffix = cli->isUsed("--filter-ext");//osEnv()->existsAppArgument("--filter-ext");

        if (filterSuffix)
        {
            SkString str = cli->value("--filter-ext").toString();
            str.split(",", suffixes);
        }

        if (cli->isUsed("--exclude-ext"))
        {
            SkString str = cli->value("--exclude-ext").toString();
            str.split(",", suffixesExcluding);
        }

        SkString outputFileName = "out.h";
        SkString outputHashFileName = "out.h.md5";

        if (cli->isUsed("--out-file"))
        {
            outputFileName = cli->value("--out-file").toString();//osEnv()->getAppArgument("--out-file").c_str();
            outputHashFileName = outputFileName;
            outputHashFileName.append(".md5");
        }

        Long maxFileInputSize = 10000000;

        if (cli->isUsed("--max-content-size"))
            maxFileInputSize = cli->value("--max-content-size").toInt64();//osEnv()->getAppArgument("--max-content-size").toLongLong();

        AssertKiller(maxFileInputSize<=0);

        SkFileInfosList *l = new SkFileInfosList(this);
        SkFsUtils::ls(inputDirName.c_str(), l);

        AssertKiller(l->isEmpty());

        ObjectWarning("Checking for root-directory md5 CHECKSUM: " << inputDirName);

        l->open();

        ULong index = 0;
        SkDataBuffer row;

        while(l->next())
        {
            SkFileInfo info;
            SkFsUtils::fillFileInfo(l->currentPath().c_str(), info, true);

            if ((filterSuffix && !suffixes.contains(info.suffix)) || suffixesExcluding.contains(info.suffix) || info.size > maxFileInputSize)
                continue;

            row.append(info.completeAbsolutePath);

            if (info.isRegularFile)
                row.append(info.checkSum);

            SkString msg("[");
            msg.concat(index++);
            msg.append("/");
            msg.concat(l->count());

            if (info.isDir)
                msg.append("] Checking DIR [");

            else if (info.isRegularFile)
            {
                msg.append("] Checking FILE [");
                msg.concat(info.size);
                msg.append(" B]");
            }

            msg.append(" -> ");
            msg.append(info.completeName);
            msg.append(" FROM ");
            msg.append(info.absoluteParentPath);

            indicatorNextStep(msg.c_str());
        }

        l->close();

        SkString currentCheckSum;
        row.toStringHash(currentCheckSum);

        SkString storedCheckSum;

        if (SkFsUtils::exists(outputHashFileName.c_str()))
            SkFsUtils::readTEXT(outputHashFileName.c_str(), storedCheckSum);

        if (storedCheckSum == currentCheckSum)
        {
            indicatorClear();
            ObjectWarning("Root-directory NOT changed: " << inputDirName);
            return false;
        }

        SkString structArrDecl("const char *");
        structArrDecl.append(fsArrName);
        structArrDecl.append("_rootdir = \"");
        structArrDecl.append(rootDirInfo.completeName);
        structArrDecl.append("\";\n");

        structArrDecl.append("const unsigned ");
        structArrDecl.append(fsArrName);
        structArrDecl.append("_count = ");
        structArrDecl.concat(l->count());
        structArrDecl.append(";\n\n");

        structArrDecl.append("const struct {\n");
        structArrDecl.append("\tconst bool isDir;\n");
        structArrDecl.append("\tconst char *relativePath;\n");
        structArrDecl.append("\tconst char *name;\n");

        structArrDecl.append("\tconst unsigned size;\n");

        if (isBinGen)
        {
            structArrDecl.append("\tconst bool isCompressed;\n");
            structArrDecl.append("\tconst void *data;\n");
        }

        else
            structArrDecl.append("\tconst char *data;\n");

        structArrDecl.append("} ");
        structArrDecl.append(fsArrName);
        structArrDecl.append("[");
        structArrDecl.concat(l->count());
        structArrDecl.append("] = {\n");

        indicatorClear();
        ObjectMessage("Generating fs-to-header: " << inputDirName);

        l->open();

        SkFile *in = new SkFile(this);

        SkDataBuffer uncompressed;
        SkDataBuffer compressed;
        SkBufferDevice *inCompressedBin = nullptr;

        if (isBinGen)
            inCompressedBin = new SkBufferDevice(this);

        SkFile *out = new SkFile(this);
        out->setFilePath(outputFileName.c_str());
        AssertKiller(!out->open(SkFileMode::FLM_ONLYWRITE));

        fsArrName.toUpperCase();

        SkString ifDef("#ifndef ");
        ifDef.append(fsArrName);
        ifDef.append("_H\n");
        ifDef.append("#define ");
        ifDef.append(fsArrName);
        ifDef.append("_H\n\n");

        out->write(ifDef);

        out->write(structArrDecl);
        
        index = 0;
        SkString str;

        ULong totalSz = 0;
        ULong ignored = 0;
        ULong inserted = 0;
        ULong dirs = 0;

        SkTreeMap<SkString, SkVariant> fsArrayIndex;

        while(l->next())
        {
            SkFileInfo info;
            SkFsUtils::fillFileInfo(l->currentPath().c_str(), info);

            AssertKiller(!info.isReadable);

            out->write("\t{\n", 3);

            str = "\t\t";
            str.append(SkVariant::boolToString(info.isDir));
            str.append(",\n");

            SkString relPath = SkFsUtils::relativePath(rootDirInfo.absoluteParentPath.c_str(), info.absoluteParentPath.c_str());

            str.append("\t\t\"");
            str.append(SkFsUtils::adjustPathEndSeparator(relPath.c_str()));
            str.append("\",\n");

            str.append("\t\t\"");
            str.append(info.completeName);
            str.append("\",\n");

            SkString relCompletePath = SkFsUtils::adjustPathEndSeparator(relPath.c_str());
            relCompletePath.append(info.completeName);
            fsArrayIndex[relCompletePath] = index;

            if (info.isDir)
            {
                str.append("\t\t0,\n");

                if (isBinGen)
                    str.append("\t\tfalse,\n");

                str.append("\t\tnullptr\n");

                out->write(str);
                dirs++;
            }

            else if (info.isRegularFile && info.size > maxFileInputSize)
            {
                indicatorClear();
                ObjectWarning("Ignored file too large [limit: 1: " << info.size << " B");

                str.append("\t\t0,\n");

                if (isBinGen)
                    str.append("\t\tfalse,\n");

                str.append("\t\tnullptr\n");

                out->write(str);
                ignored++;
            }

            else if (info.isFile && !info.isSymLink)
            {
                if ((!filterSuffix || (filterSuffix && suffixes.contains(info.suffix))) && !suffixesExcluding.contains(info.suffix))
                {
                    in->setFilePath(l->currentPath().c_str());
                    AssertKiller(!in->open(SkFileMode::FLM_ONLYREAD));

                    bool requiresCompress = (isBinGen && (info.size > 10000));

                    if (requiresCompress)
                    {
                        if (!uncompressed.isEmpty())
                        {
                            uncompressed.clear();
                            compressed.clear();
                        }

                        in->readAll(&uncompressed);
                        in->close();

                        SkString msg("[");
                        msg.concat(index);
                        msg.append("/");
                        msg.concat(l->count());
                        msg.append("] Compressing FILE [");
                        msg.concat(info.size);
                        msg.append(" B] -> ");
                        msg.append(info.completeName);
                        msg.append(" FROM ");
                        msg.append(info.absoluteParentPath);

                        indicatorNextStep(msg.c_str());

                        SkDataBuffer::compress(uncompressed, compressed, 9);

                        str.append("\t\t");
                        str.concat(compressed.size());
                        str.append(",\n");

                        str.append("\t\ttrue,\n");

                        out->write(str);

                        indicatorClear();
                        ObjectMessage("ADDING [compressed: true; " << info.size << " B -> " << compressed.size() << " B]: " << relPath.c_str() << "/" << info.completeName);

                        inCompressedBin->open(compressed, BVM_ONLYREAD);
                        generateFsToHeaderItem(inCompressedBin, out, isBinGen, info, index, l->count(), 2);
                        inCompressedBin->close();

                        totalSz += compressed.size();
                    }

                    else
                    {
                        str.append("\t\t");
                        str.concat(info.size);
                        str.append(",\n");

                        if (isBinGen)
                            str.append("\t\tfalse,\n");

                        out->write(str);

                        indicatorClear();

                        if (isBinGen)
                            ObjectMessage("ADDING [compressed: false; " << info.size << " B]: " << relPath.c_str() << "/" << info.completeName);
                        else
                            ObjectMessage("ADDING [" << info.size << " B]: " << relPath.c_str() << "/" << info.completeName);

                        generateFsToHeaderItem(in, out, isBinGen, info, index, l->count(), 2);
                        in->close();

                        totalSz += info.size;
                    }

                    out->write("\n", 1);
                    inserted++;
                }

                else
                {

                    str.append("\t\t0,\n");

                    if (isBinGen)
                        str.append("\t\tfalse,\n");

                    str.append("\t\tnullptr\n");

                    out->write(str);
                    ignored++;
                }
            }

            out->write("\t}", 2);

            if (index < l->count()-1)
                out->write(",", 1);

            out->write("\n", 1);

            index++;
        }

        out->write("};\n\n", 4);

        SkVariant indexVal(fsArrayIndex);
        SkString indexJson;
        indexVal.toJson(indexJson);
        indexJson.replace("\\", "\\\\");
        indexJson.replace("\"", "\\\"");
        indexJson.replace("\r", "\\r");
        indexJson.replace("\n", "\\n");
        indexJson.append("\"");
        indexJson.prepend("\"");

        SkString indexDefine("#define ");
        indexDefine.append(fsArrName);
        indexDefine.append("_INDEX\t");
        indexDefine.append(indexJson);

        out->write(indexDefine);

        ifDef = "\n\n#endif // ";
        ifDef.append(fsArrName);
        ifDef.append("_H\n");

        out->write(ifDef);

        out->close();
        out->destroyLater();

        if (isBinGen)
            inCompressedBin->destroyLater();

        in->destroyLater();

        l->close();
        l->destroyLater();

        SkFsUtils::writeTEXT(outputHashFileName.c_str(), currentCheckSum.c_str());

        indicatorClear();
        ObjectMessage("Completed [" << inputDirName << " -> " << outputFileName << "; "
                                    << "Directories: " << dirs << "; "
                                    << "Files: " << inserted << "; "
                                    << "Ignored: " << ignored << "]: "
                                    << totalSz << " B -> " << SkFsUtils::size(outputFileName.c_str()) << " B; "
                                    << "md5: " << currentCheckSum);
        return true;
    }

    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkMakeApp::generateFsToHeaderItem(SkAbstractDevice *in, SkFile *out, bool isBinGen, SkFileInfo &info, ULong index, ULong total, int level)
{
    SkString tab;
    tab.fill('\t', level);

    if (isBinGen)
    {
        out->write(tab);
        SkString str("(const unsigned char[]){\n");
        out->write(str);

        SkString tabInternal;
        tabInternal.fill('\t', level+1);

        ULong sz = 0;
        int max = 11;
        ULong buffLen = max;
        char data[buffLen+1];
        data[buffLen] = '\0';
        uint8_t *buffer = SkArrayCast::toUInt8(data);

        char hexString[3];

        while(!in->atEof())
        {
            SkString msg("[");
            msg.concat(index);
            msg.append("/");
            msg.concat(total);
            msg.append("] Evaluating FILE [");
            msg.concat(in->pos());
            msg.append("/");
            msg.concat(in->size());
            msg.append(" B] -> ");
            msg.append(info.completeName);
            indicatorNextStep(msg.c_str());

            SkString line(tabInternal);
            buffLen = max;//CAN CHANGE
            memset(data, '\0', buffLen);

            AssertKiller(!in->read(data, buffLen));

            for(ULong i=0; i<buffLen; i++)
            {
                line.append("0x");
                sprintf(hexString, "%02X", buffer[i]);
                line.append(hexString, 2);

                if (i<buffLen-1)
                    line.append(", ");

                sz++;
            }

            if (in->atEof())
            {
                line.append("\n");
                line.append(tab);
                line.append("}");
            }
            else
                line.append(",\n");

            out->write(line);
        }
    }

    else
    {
        while(!in->atEof())
        {
            SkString msg("[");
            msg.concat(index);
            msg.append("/");
            msg.concat(total);
            msg.append("] Evaluating FILE [");
            msg.concat(in->pos());
            msg.append("/");
            msg.concat(in->size());
            msg.append(" B] -> ");
            msg.append(info.completeName);
            indicatorNextStep(msg.c_str());

            ULong lineLen = 0;

            if (in->canReadLine(lineLen))
            {
                SkString line;
                in->read(line, lineLen);

                line.replace("\\", "\\\\");
                line.replace("\"", "\\\"");
                line.replace("\r", "\\r");
                line.replace("\n", "\\n");

                line.prepend("\"");
                line.prepend(tab.c_str());
                line.append("\"");

                if (!in->atEof())
                    line.append(" \\\n");

                out->write(line);
            }
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkMakeApp::printTemplatesList(CStr *lsModeCliArg)
{
    //if (osEnv()->existsAppArgument(lsModeCliArg))
    if (cli->isUsed(lsModeCliArg))
    {
        bool withPaths = SkString::compare(lsModeCliArg, "--ls-templates-with-paths");

        SkString filter = cli->value(lsModeCliArg).toString();//osEnv()->getAppArgument(lsModeCliArg);
        bool filterResults = !filter.isEmpty();

        cout << "\nTemplates list:\n\n";

        unsigned count = 0;
        for(unsigned i=0; i<templates_count; i++)
            if (templates[i].isDir)
            {
                SkString filePath = templates[i].relativePath;

                if (!filterResults || (filterResults && filePath.startsWith(filter.c_str())))
                {
                    SkString name = templates[i].name;

                    if (name.endsWith(".template"))
                    {
                        if (withPaths)
                            cout << " - " << filePath << templates[i].name << "\n";

                        else
                            cout << " - " << name << "\n";

                        count++;
                    }
                }
            }

        cout << "\ntotal: "<< count << " elements\n\n";
        return true;
    }

    return false;
}

bool SkMakeApp::printTemplateFile()
{
    if (cli->isUsed("--print-template"))
    {
        SkString templateFile = cli->value("--print-template").toString();//osEnv()->getAppArgument("--print-template");
        AssertKiller(!templatesIndexes.contains(templateFile));
        unsigned index = templatesIndexes[templateFile];
        cout << "Content of " << templateFile << " [" << templates[index].size << " B]:\n\n" << templates[index].data << "\n";
        return true;
    }

    return false;
}

bool SkMakeApp::printTemplateFilesList()
{
    if (cli->isUsed("--ls-template-files"))
    {
        SkString templateDir = SkFsUtils::adjustPathEndSeparator(cli->value("--ls-template-files").data());
        cout << "\nComponents for template: "<< templateDir << "\n\n";

        unsigned count = 0;

        for(unsigned i=0; i<templates_count; i++)
            if (!templates[i].isDir)
            {
                SkString filePath = templates[i].relativePath;

                if (filePath.endsWith(templateDir.c_str()))
                {
                    cout << " - " << templates[i].name << "\n";
                    count++;
                }
            }


        cout << "\ntotal: "<< count << " elements\n\n";
        return true;
    }

    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkMakeApp::checkForCppClassGeneration()
{
    if (cli->isUsed("--create-cpp-class"))
    {
        SkString sketchTemplateName = cli->value("--create-cpp-class").toString();//osEnv()->getAppArgument("--create-cpp-class");
        AssertKiller(sketchTemplateName.isEmpty());

        SkString templatePath = "templates/CPP/Sk/Classes/";
        templatePath.append(SkFsUtils::adjustPathEndSeparator(sketchTemplateName.c_str()));

        SkString templateDeclPath = templatePath;
        templateDeclPath.append("decl");

        SkString templateImplPath = templatePath;
        templateImplPath.append("impl");

        AssertKiller(!templatesIndexes.contains(templateDeclPath));
        AssertKiller(!templatesIndexes.contains(templateImplPath));

        SkString className;
        checkOutputItemName(sketchTemplateName.c_str(), className);

        SkString outputDirPath;
        checkOutputDirPath(className.c_str(), outputDirPath);

        SkString upperType = className;
        upperType.toUpperCase();

        SkString lowerType = className;
        lowerType.toLowerCase();

        SkPair<SkString, SkString> pair;
        SkVector<SkPair<SkString, SkString>> vars;

        pair.set("TYPE", className);
        vars.append(pair);

        pair.set("UPPER_TYPE", upperType);
        vars.append(pair);

        pair.set("LOWER_TYPE", lowerType);
        vars.append(pair);

        // //
        /*if (osEnv()->existsAppArgument("--set-template-config"))
        {
        }*/
        // //

        SkString output;

        buildFromTemplate(templatePath.c_str(), "decl", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), lowerType.c_str(), "h", output);

        output.clear();
        buildFromTemplate(templatePath.c_str(), "impl", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), lowerType.c_str(), "cpp", output);

        return true;
    }

    return false;
}

bool SkMakeApp::checkForCppProgramGeneration()
{
    if (cli->isUsed("--create-cpp-program"))
    {
        SkString sketchTemplateName = cli->value("--create-cpp-program").toString();//osEnv()->getAppArgument("--create-cpp-program");
        AssertKiller(sketchTemplateName.isEmpty());

        SkString templatePath = "templates/CPP/Sk/Programs/";
        templatePath.append(SkFsUtils::adjustPathEndSeparator(sketchTemplateName.c_str()));

        SkString templateMainlPath = templatePath;
        templateMainlPath.append("main");

        SkString templateDeclPath = templatePath;
        templateDeclPath.append("decl");

        SkString templateImplPath = templatePath;
        templateImplPath.append("impl");

        SkString templateSkMakePath = templatePath;
        templateSkMakePath.append("skmake");

        SkString templateQtProjPath = templatePath;
        templateQtProjPath.append("qtproj");

        AssertKiller(!templatesIndexes.contains(templateMainlPath));
        AssertKiller(!templatesIndexes.contains(templateDeclPath));
        AssertKiller(!templatesIndexes.contains(templateImplPath));
        AssertKiller(!templatesIndexes.contains(templateSkMakePath));
        AssertKiller(!templatesIndexes.contains(templateQtProjPath));

        SkString className;
        checkOutputItemName(sketchTemplateName.c_str(), className);

        SkString outputDirPath;
        checkOutputDirPath(className.c_str(), outputDirPath);

        SkString upperType = className;
        upperType.toUpperCase();

        SkString lowerType = className;
        lowerType.toLowerCase();

        SkPair<SkString, SkString> pair;
        SkVector<SkPair<SkString, SkString>> vars;

        pair.set("SK_SRC", skSrcPath);
        vars.append(pair);

        pair.set("TYPE", className);
        vars.append(pair);

        pair.set("UPPER_TYPE", upperType);
        vars.append(pair);

        pair.set("LOWER_TYPE", lowerType);
        vars.append(pair);

        pair.set("BINARY_NAME", className);
        vars.append(pair);

        SkString output;

        buildFromTemplate(templatePath.c_str(), "main", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), "main", "cpp", output);

        output.clear();
        buildFromTemplate(templatePath.c_str(), "decl", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), lowerType.c_str(), "h", output);

        output.clear();
        buildFromTemplate(templatePath.c_str(), "impl", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), lowerType.c_str(), "cpp", output);

        output.clear();
        buildFromTemplate(templatePath.c_str(), "skmake", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), "skmake", "json", output);

        output.clear();
        buildFromTemplate(templatePath.c_str(), "qtproj", &vars, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), outputDirPath.c_str(), "pro", output);

        return true;
    }

    return false;
}

bool SkMakeApp::checkForPyClassGeneration()
{
    if (cli->isUsed("--create-py-class"))
    {
        SkString sketchTemplateName = cli->value("--create-py-class").toString();//osEnv()->getAppArgument("--create-py-class");
        AssertKiller(sketchTemplateName.isEmpty());

        return true;
    }

    return false;
}

bool SkMakeApp::checkForPyProgramGeneration()
{
    if (cli->isUsed("--create-py-program"))
    {
        SkString sketchTemplateName = cli->value("--create-py-program").toString();//osEnv()->getAppArgument("--create-py-program");
        AssertKiller(sketchTemplateName.isEmpty());

        return true;
    }

    return false;
}

bool SkMakeApp::checkForPySketchGeneration()
{
    if (cli->isUsed("--create-py-sketch"))
    {
        SkString sketchTemplateName = cli->value("--create-py-sketch").toString();//osEnv()->getAppArgument("--create-py-sketch");
        AssertKiller(sketchTemplateName.isEmpty());

        SkString outputDirPath;
        checkOutputDirPath(sketchTemplateName.c_str(), outputDirPath);

        SkString templatePath = "templates/PY3/Sketches/";
        templatePath.append(SkFsUtils::adjustPathEndSeparator(sketchTemplateName.c_str()));

        SkString templateFilePath = templatePath;
        templateFilePath.append("sketch");

        AssertKiller(!templatesIndexes.contains(templateFilePath));

        SkString output;
        buildFromTemplate(templatePath.c_str(), "sketch", nullptr, nullptr, output);
        writeOutputTextFile(outputDirPath.c_str(), "sketch", "py", output);

        SkString pySketchDir = SkFsUtils::adjustPathEndSeparator(outputDirPath.c_str());
        pySketchDir.append("PySketch");

        if (!SkFsUtils::exists(pySketchDir.c_str()))
            SkFsUtils::mkdir(pySketchDir.c_str());

        copyTemplateDir("templates/PY3/PySketch.lib", pySketchDir.c_str());

        SkFilePerm perm;
        perm.owner = PM_RWX;
        perm.group = PM_R__;
        perm.other = PM_NOPERM;

        SkString executable = SkFsUtils::adjustPathEndSeparator(outputDirPath.c_str());
        executable.append("sketch.py");
        SkFsUtils::chmod(executable.c_str(), perm);

        executable = SkFsUtils::adjustPathEndSeparator(outputDirPath.c_str());
        executable.append("PySketch/sketch-executor.py");
        SkFsUtils::chmod(executable.c_str(), perm);

        return true;
    }

    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkMakeApp::checkOutputItemName(CStr *itemName, SkString &outputItemName)
{
    if (cli->isUsed("--out-name"))
        outputItemName = cli->value("--out-name").toString();

    else
    {
        SkStringList parsed;
        SkString name = itemName;
        name.split(".", parsed);
        outputItemName = parsed.first();
    }
}

void SkMakeApp::checkOutputDirPath(CStr *outputItemName, SkString &outputDirPath)
{
    outputDirPath = outputItemName;

    if (cli->isUsed("--out-dir"))
        outputDirPath = cli->value("--out-dir").toString();

    if (!SkFsUtils::exists(outputDirPath.c_str()))
        SkFsUtils::mkdir(outputDirPath.c_str());
}

void SkMakeApp::buildFromTemplate(CStr *templatePath, CStr *templateFileName, SkVector<SkPair<SkString, SkString>> *vars, SkVector<SkPair<SkString, SkString>> *blocks, SkString &output)
{
    SkString templateFilePath = SkFsUtils::adjustPathEndSeparator(templatePath);
    templateFilePath.append(templateFileName);

    unsigned index = templatesIndexes[templateFilePath.c_str()];

    SkTemplateModeler modeler;
    modeler.setup(templateFilePath.c_str(), templates[index].data);

    if (vars)
        for(ULong i=0; i<vars->count(); i++)
        {
            SkPair<SkString, SkString> &pair = vars->at(i);
            modeler.setVariable(pair.key().c_str(), pair.value().data());
        }

    if (blocks)
        for(ULong i=0; i<blocks->count(); i++)
        {
            SkPair<SkString, SkString> &pair = blocks->at(i);
            modeler.setBlock(pair.key().c_str(), pair.value().data());
        }

    modeler.build(output);
}

void SkMakeApp::writeOutputTextFile(CStr *outputDirPath, CStr *name, CStr *suffix, SkString &content)
{
    SkString outputFileName = SkFsUtils::adjustPathEndSeparator(outputDirPath);
    outputFileName.append(name);
    outputFileName.append(".");
    outputFileName.append(suffix);

    SkFsUtils::writeTEXT(outputFileName.c_str(), content.c_str());
}

void SkMakeApp::copyTemplateDir(CStr *templatePath, CStr *outputDirPath)
{
    for(unsigned i=0; i<templates_count; i++)
        if (templates[i].size > 0)
        {
            SkString filePath = templates[i].relativePath;

            if (filePath.startsWith(templatePath))
            {
                filePath.append(templates[i].name);

                SkString outputFileName = SkFsUtils::adjustPathEndSeparator(outputDirPath);
                outputFileName.append(templates[i].name);

                SkFsUtils::writeTEXT( outputFileName.c_str(), templates[i].data);
            }
        }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkMakeApp::indicatorClear()
{
    if (lastIndicatorSize > 0)
    {
        SkString empty;
        empty.fill(' ', lastIndicatorSize);
        cout << empty << "\r";
        lastIndicatorSize = 0;
    }
}

void SkMakeApp::indicatorNextStep(CStr *msg)
{
    SkString indicator;

    if (indicatorSteps == 0)
        indicator = "-";
    else if (indicatorSteps == 1)
        indicator = "\\";
    else if (indicatorSteps == 2)
        indicator = "|";
    else if (indicatorSteps == 3)
        indicator = "/";

    indicatorClear();

    cout << "["<< indicator << "] " << msg << "\r"; cout.flush();
    lastIndicatorSize = strlen(msg) + 4;

    if (indicatorSteps == 3)
        indicatorSteps = 0;
    else
        indicatorSteps++;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkMakeApp, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (Arg_Int32 != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
