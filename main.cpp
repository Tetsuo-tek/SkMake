#include "skmakeapp.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--skmake-file", "-m", "", "creates Makefile starting from skmake.json");
    cli->add("--cores", "-c", "", "compiles when Makefile is ready");

    cli->add("--sk-src-path", "", "", "sets SpecialK source directory");

    cli->add("--out-dir", "", "", "sets directory path for output code");
    cli->add("--out-name", "", "", "sets name for output item");
    cli->add("--out-file", "", "", "sets name for output file");
    cli->add("--out-binary", "", "", "sets data output to binary (CVoid *), otherwise it will be text (CStr *)");

    cli->add("--max-content-size", "", "", "sets the limit to include a file-content by size (default is 100 kB)");
    cli->add("--filter-ext", "", "", "sets preferred file type to evaluate");
    cli->add("--exclude-ext", "", "", "excludes file types (by suffixes) from evaluation process");
    cli->add("--exclude-dir", "", "", "excludes subdirectories from evaluation process");
    cli->add("--fs-to-h", "", "", "generates C header from a directory set contents");

    cli->add("--print-template", "", "", "prints templates list");

    cli->add("--ls-templates", "", "", "prints templates list");
    cli->add("--ls-templates-with-paths", "", "", "as '--ls-templates' but showing their relative paths");
    cli->add("--ls-template-files", "", "", "prints all files owned by a template");

    cli->add("--set-template-config", "", "", "sets JSON configuration object for current requested template");

    cli->add("--create-cpp-class", "", "", "generates cpp class");
    cli->add("--create-cpp-program", "", "", "generates cpp program");

    cli->add("--create-py-class", "", "", "generates py class");
    cli->add("--create-py-program", "", "", "generates Python3 program");
    cli->add("--create-py-sketch", "", "", "generates Python3 flow-sketch");

    if (!cli->check())
        exit(1);

    //logger->enable(skApp->osEnv()->existsAppArgument("-v") || skApp->osEnv()->existsAppArgument("--help"));
    skApp->init(1, 200000, SK_FREELOOP, true);

    new SkMakeApp;
    return skApp->exec();
}
