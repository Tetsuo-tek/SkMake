################################################################################
# Makefile for building: SkMake.bin
# GMT-Time: 
################################################################################

TARGET = SkMake.bin
CXX = /usr/bin/g++
LINK = /usr/bin/g++
BUILD_PATH = ./build
DEFINES = -DENABLE_SKAPP -DSPECIALK_APPLICATION -DENABLE_AES
CXXFLAGS = -pipe -g -std=gnu++11 -W -fPIC $(DEFINES)
DEL_FILE = rm -vf
DEL_DIR = rmdir -v
CHK_DIR_EXISTS = test -d
MKDIR = mkdir -p
COPY_FILE = cp -vf
COPY_DIR = cp -vf -R
INSTALL_FILE = install -m 644 -vp
INSTALL_PROGRAM = install -m 755 -vp
SYMLINK = ln -vf -s
MOVE = mv -vf
TAR = tar -cvf
COMPRESS = gzip -9f
SED = sed
STRIP = strip
INCPATH = -I../SpecialK/LibSkFlat/ -I../SpecialK/LibSkCore/ -I../SpecialK/LibSkScript/
LIBS = -L/usr/lib -lm -lz -lcrypto -pthread
OBJECTS = \
		./build/skmakeconstructor.o \
		./build/skbufferdevice.o \
		./build/skabstractdevice.o \
		./build/skflatsignal.o \
		./build/skabstractflatdevice.o \
		./build/skflatfile.o \
		./build/skfile.o \
		./build/sktempfile.o \
		./build/skfileinfoslist.o \
		./build/skattach.o \
		./build/sksignal.o \
		./build/skslot.o \
		./build/skmutexlocker.o \
		./build/skcli.o \
		./build/skmimetype.o \
		./build/skelapsedtime.o \
		./build/skstandalonesignal.o \
		./build/skringbuffer.o \
		./build/skmath.o \
		./build/skdatabuffer.o \
		./build/skdatacrypt.o \
		./build/skthread.o \
		./build/skeventloop.o \
		./build/skosenv.o \
		./build/skfltkgroups.o \
		./build/skfltklayout.o \
		./build/skfltkwindows.o \
		./build/skfltkwidgetwrapper.o \
		./build/skdatetime.o \
		./build/skfltkui.o \
		./build/skapp.o \
		./build/skobject.o \
		./build/skdefines.o \
		./build/skstring.o \
		./build/skstringlist.o \
		./build/skargsmap.o \
		./build/sklogmachine.o \
		./build/skvariant.o \
		./build/skflatobject.o \
		./build/skarraycast.o \
		./build/skfsutils.o \
		./build/sktemplatemodeler.o \
		./build/skmakeapp.o \
		./build/main.o

all: Makefile SkMake.bin

clean: 
	-$(DEL_FILE) $(OBJECTS)
distclean: clean 
	-$(DEL_FILE) $(OBJECTS) 
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) ./Makefile

SkMake.bin: $(OBJECTS)
	$(LINK) $(OBJECTS) $(LIBS) -o $(TARGET)

./build/skelapsedtime.o: \
		../SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skelapsedtime.o ../SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.cpp

./build/skringbuffer.o: \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skringbuffer.o ../SpecialK/LibSkFlat/Core/Containers/skringbuffer.cpp

./build/skmutexlocker.o: \
		../SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmutexlocker.o ../SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.cpp

./build/skstringlist.o: \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstringlist.o ../SpecialK/LibSkFlat/Core/Containers/skstringlist.cpp

./build/skdefines.o: \
		../SpecialK/LibSkFlat/skdefines.cpp \
		../SpecialK/LibSkFlat/skdefines.h \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdefines.o ../SpecialK/LibSkFlat/skdefines.cpp

./build/skfltkwindows.o: \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwindows.cpp \
		../SpecialK/LibSkCore/UI/FLTK/skfltkgroups.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltklayout.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwindows.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		../SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkwindows.o ../SpecialK/LibSkCore/UI/FLTK/skfltkwindows.cpp

./build/skdatabuffer.o: \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/skmath.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatabuffer.o ../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.cpp

./build/skmakeapp.o: \
		../SkMake/skmakeapp.cpp \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SkMake/skmakeconstructor.h \
		../SkMake/skmakeapp.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h \
		../SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		../SpecialK/LibSkScript/Core/Scripting/SkCodeTemplate/sktemplatemodeler.h \
		../SkMake/templates.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmakeapp.o ../SkMake/skmakeapp.cpp

./build/skstandalonesignal.o: \
		../SpecialK/LibSkCore/Core/Object/skstandalonesignal.cpp \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/Core/Object/skstandalonesignal.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstandalonesignal.o ../SpecialK/LibSkCore/Core/Object/skstandalonesignal.cpp

./build/skeventloop.o: \
		../SpecialK/LibSkCore/Core/App/skeventloop.cpp \
		../SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		../SpecialK/LibSkCore/Core/Object/skstandalonesignal.h \
		../SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		../SpecialK/LibSkCore/Core/App/skeventloop.h \
		../SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		../SpecialK/LibSkCore/Core/Object/skstandalonesignal.h \
		../SpecialK/LibSkFlat/Core/System/skdatacrypt.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skeventloop.o ../SpecialK/LibSkCore/Core/App/skeventloop.cpp

./build/skmakeconstructor.o: \
		../SkMake/skmakeconstructor.cpp \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SkMake/skmakeconstructor.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmakeconstructor.o ../SkMake/skmakeconstructor.cpp

./build/sktempfile.o: \
		../SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.cpp \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkFlat/Core/System/skosenv.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sktempfile.o ../SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.cpp

./build/skslot.o: \
		../SpecialK/LibSkCore/Core/Object/skslot.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/Containers/skpair.h \
		../SpecialK/LibSkCore/Core/Object/sksignal.h \
		../SpecialK/LibSkCore/Core/Object/skslot.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skslot.o ../SpecialK/LibSkCore/Core/Object/skslot.cpp

./build/skfltkwidgetwrapper.o: \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.cpp \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwindows.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkwidgetwrapper.o ../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.cpp

./build/skfsutils.o: \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.cpp \
		../SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		../SpecialK/LibSkFlat/Core/System/skosenv.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfsutils.o ../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.cpp

./build/skargsmap.o: \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skmap.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skargsmap.o ../SpecialK/LibSkFlat/Core/Containers/skargsmap.cpp

./build/skcli.o: \
		../SpecialK/LibSkFlat/Core/System/skcli.cpp \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/System/skcli.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skcli.o ../SpecialK/LibSkFlat/Core/System/skcli.cpp

./build/skabstractflatdevice.o: \
		../SpecialK/LibSkFlat/Core/System/skabstractflatdevice.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Object/skflatsignal.h \
		../SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflatdevice.o ../SpecialK/LibSkFlat/Core/System/skabstractflatdevice.cpp

./build/skthread.o: \
		../SpecialK/LibSkCore/Core/System/Thread/skthread.cpp \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/Core/App/skeventloop.h \
		../SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		../SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skthread.o ../SpecialK/LibSkCore/Core/System/Thread/skthread.cpp

./build/skosenv.o: \
		../SpecialK/LibSkFlat/Core/System/skosenv.cpp \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/System/skosenv.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skosenv.o ../SpecialK/LibSkFlat/Core/System/skosenv.cpp

./build/skflatobject.o: \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.cpp \
		../SpecialK/LibSkFlat/skdefines.h \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatobject.o ../SpecialK/LibSkFlat/Core/Object/skflatobject.cpp

./build/skmimetype.o: \
		../SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.h \
		../SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmimetype.o ../SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.cpp

./build/skflatsignal.o: \
		../SpecialK/LibSkFlat/Core/Object/skflatsignal.cpp \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkFlat/Core/Object/skflatsignal.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatsignal.o ../SpecialK/LibSkFlat/Core/Object/skflatsignal.cpp

./build/skapp.o: \
		../SpecialK/LibSkCore/Core/App/skapp.cpp \
		../SpecialK/LibSkCore/Core/App/skeventloop.h \
		../SpecialK/LibSkFlat/Core/System/skcli.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.h \
		../SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/System/skosenv.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skapp.o ../SpecialK/LibSkCore/Core/App/skapp.cpp

./build/skstring.o: \
		../SpecialK/LibSkFlat/Core/Containers/skstring.cpp \
		../SpecialK/LibSkFlat/skdefines.h \
		../SpecialK/LibSkFlat/Core/Containers/skvector.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstring.o ../SpecialK/LibSkFlat/Core/Containers/skstring.cpp

./build/skfltklayout.o: \
		../SpecialK/LibSkCore/UI/FLTK/skfltklayout.cpp \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltklayout.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltklayout.o ../SpecialK/LibSkCore/UI/FLTK/skfltklayout.cpp

./build/skobject.o: \
		../SpecialK/LibSkCore/Core/Object/skobject.cpp \
		../SpecialK/LibSkCore/Core/Object/skattach.h \
		../SpecialK/LibSkCore/Core/Object/sksignal.h \
		../SpecialK/LibSkCore/Core/Object/skslot.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/Containers/sklinkedmap.h \
		../SpecialK/LibSkFlat/Core/Containers/sklist.h \
		../SpecialK/LibSkFlat/Core/Containers/skarray.h \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skobject.o ../SpecialK/LibSkCore/Core/Object/skobject.cpp

./build/skdatetime.o: \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatetime.o ../SpecialK/LibSkFlat/Core/System/Time/skdatetime.cpp

./build/skbufferdevice.o: \
		../SpecialK/LibSkCore/Core/System/skbufferdevice.cpp \
		../SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skbufferdevice.o ../SpecialK/LibSkCore/Core/System/skbufferdevice.cpp

./build/skfileinfoslist.o: \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.cpp \
		../SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfileinfoslist.o ../SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.cpp

./build/skarraycast.o: \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skarraycast.o ../SpecialK/LibSkFlat/Core/Containers/skarraycast.cpp

./build/skfltkui.o: \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.cpp \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkui.o ../SpecialK/LibSkCore/UI/FLTK/skfltkui.cpp

./build/sktemplatemodeler.o: \
		../SpecialK/LibSkScript/Core/Scripting/SkCodeTemplate/sktemplatemodeler.cpp \
		../SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		../SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkScript/Core/Scripting/SkCodeTemplate/sktemplatemodeler.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sktemplatemodeler.o ../SpecialK/LibSkScript/Core/Scripting/SkCodeTemplate/sktemplatemodeler.cpp

./build/skfltkgroups.o: \
		../SpecialK/LibSkCore/UI/FLTK/skfltkgroups.cpp \
		../SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkgroups.h \
		../SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkgroups.o ../SpecialK/LibSkCore/UI/FLTK/skfltkgroups.cpp

./build/skdatacrypt.o: \
		../SpecialK/LibSkFlat/Core/System/skdatacrypt.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatacrypt.o ../SpecialK/LibSkFlat/Core/System/skdatacrypt.cpp

./build/skflatfile.o: \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.cpp \
		../SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatfile.o ../SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.cpp

./build/skabstractdevice.o: \
		../SpecialK/LibSkCore/Core/System/skabstractdevice.cpp \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		../SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractdevice.o ../SpecialK/LibSkCore/Core/System/skabstractdevice.cpp

./build/skattach.o: \
		../SpecialK/LibSkCore/Core/Object/skattach.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkCore/Core/Object/skattach.h \
		../SpecialK/LibSkCore/Core/Object/skobject.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skattach.o ../SpecialK/LibSkCore/Core/Object/skattach.cpp

./build/skvariant.o: \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkFlat/Core/Containers/skstack.h \
		../SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		../SpecialK/LibSkFlat/Core/Containers/abstract/skabstractlist.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skcpointer.h \
		../SpecialK/LibSkFlat/Core/Containers/skmap.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		../SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/sklist.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skvariant.o ../SpecialK/LibSkFlat/Core/Containers/skvariant.cpp

./build/main.o: \
		../SkMake/main.cpp \
		../SkMake/skmakeapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/main.o ../SkMake/main.cpp

./build/sksignal.o: \
		../SpecialK/LibSkCore/Core/Object/sksignal.cpp \
		../SpecialK/LibSkCore/Core/Object/skattach.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkCore/Core/Object/sksignal.h \
		../SpecialK/LibSkCore/Core/Object/skslot.h \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sksignal.o ../SpecialK/LibSkCore/Core/Object/sksignal.cpp

./build/skfile.o: \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfile.cpp \
		../SpecialK/LibSkFlat/skdefines.h \
		../SpecialK/LibSkFlat/Core/Containers/skstring.h \
		../SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		../SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h \
		../SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		../SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfile.o ../SpecialK/LibSkCore/Core/System/Filesystem/skfile.cpp

./build/skmath.o: \
		../SpecialK/LibSkFlat/Core/skmath.cpp \
		../SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		../SpecialK/LibSkFlat/Core/skmath.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmath.o ../SpecialK/LibSkFlat/Core/skmath.cpp

./build/sklogmachine.o: \
		../SpecialK/LibSkFlat/Core/sklogmachine.cpp \
		../SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		../SpecialK/LibSkFlat/Core/sklogmachine.h \
		../SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		../SpecialK/LibSkCore/Core/App/skapp.h \
		../SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		../SpecialK/LibSkCore/Core/Object/skobject.h \
		../SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		../SpecialK/LibSkFlat/Core/Containers/skargsmap.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sklogmachine.o ../SpecialK/LibSkFlat/Core/sklogmachine.cpp

