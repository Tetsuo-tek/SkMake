# SkMake

This is the tool that simplifies the compile process for C/C++ applications (Sk and not-Sk):

* creates the Makefile starting from a minimal Json configuration;
* automatically starts the compilation if requested;
* has other features that I do not describe because they are evolving.

SkMake vaguely resembles Qt's qmake application since the goal is also the creation of a Makefile. Unlike qmake, however, the output file will be much smaller in size as it will only contain definitions related to what is actually being included in the project.

SkMake is part of the [pck](https://gitlab.com/Tetsuo-tek/pck) base ecosystem.

## Usage

To compile a program with SkMake, it is necessary to create a Json similar to the following, inside the project directory:

```json
{
    "target" : "AudioScope.bin",
    "platform" : "X86_64",
    "compiler" : "/usr/bin/g++",
    "buildPath" : "./build",
    "debugger" : "/usr/bin/gdb",
    "buildOptions" : ["-pipe", "-g", "-std=gnu++11", "-W", "-fPIC"],
    "sourcesPaths" : [
        "../SpecialK/LibSkFlat",
        "../SpecialK/LibSkCore",
        "../SpecialK/LibSkAudio",
        "../SpecialK/LibSkGui"
    ],
    "includeOptPaths" : [ ],
    "dynamicDeps" : [ ],
    "defines" : ["-DENABLE_SKAPP", "-DSPECIALK_APPLICATION", "-DENABLE_AES", "-DENABLE_AUDIO"],
    "libs" : ["-pthread", "-L/usr/lib", "-lm", "-lz", "-lcrypto", "-lfltk"]
}
```

Below is a summary of the properties of this Json object:

* **target** - the name of the executable binary to be created;
* **platform** - the compilation platform (not yet used);
* **compiler** - the path of the compiler;
* **buildPath** - the temporary directory to use for creating the intermediate .o files;
* **debugger** - the path of the debugger (not yet used);
* **buildOptions** - the flags to pass to the compiler;
* **sourcesPaths** - the path of the Sk sources, if the target is a Sk/C++ application;
* **includeOptPaths** - inclusion of additional paths where the included sources can be found;
* **dynamicDeps** - all header files used but not directly included by any header file of the program or framework;
 **defines** - all definition macros;
* **libs** - all libraries to include.

In the case of dynamicDeps, you include the headers of classes that are not included by the program code or other Sk files. This happens, for example, in SkRobot, which uses introspection and does not directly include the modules with the #include macro; they are instantiated directly by the string name passed to them. If you traditionally include everything the program will use, you can leave this property empty.

To launch the Makefile generation and subsequent compilation using an appropriate number of cores for compiling (the machine I am using has 8), execute the following command line:

```sh
SkMake.bin -m skmake.json -v -c 7
```

When SkMake is installed through [pck](https://gitlab.com/Tetsuo-tek/pck), the name of the application will be 'skmake':

```sh
skmake -m skmake.json -v -c 7
```

This command will compile the sources and produce the application binary unless there are errors in the code.

SkMake is not yet complete but already proves to be very useful as it is. The application manages to perfectly replace the limited use of qmake that I previously used to compile Sk applications.

---