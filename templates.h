#ifndef TEMPLATES_H
#define TEMPLATES_H

const char *templates_rootdir = "templates";
const unsigned templates_count = 76;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} templates[76] = {
	{
		true,
		"templates/",
		"CPP",
		0,
		nullptr
	},
	{
		true,
		"templates/",
		"PY3",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/",
		"Programs",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/",
		"Classes",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/",
		"Pieces",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/",
		"PySketch.lib",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/",
		"Sketches",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/Classes/",
		"Class.cli.template",
		0,
		nullptr
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"robotmod.py",
		1995,
		"from flowasync import FlowAsync\n" \
		"from flowsync import FlowSync\n" \
		"\n" \
		"import platform\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class RobotModule(FlowAsync):\n" \
		"    def __init__(self) -> None:\n" \
		"        super().__init__()\n" \
		"        self._userName = \"User1\"\n" \
		"        self._passwd = \"password\"\n" \
		"        self._address = \"127.0.0.1\"\n" \
		"        #self._address = \"192.168.2.51\"\n" \
		"        self._port = 9000\n" \
		"        \n" \
		"        print(\"Operating System:\", platform.system())\n" \
		"        print(\"OS Release:\", platform.release())\n" \
		"        print(\"OS Version:\", platform.version())\n" \
		"        print(\"Machine:\", platform.machine())\n" \
		"        print(\"Processor Architecture:\", platform.architecture())\n" \
		"        print(\"Network Name:\", platform.node())\n" \
		"        print(\"Python Version:\", platform.python_version())\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def newSyncClient(self) -> FlowSync:\n" \
		"        sync = FlowSync()\n" \
		"        ok: bool = False\n" \
		"\n" \
		"        if self._port == 0:\n" \
		"            #print(f\"Connecting to local-unix FlowService [SYNC]: {self._address}\")\n" \
		"            ok = sync.localConnect(self._address)\n" \
		"        \n" \
		"        else:\n" \
		"            #print(f\"Connecting to tcp/ip FlowService [SYNC]: {self._address}:{self._port}\")\n" \
		"            ok = sync.tcpConnect(self._address, self._port)\n" \
		"        \n" \
		"        if ok:\n" \
		"            ok = sync.login(self._userName, self._passwd)\n" \
		"\n" \
		"        if not ok:\n" \
		"            return FlowSync()\n" \
		"        \n" \
		"        else:\n" \
		"            sync.setCurrentDbName(\"Main\")\n" \
		"\n" \
		"        return sync\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def connect(self) -> bool:\n" \
		"        ok = self.tcpConnect(self._address, self._port)\n" \
		"        \n" \
		"        if ok:\n" \
		"            print(f\"Trying to login: {self._userName}\")\n" \
		"            ok = self.login(self._userName, self._passwd)\n" \
		"        \n" \
		"        if ok:\n" \
		"            self.setCurrentDbName(\"Main\")\n" \
		"\n" \
		"        return ok\n" \
		"    \n" \
		"    def disconnect(self) -> None:\n" \
		"        if self.isConnected():\n" \
		"            self.close()\n" \
		"\n" \
		"####################################################\n"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"flowproto.py",
		9521,
		"\n" \
		"import ctypes\n" \
		"import json\n" \
		"\n" \
		"from enum import  Enum, auto\n" \
		"from socketdevice import SocketDevice\n" \
		"from typing import NewType, Any\n" \
		"\n" \
		"FlowChanID = NewType('FlowChanID', ctypes.c_int16)\n" \
		"\n" \
		"class FlowChannel_T(Enum):\n" \
		"    ChannelNotValid = 0\n" \
		"    StreamingChannel = auto()\n" \
		"    ServiceChannel = auto()\n" \
		"\n" \
		"class Variant_T(Enum):\n" \
		"    T_NULL = 0\n" \
		"    T_BOOL = auto()\n" \
		"    T_INT8 = auto()\n" \
		"    T_UINT8 = auto()\n" \
		"    T_INT16 = auto()\n" \
		"    T_UINT16 = auto()\n" \
		"    T_INT32 = auto()\n" \
		"    T_UINT32 = auto()\n" \
		"    T_FLOAT = auto()\n" \
		"    T_INT64 = auto()\n" \
		"    T_UINT64 = auto()\n" \
		"    T_DOUBLE = auto()\n" \
		"    T_BYTEARRAY = auto()\n" \
		"    T_STRING = auto()\n" \
		"    T_LIST = auto()\n" \
		"    T_PAIR = auto()\n" \
		"    T_MAP = auto()\n" \
		"    T_RAWPOINTER = auto()\n" \
		"\n" \
		"class FlowCommand(Enum):\n" \
		"    FCMD_NOCMD = 0\n" \
		"\n" \
		"    FCMD_LOGIN = auto()                     # ONLY SYNC (ASYNC STARTs AS SYNC)\n" \
		"\n" \
		"    FCMD_SET_ASYNC = auto()                 # ONLY SYNC\n" \
		"    FCMD_CHK_SERVICE = auto()               # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_GET_CHANS_LIST = auto()            # ONLY SYNC\n" \
		"    FCMD_GET_CHAN_PROPS = auto()            # ONLY SYNC\n" \
		"\n" \
		"    #FCMD_GET_CHAN_HEADER = auto()           # ONLY SYNC\n" \
		"    FCMD_SET_CHAN_HEADER = auto()           # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_EXISTS_DATABASE = auto()           # ONLY SYNC\n" \
		"    FCMD_ADD_DATABASE = auto()              # *\n" \
		"    FCMD_SET_DATABASE = auto()              # *\n" \
		"    FCMD_GET_DATABASE = auto()              # ONLY SYNC\n" \
		"    FCMD_GET_VARIABLES_KEYS = auto()        # ONLY SYNC\n" \
		"    FCMD_GET_VARIABLES = auto()             # ONLY SYNC\n" \
		"    FCMD_GET_VARIABLES_JSON = auto()        # ONLY SYNC\n" \
		"    FCMD_EXISTS_VARIABLE = auto()           # ONLY SYNC\n" \
		"    FCMD_SET_VARIABLE = auto()              # *\n" \
		"    FCMD_SET_VARIABLE_JSON = auto()         # *\n" \
		"    FCMD_GET_VARIABLE = auto()              # ONLY SYNC\n" \
		"    FCMD_GET_VARIABLE_JSON = auto()         # ONLY SYNC\n" \
		"    FCMD_DEL_VARIABLE = auto()              # *\n" \
		"    FCMD_FLUSHALL = auto()                  #\n" \
		"\n" \
		"    FCMD_ADD_STREAMING_CHAN = auto()        # ONLY ASYNC\n" \
		"    FCMD_ADD_SERVICE_CHAN = auto()          # ONLY ASYNC\n" \
		"    FCMD_DEL_CHAN = auto()                  # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_ATTACH_CHAN = auto()               # ONLY ASYNC\n" \
		"    FCMD_DETACH_CHAN = auto()               # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_EXEC_SERVICE_REQUEST = auto()      # *\n" \
		"    FCMD_RETURN_SERVICE_RESPONSE = auto()   # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_GRAB_REGISTER_CHAN = auto()        # ONLY SYNC\n" \
		"    FCMD_GRAB_UNREGISTER_CHAN = auto()      # ONLY SYNC\n" \
		"    FCMD_GRAB_LASTDATA_CHAN = auto()        # ONLY SYNC\n" \
		"\n" \
		"    FCMD_PUBLISH = auto()                   # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_SUBSCRIBE_CHAN = auto()            # ONLY ASYNC\n" \
		"    FCMD_UNSUBSCRIBE_CHAN = auto()          # ONLY ASYNC\n" \
		"\n" \
		"    FCMD_QUIT = auto()\n" \
		"\n" \
		"class FlowResponse(Enum):\n" \
		"    FRSP_NORSP = 0\n" \
		"\n" \
		"    FRSP_CHK_FLOW = auto()\n" \
		"\n" \
		"    # SENT ONLY TO THE CONNECTION THAT CHANGED DB\n" \
		"    FRSP_CURRENTDB_CHANGED = auto()\n" \
		"\n" \
		"    # SENT ONLY TO ALL\n" \
		"    FRSP_CHANNEL_ADDED = auto()\n" \
		"    FRSP_CHANNEL_REMOVED = auto()\n" \
		"    FRSP_CHANNEL_HEADER = auto()\n" \
		"\n" \
		"    # SENT ONLY TO THE SERVICE\n" \
		"    FRSP_SEND_REQUEST_TO_SERVICE = auto()\n" \
		"    \n" \
		"    # SENT ONLY TO THE REQUESTER\n" \
		"    FRSP_SEND_RESPONSE_TO_REQUESTER = auto()\n" \
		"\n" \
		"    # SENT ONLY TO THE CHANNEL-OWNER\n" \
		"    FRSP_CHANNEL_PUBLISH_START = auto()\n" \
		"    FRSP_CHANNEL_PUBLISH_STOP = auto()\n" \
		"\n" \
		"    # SENT ONLY TO ALL SUBSCRIBERS\n" \
		"    FRSP_SUBSCRIBED_DATA = auto()\n" \
		"\n" \
		"    FRSP_OK = auto()\n" \
		"    FRSP_KO = auto()\n" \
		"\n" \
		"class FlowProto:\n" \
		"    def __init__(self) -> None:\n" \
		"        self._currentSendCmd = FlowCommand.FCMD_NOCMD\n" \
		"        self._currentSendRsp = FlowResponse.FRSP_NORSP\n" \
		"        self._currentRecvCmd = FlowCommand.FCMD_NOCMD\n" \
		"        self._currentRecvRsp = FlowResponse.FRSP_NORSP\n" \
		"        self._buffer = bytearray()\n" \
		"        self._sck = None\n" \
		"    \n" \
		"    def setup(self, sck: SocketDevice) -> None:\n" \
		"        self._sck = sck\n" \
		"\n" \
		"####################################################\n" \
		"# SEND (buffered)\n" \
		"\n" \
		"    def sendStartOfTransaction(self, cmd: FlowCommand) -> bool:\n" \
		"        if self._buffer:\n" \
		"            print(f\"CMD transaction ALREADY open [sending]: {self.commandToString(self._currentSendCmd)}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._currentSendCmd = cmd\n" \
		"        self._buffer += cmd.value.to_bytes(2, byteorder='little')\n" \
		"\n" \
		"        return True\n" \
		"\n" \
		"    def sendStartOfTransaction(self, rsp: FlowResponse) -> bool:\n" \
		"        if self._buffer:\n" \
		"            print(f\"RSP transaction ALREADY open [sending]: {self.responseToString(self._currentSendRsp)}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._currentSendRsp = rsp\n" \
		"        self._buffer += rsp.value.to_bytes(2, byteorder='little')\n" \
		"\n" \
		"        return True\n" \
		"    \n" \
		"    def sendChanType(self, t: FlowChannel_T) -> None:\n" \
		"        self._buffer += t.value.to_bytes(1, byteorder='little')\n" \
		"    \n" \
		"    def sendVariantType(self, t: Variant_T) -> None:\n" \
		"        self._buffer += t.value.to_bytes(2, byteorder='little')\n" \
		"    \n" \
		"    def sendChanID(self, chanID: FlowChanID) -> None:\n" \
		"        self._buffer += chanID.to_bytes(2, byteorder='little')\n" \
		"    \n" \
		"    def sendSize(self, sz: ctypes.c_uint32) -> None:\n" \
		"        self._buffer += sz.to_bytes(4, byteorder='little')\n" \
		"    \n" \
		"    def sendBool(self, v: bool) -> None:\n" \
		"        self._buffer += v.to_bytes(1, byteorder='little')\n" \
		"    \n" \
		"    def sendString(self, text: str) -> None:\n" \
		"        sz = len(text)\n" \
		"        self.sendSize(sz)\n" \
		"\n" \
		"        if sz == 0:\n" \
		"            return\n" \
		"        \n" \
		"        self._buffer += text.encode('utf-8')\n" \
		"    \n" \
		"    def sendJSON(self, val) -> None:\n" \
		"        self.sendString(json.dumps(val))\n" \
		"    \n" \
		"    def sendBuffer(self, b: bytearray) -> None:\n" \
		"        sz = len(b)\n" \
		"        self.sendSize(sz)\n" \
		"\n" \
		"        if sz == 0:\n" \
		"            return\n" \
		"        \n" \
		"        self._buffer += b\n" \
		"    \n" \
		"    def sendEndOfList(self) -> None:\n" \
		"        val = 0\n" \
		"        self._buffer += val.to_bytes(4, byteorder='little')\n" \
		"    \n" \
		"    def sendEndOfMap(self) -> None:\n" \
		"        val = 0\n" \
		"        self._buffer += val.to_bytes(4, byteorder='little')\n" \
		"\n" \
		"    def sendEndOfTransaction(self) -> bool:\n" \
		"        isConnected = self._sck.isConnected()\n" \
		"        canWrite = self._sck.canWrite()\n" \
		"\n" \
		"        if not isConnected or not canWrite:\n" \
		"            print(f\"CANNOT send protocol-transaction [isOpen: {isConnected}; canWrite: {canWrite}]\")\n" \
		"            return False\n" \
		"        \n" \
		"        val = 0\n" \
		"        self._buffer += val.to_bytes(4, byteorder='little')\n" \
		"        \n" \
		"        ok = self._sck.write(self._buffer)\n" \
		"        self._buffer.clear()\n" \
		"\n" \
		"        self._currentSendCmd = FlowCommand.FCMD_NOCMD\n" \
		"        self._currentSendRsp = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        return ok\n" \
		"\n" \
		"\n" \
		"\n" \
		"####################################################\n" \
		"# RECV (unbuffered)\n" \
		"\n" \
		"    def recvStartOfCmdTransaction(self) -> FlowCommand:\n" \
		"        if self._currentRecvCmd != FlowCommand.FCMD_NOCMD:\n" \
		"            print(f\"CMD transaction ALREADY open [receiving]: {self.commandToString(self._currentRecvCmd)}\")\n" \
		"            return FlowCommand.FCMD_NOCMD\n" \
		"        \n" \
		"        cmd = FlowCommand(self._sck.readUInt16())\n" \
		"        self._currentRecvCmd = cmd\n" \
		"        return cmd\n" \
		"    \n" \
		"    def recvStartOfRspTransaction(self) -> FlowResponse:\n" \
		"        if self._currentRecvRsp != FlowResponse.FRSP_NORSP:\n" \
		"            print(f\"RSP transaction ALREADY open [receiving]: {self.commandToString(self._currentRecvRsp)}\")\n" \
		"            return FlowResponse.FRSP_NORSP\n" \
		"        \n" \
		"        rsp = FlowResponse(self._sck.readUInt16())\n" \
		"        self._currentRecvRsp = rsp\n" \
		"        return rsp\n" \
		"\n" \
		"    def recvChanType(self) -> FlowChannel_T:\n" \
		"        return FlowChannel_T(self._sck.readUInt8())\n" \
		"\n" \
		"    def recvVariantType(self) -> Variant_T:\n" \
		"        return Variant_T(self._sck.readUInt16())\n" \
		"\n" \
		"    def recvChanID(self) -> FlowChanID:\n" \
		"        return self._sck.readInt16()\n" \
		"\n" \
		"    def recvSize(self) -> ctypes.c_uint32:\n" \
		"        return self._sck.readUInt32()\n" \
		"\n" \
		"    def recvBool(self) -> bool:\n" \
		"        return bool(self._sck.readUInt8())\n" \
		"\n" \
		"    def recvString(self, sz: ctypes.c_uint32) -> str:\n" \
		"        if sz == 0:\n" \
		"            return \"\"\n" \
		"        \n" \
		"        return self._sck.readString(sz)\n" \
		"    \n" \
		"    def recvJSON(self, sz) -> Any:\n" \
		"        text = self.recvString(sz)\n" \
		"\n" \
		"        if len(text) == 0:\n" \
		"            return None\n" \
		"    \n" \
		"        try:\n" \
		"            return json.loads(text)\n" \
		"\n" \
		"        except json.JSONDecodeError as e:\n" \
		"            print(\"Error during JSON decoding:\", e)\n" \
		"\n" \
		"    def recvBuffer(self, sz: ctypes.c_uint32) -> bytearray:\n" \
		"        if sz == 0:\n" \
		"            return \"\"\n" \
		"        \n" \
		"        return self._sck.read(sz)\n" \
		"    \n" \
		"    def recvList(self) -> list:\n" \
		"        sz: ctypes.c_uint32 = 0\n" \
		"        l = []\n" \
		"\n" \
		"        while True:\n" \
		"            sz = self.recvSize()\n" \
		"\n" \
		"            if sz > 0:\n" \
		"                item: str = self.recvString(sz)\n" \
		"\n" \
		"                if not item:\n" \
		"                    return l\n" \
		"                \n" \
		"                l.append(item)\n" \
		"            \n" \
		"            else:\n" \
		"                return l                \n" \
		"\n" \
		"    def recvEndOfTransaction(self) -> bool:\n" \
		"        ret = self._sck.readUInt32()\n" \
		"\n" \
		"        if ret != 0:\n" \
		"            print(\"FAILED to RECV EndOfTransaction [CurrentRsp: {}; ret: {}!=0]\".format(self.commandToString(self._currentRecvCmd), ret))\n" \
		"            return False\n" \
		"\n" \
		"        self._currentRecvCmd = FlowCommand.FCMD_NOCMD\n" \
		"        self._currentRecvRsp = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        return True\n" \
		"\n" \
		"####################################################\n" \
		"# REQ\n" \
		"\n" \
		"####################################################\n" \
		"# ANSWER\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def commandToString(self, cmd: FlowCommand) -> str:\n" \
		"        return cmd.name\n" \
		"    \n" \
		"    def commandToBin(cmd : str) -> FlowCommand:\n" \
		"        return FlowCommand[str]\n" \
		"\n" \
		"    def responseToString(self, rsp: FlowResponse) -> str:\n" \
		"        return rsp.name\n" \
		"    \n" \
		"    def responseToBin(cmd : str) -> FlowResponse:\n" \
		"        return FlowResponse[str]\n"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"flowasync.py",
		21107,
		"import ctypes\n" \
		"import struct\n" \
		"import json\n" \
		"\n" \
		"from collections import deque\n" \
		"from flowproto import FlowResponse, FlowCommand, FlowChanID, FlowChannel_T, Variant_T\n" \
		"from abstractflow import FlowChannel, AbstractFlow, ProtocolRecvError\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class FlowChannelData:\n" \
		"    def __init__(self) -> None:\n" \
		"        self.chanID: FlowChanID = -1\n" \
		"        self.data: bytearray = b\"\"\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class FlowAsync(AbstractFlow):\n" \
		"    def __init__(self) -> None:\n" \
		"        super().__init__()\n" \
		"        self._maxDataQueueCount = 1000\n" \
		"        self._subscribedChannels = []\n" \
		"        self._channelsData = deque()\n" \
		"        self._currentData = FlowChannelData()\n" \
		"        self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"        self._currentDbName = \"\"\n" \
		"        self._onNewChanCb = None\n" \
		"        self._onDelChanCb = None\n" \
		"        self._onStartChanPubReqCb = None\n" \
		"        self._onStopChanPubReqCb = None\n" \
		"        self._onGrabDataCb = None\n" \
		"        self._onServiceRequest = None\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def setNewChanCallBack(self, cb) -> None:\n" \
		"        self._onNewChanCb = cb\n" \
		"\n" \
		"    def setDelChanCallBack(self, cb) -> None:\n" \
		"        self._onDelChanCb = cb\n" \
		"\n" \
		"    def setStartChanPubReqCallBack(self, cb) -> None:\n" \
		"        self._onStartChanPubReqCb = cb\n" \
		"\n" \
		"    def setStopChanPubReqCallBack(self, cb) -> None:\n" \
		"        self._onStopChanPubReqCb = cb\n" \
		"\n" \
		"    def setGrabDataCallBack(self, cb) -> None:\n" \
		"        self._onGrabDataCb = cb\n" \
		"\n" \
		"    def setRequestCallBack(self, cb) -> None:\n" \
		"        self._onServiceRequest = cb\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def checkService(self) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_ASYNC)\n" \
		"        self._p.sendEndOfTransaction();\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        return True\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def addStreamingChannel(self, t: Variant_T, name: str, mime: str=\"\", udm: str=\"\") -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        l = []\n" \
		"        l.append(t.value)\n" \
		"        l.append(name)\n" \
		"        l.append(mime)\n" \
		"        l.append(udm)\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_STREAMING_CHAN)\n" \
		"        self._p.sendJSON(l)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"REQUESTED to ADD a new StreamingChannel: {name}\")\n" \
		"        return True\n" \
		"    \n" \
		"    def addServiceChannel(self, name: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        l = []\n" \
		"        l.append(name)\n" \
		"\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_SERVICE_CHAN)\n" \
		"        self._p.sendJSON(l)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"REQUESTED to ADD a new ServiceChannel: {name}\")\n" \
		"        return True\n" \
		"\n" \
		"    def removeChannel(self, chanID: FlowChanID) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        if chanID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {chanID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_CHAN)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"REQUESTED to REMOVE a channel: {self._channelsIndexes[chanID].name}\")\n" \
		"        return True\n" \
		"\n" \
		"    def setChannelHeader(self, chanID: FlowChanID, data: bytearray):\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        if chanID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {chanID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_CHAN_HEADER)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendBuffer(data)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        print(f\"Setup HEADER [{len(data)} B] for a channel: {self._channelsIndexes[chanID].name}\");\n" \
		"        return True\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def attach(self, sourceID: FlowChanID, targetID: FlowChanID) -> bool:\n" \
		"        if sourceID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {sourceID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        if targetID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {targetID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)\n" \
		"        self._p.sendChanID(sourceID)\n" \
		"        self._p.sendChanID(targetID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"REQUESTED to ATTACH channels: {self._channelsIndexes[sourceID].name} -> {self._channelsIndexes[targetID].name}\")\n" \
		"        return True\n" \
		"    \n" \
		"    def detach(self, sourceID: FlowChanID, targetID: FlowChanID) -> bool:\n" \
		"        if sourceID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {sourceID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        if targetID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {targetID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)\n" \
		"        self._p.sendChanID(sourceID)\n" \
		"        self._p.sendChanID(targetID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"REQUESTED to DETACH channels: {self._channelsIndexes[sourceID].name} -> {self._channelsIndexes[targetID].name}\")\n" \
		"        return True\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def sendServiceRequest(self, chanID: FlowChanID, cmd: str, val) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        if chanID not in self._channelsIndexes:\n" \
		"            print(f\"Channel NOT found: {chanID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        if self.channelByID(chanID).chan_t != FlowChannel_T.ServiceChannel:\n" \
		"            print(f\"Channel is NOT a service: {chanID}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXEC_SERVICE_REQUEST)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendString(cmd)\n" \
		"        self._p.sendJSON(val)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"ServiceChannel REQUEST sent: {self._channelsIndexes[chanID].name}\")\n" \
		"        return True\n" \
		"    \n" \
		"    def sendServiceResponse(self, chanID: FlowChanID, hash: str, val):\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_RETURN_SERVICE_RESPONSE)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendString(hash)\n" \
		"        self._p.sendJSON(val)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def publish(self, chanID: FlowChanID, b: bytearray) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, b)\n" \
		"    \n" \
		"    def publishString(self, chanID: FlowChanID, text: str) -> bool:        \n" \
		"        return self._publishPacketizedData(chanID, text.encode('utf-8'))\n" \
		"    \n" \
		"    def publishJSON(self, chanID: FlowChanID, val) -> bool:\n" \
		"        try:\n" \
		"            text = json.dumps(val)\n" \
		"\n" \
		"        except json.JSONEncodeError as e:\n" \
		"            print(\"Error during JSON encoding:\", e)\n" \
		"            return False\n" \
		"        \n" \
		"        return self._publishPacketizedData(chanID, text.encode('utf-8'))\n" \
		"\n" \
		"    def publishUInt8(self, chanID: FlowChanID, val: ctypes.c_uint8) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<B\", val))\n" \
		"\n" \
		"    def publishInt8(self, chanID: FlowChanID, val: ctypes.c_int8) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<b\", val))\n" \
		"\n" \
		"    def publishUInt16(self, chanID: FlowChanID, val: ctypes.c_uint16) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<H\", val))\n" \
		"\n" \
		"    def publishInt16(self, chanID: FlowChanID, val: ctypes.c_int16) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<h\", val))\n" \
		"\n" \
		"    def publishUInt32(self, chanID: FlowChanID, val: ctypes.c_uint32) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<I\", val))\n" \
		"\n" \
		"    def publishInt32(self, chanID: FlowChanID, val: ctypes.c_int32) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<i\", val))\n" \
		"\n" \
		"    def publishUInt64(self, chanID: FlowChanID, val: ctypes.c_uint64) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<Q\", val))\n" \
		"\n" \
		"    def publishInt64(self, chanID: FlowChanID, val: ctypes.c_int64) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<q\", val))\n" \
		"\n" \
		"    def publishFloat(self, chanID: FlowChanID, val: ctypes.c_float) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<f\", val))\n" \
		"\n" \
		"    def publishDouble(self, chanID: FlowChanID, val: ctypes.c_double) -> bool:\n" \
		"        return self._publishPacketizedData(chanID, struct.pack(\"<d\", val))\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def _publishPacketizedData(self, chanID: FlowChanID, data: bytearray) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        # NO RESPONSE\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_PUBLISH)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendBuffer(data)\n" \
		"        return self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"####################################################\n" \
		"\n" \
		"    def isSubscribed(self, chanID: FlowChanID) -> bool:\n" \
		"        return (chanID in self._subscribedChannels)\n" \
		"\n" \
		"    def subscribeChannel(self, chanID: FlowChanID) -> bool:\n" \
		"        if FlowChanID in self._subscribedChannels:\n" \
		"            print(f\"Channel is ALREADY subscribed: {self._channelsIndexes[chanID].name}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SUBSCRIBE_CHAN)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        print(f\"Sent SUBSCRIBE for a channel: {self._channelsIndexes[chanID].name}\")\n" \
		"        return True\n" \
		"\n" \
		"    def unsubscribeChannel(self, chanID: FlowChanID) -> bool:\n" \
		"        if FlowChanID in self._subscribedChannels:\n" \
		"            print(f\"Channel is ALREADY subscribed: {self._channelsIndexes[chanID].name}\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_UNSUBSCRIBE_CHAN)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"        # NO RESPONSE\n" \
		"        print(f\"Sent UNSUBSCRIBE for a channel: {self._channelsIndexes[chanID].nam}\")\n" \
		"        return True\n" \
		"\n" \
		"    def hasNextData(self) -> bool:\n" \
		"        return len(self._channelsData) > 0\n" \
		"\n" \
		"    def nextData(self) -> bool:\n" \
		"        if not self.hasNextData():\n" \
		"            return False\n" \
		"        \n" \
		"        self._currentData = self._channelsData.popleft()\n" \
		"        return True\n" \
		"\n" \
		"    def getCurrentData(self) -> FlowChannelData:\n" \
		"        return self._currentData\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def getCurrentDbName(self) -> str:\n" \
		"        return self._currentDbName\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def _onOpenUnixSocket(self) -> bool:\n" \
		"        return True\n" \
		"\n" \
		"    def _onOpenTcpSocket(self) -> bool:\n" \
		"        return True\n" \
		"    \n" \
		"    def _onLogin(self) -> bool:\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_ASYNC)\n" \
		"        self._p.sendEndOfTransaction();\n" \
		"\n" \
		"        # NO RESPONSE\n" \
		"        return True\n" \
		"\n" \
		"    def _onOpen(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    def _onClose(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    def _onDisconnected(self) -> None:\n" \
		"        self._subscribedChannels = []\n" \
		"        self._channelsData = deque()\n" \
		"        self._currentData = FlowChannelData()\n" \
		"        self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"    def _onTick(self) -> None:\n" \
		"        while self._sck.bytesAvailable() > 0:\n" \
		"            sz = self._sck.bytesAvailable()\n" \
		"            self._analyze()\n" \
		"            if sz == self._sck.bytesAvailable():\n" \
		"                break\n" \
		"    \n" \
		"        if self._onGrabDataCb is not None:\n" \
		"            while self.nextData():\n" \
		"                currentData = self.getCurrentData()\n" \
		"                self._onGrabDataCb(currentData.chanID, currentData.data)\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def _analyze(self) -> None:\n" \
		"        if self._lastResponse == FlowResponse.FRSP_NORSP and self._sck.bytesAvailable() > ctypes.sizeof(ctypes.c_int16):\n" \
		"            self._lastResponse = self._p.recvStartOfRspTransaction()\n" \
		"        \n" \
		"        ##\n" \
		"\n" \
		"        if self._lastResponse == FlowResponse.FRSP_CHK_FLOW:\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"        \n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        ##\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CURRENTDB_CHANGED:\n" \
		"            sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"            self._currentDbName = self._p.recvString(sz)\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        ##\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_ADDED:\n" \
		"            ch: FlowChannel = FlowChannel()\n" \
		"            ch.isPublishingEnabled = False\n" \
		"\n" \
		"            ch.chan_t = self._p.recvChanType()\n" \
		"            ch.chanID = self._p.recvChanID()\n" \
		"\n" \
		"            sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"            ch.name = self._p.recvString(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.hashID = self._p.recvString(sz)\n" \
		"\n" \
		"            if ch.chanID in self._channelsIndexes:\n" \
		"                ProtocolRecvError(self._sck, f\"Channel is ALREADY stored: {ch.name} [ChanID: {ch.chanID}]\")\n" \
		"                return\n" \
		"            \n" \
		"            if ch.chan_t == FlowChannel_T.StreamingChannel:\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.udm = self._p.recvString(sz)\n" \
		"\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.mime = self._p.recvString(sz)\n" \
		"\n" \
		"                ch.t = self._p.recvVariantType()\n" \
		"\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.min = self._p.recvJSON(sz)\n" \
		"\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.max = self._p.recvJSON(sz)\n" \
		"\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.hasHeader = self._p.recvJSON(sz)\n" \
		"\n" \
		"                if ch.hasHeader:\n" \
		"                    sz = self._p.recvSize()\n" \
		"                    ch.header = self._p.recvBuffer(sz)\n" \
		"\n" \
		"                sz = self._p.recvSize()\n" \
		"                ch.mpPath = self._p.recvString(sz)\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            self._channelsNames[ch.name] = ch\n" \
		"            #print(type(chanID))\n" \
		"            self._channelsIndexes[ch.chanID] = ch\n" \
		"\n" \
		"            '''\n" \
		"            print(\"ADDED channel: [ID: {}; name: {}; val_T: {}; mime_T: {}; udm: {}; http: {}; hashID: {}]\"\n" \
		"                    .format(ch.chanID, ch.name, ch.t.name, ch.mime, ch.udm, ch.mpPath, ch.hashID))\n" \
		"            '''\n" \
		"\n" \
		"            if self._onNewChanCb is not None:\n" \
		"                self._onNewChanCb(ch)\n" \
		"\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_REMOVED:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            if chanID not in self._channelsIndexes:\n" \
		"                print(\"Channel is NOT stored: [ChanID: {}]\".format(chanID))\n" \
		"                return\n" \
		"            \n" \
		"            ch: FlowChannel = self._channelsIndexes[chanID]\n" \
		"\n" \
		"            if chanID in self._subscribedChannels:\n" \
		"                print(f\"AUTOMATIC UNSUBSCRIBE for a removed channel: [ChanID: {ch.chanID}; name: {ch.name}]\")\n" \
		"                self._subscribedChannels.remove(chanID)\n" \
		"\n" \
		"            '''\n" \
		"            print(\"REMOVED channel: [ID: {}; name: {}; hashID: {}]\" .format(ch.chanID, ch.name, ch.hashID))\n" \
		"            '''\n" \
		"\n" \
		"            del self._channelsNames[ch.name]\n" \
		"            del self._channelsIndexes[ch.chanID]\n" \
		"\n" \
		"            if self._onDelChanCb is not None:\n" \
		"                self._onDelChanCb(ch)\n" \
		"\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_HEADER:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"\n" \
		"            if chanID not in self._channelsIndexes:\n" \
		"                print(\"Channel is NOT stored: [ChanID: {}]\".format(chanID))\n" \
		"                return\n" \
		"            \n" \
		"            ch: FlowChannel = self._channelsIndexes[chanID]\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.header = self._p.recvBuffer(sz)\n" \
		"            ch.hasHeader = (len(ch.header) > 0)\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_SEND_RESPONSE_TO_REQUESTER:\n" \
		"            sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"            val = self._p.recvJSON(sz)\n" \
		"\n" \
		"            #print(f\"!!!!!!! {val}\")\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_SEND_REQUEST_TO_SERVICE:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"\n" \
		"            sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"            hash = self._p.recvString(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            cmdName = self._p.recvString(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            val = self._p.recvJSON(sz)\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            if self._onServiceRequest is not None:\n" \
		"                self._onServiceRequest(chanID, hash, cmdName, val)\n" \
		"                \n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_START:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"\n" \
		"            if chanID not in self._channelsIndexes:\n" \
		"                print(f\"Channel is NOT stored: [ChanID: {chanID}]\")\n" \
		"                return\n" \
		"            \n" \
		"            ch: FlowChannel = self._channelsIndexes[chanID]\n" \
		"            ch.isPublishingEnabled = True\n" \
		"\n" \
		"            if self._onStartChanPubReqCb is not None:\n" \
		"                self._onStartChanPubReqCb(ch)\n" \
		"            '''\n" \
		"            print(\"Server has requested for channel-publish START: [ID: {}; name: {}]\" .format(ch.chanID, ch.name))\n" \
		"            '''\n" \
		"\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_STOP:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"\n" \
		"            if chanID not in self._channelsIndexes:\n" \
		"                print(f\"Channel is NOT stored: [ChanID: {chanID}]\")\n" \
		"                return\n" \
		"            \n" \
		"            ch: FlowChannel = self._channelsIndexes[chanID]\n" \
		"            ch.isPublishingEnabled = False\n" \
		"\n" \
		"            if self._onStopChanPubReqCb is not None:\n" \
		"                self._onStopChanPubReqCb(ch)\n" \
		"            '''\n" \
		"            print(\"Server has requested for channel-publish STOP: [ID: {}; name: {}]\" .format(ch.chanID, ch.name))\n" \
		"            '''\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"        elif self._lastResponse == FlowResponse.FRSP_SUBSCRIBED_DATA:\n" \
		"            chanID: FlowChanID = self._p.recvChanID()\n" \
		"            \n" \
		"            sz = self._p.recvSize()\n" \
		"\n" \
		"            if sz == 0:\n" \
		"                print(\"Receiving subscribed data size is ZERO\")\n" \
		"                return\n" \
		"\n" \
		"            flowData = FlowChannelData()\n" \
		"            flowData.chanID = chanID\n" \
		"            flowData.data = self._p.recvBuffer(sz)\n" \
		"\n" \
		"            if not self._p.recvEndOfTransaction():\n" \
		"                ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"                return\n" \
		"            \n" \
		"            self._channelsData.append(flowData)\n" \
		"\n" \
		"            self._lastResponse = FlowResponse.FRSP_NORSP\n" \
		"\n" \
		"####################################################\n" \
		"\n"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"sketch-executor.py",
		1992,
		"#!/usr/bin/env python3\n" \
		"\n" \
		"import sys\n" \
		"import os\n" \
		"\n" \
		"if __name__ == \"__main__\":\n" \
		"\n" \
		"    #########################\n" \
		"\n" \
		"    def svltLoop() -> None:\n" \
		"        setup_function = None\n" \
		"        loop_function = None\n" \
		"    \n" \
		"        if len(sys.argv) < 2:\n" \
		"            print(\"Sketch path NOT specified\")\n" \
		"            return\n" \
		"\n" \
		"        sketchPath = sys.argv[1]\n" \
		"\n" \
		"        if not os.path.isfile(sketchPath):\n" \
		"            print(f\"Sketch NOT found: '{sketchPath}'\")\n" \
		"            return\n" \
		"        \n" \
		"        sketchAbsPath = os.path.abspath(sketchPath)\n" \
		"        print(f\"Importing sketch: '{sketchAbsPath}'.\")\n" \
		"\n" \
		"        sys.path.append(os.path.dirname(sketchAbsPath))\n" \
		"\n" \
		"        try:\n" \
		"            sketchName = os.path.splitext(os.path.basename(sketchPath))[0]\n" \
		"            sketch = __import__(sketchName)\n" \
		"\n" \
		"            if not hasattr(sketch, \"setup\"):\n" \
		"                print(\"setup() function NOT found!\")\n" \
		"                return\n" \
		"            \n" \
		"            setup_function = getattr(sketch, \"setup\")\n" \
		"\n" \
		"            if not hasattr(sketch, \"loop\"):\n" \
		"                print(\"loop() function NOT found!\")\n" \
		"                return\n" \
		"                \n" \
		"            loop_function = getattr(sketch, \"loop\")\n" \
		"            \n" \
		"            close_function = None\n" \
		"\n" \
		"            if hasattr(sketch, \"close\"):\n" \
		"                close_function = getattr(sketch, \"close\")\n" \
		"\n" \
		"        except (ImportError, AttributeError):\n" \
		"            print(f\"CANNOT import the Sketch from: '{sketchPath}'.\")\n" \
		"            return\n" \
		"\n" \
		"        try:\n" \
		"            if not setup_function():\n" \
		"                return\n" \
		"            \n" \
		"            print(f\"Sketch STARTED: {sketchName}\")\n" \
		"\n" \
		"            while True:\n" \
		"                if not loop_function():\n" \
		"                    break\n" \
		"\n" \
		"            print(\"Closing ..\")\n" \
		"\n" \
		"\n" \
		"        except KeyboardInterrupt:\n" \
		"            print(\"Forcing exit with CTRL-C\")\n" \
		"\n" \
		"        finally:\n" \
		"            if close_function is not None:\n" \
		"                close_function()\n" \
		"\n" \
		"            print(\"\\nHALTED\")\n" \
		"            \n" \
		"    #########################\n" \
		"\n" \
		"    svltLoop()\n" \
		"\n" \
		"####################################################"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"abstractflow.py",
		8093,
		"import ctypes\n" \
		"\n" \
		"from socketdevice import SocketDevice\n" \
		"from flowproto import FlowChanID, FlowChannel_T, FlowCommand, FlowProto, Variant_T\n" \
		"from abc import ABC, abstractmethod\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"def ProtocolRecvError(sck: SocketDevice, msg: str) -> None:\n" \
		"    print(\"[FATAL] =>\", msg)\n" \
		"    if sck.isConnected():\n" \
		"        sck.disconnect()\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class FlowChannel:\n" \
		"    def __init__(self) -> None:\n" \
		"        self.chan_t: FlowChannel_T = FlowChannel_T.ChannelNotValid\n" \
		"        self.chanID: FlowChanID = -1\n" \
		"        self.hashID: str = \"\"\n" \
		"        self.name: str = \"\"\n" \
		"        self.udm: str = \"\"\n" \
		"        self.mime: str = \"\"\n" \
		"        self.t: Variant_T = Variant_T.T_NULL\n" \
		"        self.min = None\n" \
		"        self.max = None\n" \
		"        self.hasHeader: bool = False\n" \
		"        self.isPublishingEnabled: bool = False\n" \
		"        self.header: bytearray = b\"\"\n" \
		"        self.mpPath: str = \"\"\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class AbstractFlow(ABC):\n" \
		"    def __init__(self) -> None:\n" \
		"        self._sck = None\n" \
		"        self._unixPath = \"\"\n" \
		"        self._tcpAddress = \"\"\n" \
		"        self._tcpPort = 0\n" \
		"        self._userName = \"\"\n" \
		"        self._userToken = \"\"\n" \
		"        self._isAuthorized = False\n" \
		"        self._channelsNames = {}\n" \
		"        self._channelsIndexes = {}\n" \
		"        self._p = FlowProto()\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def localConnect(self, path: str) -> bool:\n" \
		"        if self._sck is not None:\n" \
		"            print(\"SocketDecice is ALREADY initialized\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._sck = SocketDevice()\n" \
		"\n" \
		"        if not self._sck.localConnect(path):\n" \
		"            self._sck = None\n" \
		"            print(\"CANNOT connect to FsService:\", path)\n" \
		"            return False\n" \
		"\n" \
		"        self._unixPath = path\n" \
		"        self._tcpAddress = \"\"\n" \
		"        self._tcpPort = 0\n" \
		"\n" \
		"        self._open()\n" \
		"        return True\n" \
		"\n" \
		"    def tcpConnect(self, host: str, port: ctypes.c_uint16) -> bool:\n" \
		"        if self._sck is not None:\n" \
		"            print(\"SocketDecice is ALREADY initialized\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._sck = SocketDevice()\n" \
		"\n" \
		"        if not self._sck.tcpConnect(host, port):\n" \
		"            self._sck = None\n" \
		"            print(\"CANNOT connect to FsService: {}:{}\".format(host, port))\n" \
		"            return False\n" \
		"\n" \
		"        self._tcpAddress = host\n" \
		"        self._tcpPort = port\n" \
		"        self._unixPath = \"\"\n" \
		"\n" \
		"        self._open()\n" \
		"        return True\n" \
		"\n" \
		"    def _open(self) -> None:\n" \
		"        if self.channelsCount() > 0:\n" \
		"            self._resetChannels()\n" \
		"\n" \
		"        if self.isUnixSocket():\n" \
		"            self._onOpenUnixSocket()\n" \
		"        else:\n" \
		"            self._onOpenTcpSocket()\n" \
		"\n" \
		"        self._p.setup(self._sck)\n" \
		"\n" \
		"        self._onOpen()\n" \
		"        #print(\"Connected\")\n" \
		"\n" \
		"    def _resetChannels(self) -> None:\n" \
		"        self._channelsNames = {}\n" \
		"        self._channelsIndexes = {}\n" \
		"\n" \
		"    def close(self) -> None:\n" \
		"        if self._sck is None:\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return\n" \
		"        \n" \
		"        self._sck.disconnect()\n" \
		"        self._onClose()\n" \
		"\n" \
		"        #print(\"Disconnected\") < da inserire nel tick all'interno di onDisconnected()\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def tick(self) -> None:\n" \
		"        if self._sck is None:\n" \
		"            return\n" \
		"        \n" \
		"        if not self._sck.isConnected():\n" \
		"            self._onDisconnected()\n" \
		"            self._sck = None\n" \
		"            return\n" \
		"        \n" \
		"        self._sck.waitForReadyRead(0.002)\n" \
		"        self._onTick()\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def login(self, userName: str, userToken: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_LOGIN)\n" \
		"        self._p.sendString(userName)\n" \
		"        self._p.sendString(userToken)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return False\n" \
		"\n" \
		"        self._userName = userName\n" \
		"        self._userToken = userToken\n" \
		"        self._isAuthorized = True\n" \
		"\n" \
		"        return self._onLogin()\n" \
		"\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def setCurrentDbName(self, dbName: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_DATABASE)\n" \
		"        self._p.sendString(dbName)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"        #NO RESPONSE\n" \
		"        return True\n" \
		"\n" \
		"    def setVariable(self, key: str, val) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_VARIABLE_JSON)\n" \
		"        self._p.sendString(key)\n" \
		"        self._p.sendJSON(val)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        #NO RESPONSE\n" \
		"        return True\n" \
		"    \n" \
		"    def delVariable(self, key: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_VARIABLE)\n" \
		"        self._p.sendString(key)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"        #NO RESPONSE\n" \
		"        return True\n" \
		"\n" \
		"    def flushAll(self) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDecice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_FLUSHALL)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"        #NO RESPONSE\n" \
		"        return True\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def isUnixSocket(self) -> bool:\n" \
		"        return (len(self._unixPath) > 0)\n" \
		"\n" \
		"    def isTcpSocket(self) -> bool:\n" \
		"        return (self._tcpPort != 0)\n" \
		"\n" \
		"    def getUnixPath(self) -> str:\n" \
		"        return self._unixPath\n" \
		"\n" \
		"    def getTcpAddress(self) -> str:\n" \
		"        return self._tcpAddress\n" \
		"\n" \
		"    def getTcpPort(self) -> ctypes.c_uint16:\n" \
		"        return self._tcpPort\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def isConnected(self) -> bool:\n" \
		"        if self._sck is None:\n" \
		"            return False\n" \
		"        \n" \
		"        return self._sck.isConnected()\n" \
		"\n" \
		"    def isAuthorized(self) -> bool:\n" \
		"        return self._isAuthorized\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def containsChannel(self, name: str) -> bool:\n" \
		"        return (name in self._channelsNames)\n" \
		"\n" \
		"    def channels(self) -> list:\n" \
		"        return list(self._channelsNames.keys())\n" \
		"\n" \
		"    def channelByName(self, name: str) -> FlowChannel:\n" \
		"        try:\n" \
		"            return self._channelsNames[name]\n" \
		"        except KeyError:\n" \
		"            return None\n" \
		"\n" \
		"    def channelByID(self, chanID: FlowChanID) -> FlowChannel:\n" \
		"        try:\n" \
		"            return self._channelsIndexes[chanID]\n" \
		"        except KeyError:\n" \
		"            return None\n" \
		"\n" \
		"    def channelID(self, name: str) -> FlowChanID:\n" \
		"        if name in self._channelsNames:\n" \
		"            print(\"Channel UNKNOWN:\", name)\n" \
		"            return -1\n" \
		"        \n" \
		"        ch: FlowChannel = self._channelsNames[name]\n" \
		"        return ch.chanID\n" \
		"\n" \
		"    def channelsCount(self) -> int:\n" \
		"        return len(self._channelsIndexes)\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onOpenUnixSocket(self) -> bool:\n" \
		"        pass\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onOpenTcpSocket(self) -> bool:\n" \
		"        pass\n" \
		"    \n" \
		"    @abstractmethod\n" \
		"    def _onLogin(self) -> bool:\n" \
		"        return\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onOpen(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onClose(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onDisconnected(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    @abstractmethod\n" \
		"    def _onTick(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"####################################################"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"socketdevice.py",
		7014,
		"import socket\n" \
		"import select\n" \
		"import fcntl\n" \
		"import termios\n" \
		"import sys\n" \
		"import errno\n" \
		"import ctypes\n" \
		"import struct\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class SocketDevice:\n" \
		"    def __init__(self):\n" \
		"        self.sock = None\n" \
		"\n" \
		"    #unix local connection\n" \
		"    def localConnect(self, path: str) -> bool:\n" \
		"        try:\n" \
		"            self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)\n" \
		"            self.sock.connect(path)\n" \
		"            return True\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            print(\"Connection failed: {}\".format(e))\n" \
		"        \n" \
		"        return False\n" \
		"    \n" \
		"    #tcp/ip connection\n" \
		"    def tcpConnect(self, host: str, port: ctypes.c_uint16) -> bool:\n" \
		"        try:\n" \
		"            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)\n" \
		"            self.sock.connect((host, port))\n" \
		"            return True\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            print(\"Connection failed: {}\".format(e))\n" \
		"\n" \
		"        return False\n" \
		"    \n" \
		"    def disconnect(self):\n" \
		"        try:\n" \
		"            if self.sock is not None:\n" \
		"                self.sock.close()\n" \
		"                #self.sock = None\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            print(f\"Disconnect failed: {e}\")\n" \
		"        \n" \
		"    def isConnected(self):\n" \
		"        try:\n" \
		"            data = self.sock.recv(1, socket.MSG_PEEK | socket.MSG_DONTWAIT)\n" \
		"            \n" \
		"            if len(data) == 0 and errno != errno.EAGAIN:\n" \
		"                return False\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            err = e.args[0]\n" \
		"\n" \
		"            if err == errno.EAGAIN:\n" \
		"                return True\n" \
		"            \n" \
		"            else:\n" \
		"                print('Socket error: {}'.format(str(e)))\n" \
		"                return False\n" \
		"        \n" \
		"        return True\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def canWrite(self):\n" \
		"        try:\n" \
		"            r, w, e = select.select([], [self.sock], [self.sock], 0)\n" \
		"            \n" \
		"            if self.sock in w:\n" \
		"                return True\n" \
		"            \n" \
		"            elif self.sock in e:\n" \
		"                print('Error occurred on socket')\n" \
		"        \n" \
		"        except select.error as e:\n" \
		"            print(f'select failed: {e}')\n" \
		"\n" \
		"        return False\n" \
		"    \n" \
		"    def waitForReadyRead(self, timeout=0.2):\n" \
		"        try:\n" \
		"            r, w, e = select.select([self.sock], [], [self.sock], timeout)\n" \
		"            \n" \
		"            if self.sock in r:\n" \
		"                return True\n" \
		"            \n" \
		"            elif self.sock in e:\n" \
		"                print('Error occurred on socket')\n" \
		"        \n" \
		"        except select.error as e:\n" \
		"            print(f'select failed: {e}')\n" \
		"\n" \
		"        return False\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def bytesAvailable(self) -> int:\n" \
		"        try:\n" \
		"            # Ottiene la quantità di dati disponibili sul socket\n" \
		"            bytes_available = fcntl.ioctl(self.sock.fileno(), termios.FIONREAD, bytes(4))\n" \
		"            return int.from_bytes(bytes_available, byteorder=sys.byteorder)\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            print(f\"Cannot get bytes available: {e}\")\n" \
		"\n" \
		"        return 0\n" \
		"    \n" \
		"####################################################\n" \
		"\n" \
		"    def readString(self, sz: int = 0) -> str:\n" \
		"        return self.read(sz).decode('utf-8')\n" \
		"\n" \
		"    def writeString(self, text: str, sz: int = 0) -> None:\n" \
		"        encoded = text.encode('utf-8')\n" \
		"        self.write(encoded, sz)\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def readUInt8(self) -> ctypes.c_uint8:\n" \
		"        byte = self.read(1)\n" \
		"        val, = struct.unpack('<B', byte)\n" \
		"        return val\n" \
		"\n" \
		"    def readInt8(self) -> ctypes.c_int8:\n" \
		"        byte = self.read(1)\n" \
		"        val, = struct.unpack('<b', byte)\n" \
		"        return val\n" \
		"\n" \
		"    def readUInt16(self) -> ctypes.c_uint16:\n" \
		"        bytes = self.read(2)\n" \
		"        val, = struct.unpack('<H', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readInt16(self) -> ctypes.c_int16:\n" \
		"        bytes = self.read(2)\n" \
		"        val, = struct.unpack('<h', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readUInt32(self) -> ctypes.c_uint32:\n" \
		"        bytes = self.read(4)\n" \
		"        val, = struct.unpack('<I', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readInt32(self) -> ctypes.c_int32:\n" \
		"        bytes = self.read(4)\n" \
		"        val, = struct.unpack('<i', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readUInt64(self) -> ctypes.c_uint64:\n" \
		"        bytes = self.read(4)\n" \
		"        val, = struct.unpack('<Q', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readInt64(self) -> ctypes.c_int64:\n" \
		"        bytes = self.read(4)\n" \
		"        val, = struct.unpack('<q', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readFloat(self) -> ctypes.c_float:\n" \
		"        bytes = self.read(4)\n" \
		"        val, = struct.unpack('<f', bytes)\n" \
		"        return val\n" \
		"\n" \
		"    def readDouble(self) -> ctypes.c_double:\n" \
		"        bytes = self.read(8)\n" \
		"        val, = struct.unpack('<d', bytes)\n" \
		"        return val\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def writeUInt8(self, val: ctypes.c_uint8) -> None:\n" \
		"        binary_data = struct.pack(\"<B\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeInt8(self, val: ctypes.c_int8) -> None:\n" \
		"        binary_data = struct.pack(\"<b\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeUInt16(self, val: ctypes.c_uint16) -> None:\n" \
		"        binary_data = struct.pack(\"<H\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeInt16(self, val: ctypes.c_int16) -> None:\n" \
		"        binary_data = struct.pack(\"<h\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeUInt32(self, val: ctypes.c_uint32) -> None:\n" \
		"        binary_data = struct.pack(\"<I\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeInt32(self, val: ctypes.c_int32) -> None:\n" \
		"        binary_data = struct.pack(\"<i\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeUInt64(self, val: ctypes.c_uint64) -> None:\n" \
		"        binary_data = struct.pack(\"<Q\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeInt64(self, val: ctypes.c_int64) -> None:\n" \
		"        binary_data = struct.pack(\"<q\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeFloat(self, val: ctypes.c_float) -> None:\n" \
		"        binary_data = struct.pack(\"<f\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"    def writeDouble(self, val: ctypes.c_double) -> None:\n" \
		"        binary_data = struct.pack(\"<d\", val)\n" \
		"        self.write(binary_data)\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def read(self, sz: int) -> bytearray:\n" \
		"        if sz == 0:\n" \
		"            #sz = self.bytesAvailable();\n" \
		"            print(f\"Read failed [sz==0]\")\n" \
		"            return bytearray()\n" \
		"        \n" \
		"        try:\n" \
		"            data = bytearray()\n" \
		"            remaining = sz\n" \
		"\n" \
		"            while remaining > 0:\n" \
		"                chunk = self.sock.recv(remaining)\n" \
		"\n" \
		"                if not chunk:\n" \
		"                    print(\"CANNOT read chunk\")\n" \
		"                    break\n" \
		"\n" \
		"                data += chunk\n" \
		"                remaining -= len(chunk)\n" \
		"\n" \
		"            return data\n" \
		"        \n" \
		"        except socket.error as e:\n" \
		"            print(f\"Read failed: {e}\")\n" \
		"\n" \
		"        return bytearray()\n" \
		"\n" \
		"    def write(self, data: bytearray) -> bool: #int:\n" \
		"        try:\n" \
		"            self.sock.sendall(data)\n" \
		"            return True\n" \
		"\n" \
		"        except socket.error as e:\n" \
		"            print(f\"Write failed: {e}\")\n" \
		"        \n" \
		"        return False\n" \
		"\n" \
		"####################################################\n" \
		"\n"
	},
	{
		false,
		"templates/PY3/PySketch.lib/",
		"flowsync.py",
		9589,
		"\n" \
		"import ctypes\n" \
		"\n" \
		"from typing import Any\n" \
		"from flowproto import FlowCommand, FlowChanID, FlowChannel_T\n" \
		"from abstractflow import FlowChannel, AbstractFlow, ProtocolRecvError\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"class FlowSync(AbstractFlow):\n" \
		"    def __init__(self) -> None:\n" \
		"        super().__init__()\n" \
		"\n" \
		"    def existsOptionalPairDb(self, dbName: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_DATABASE)\n" \
		"        self._p.sendString(dbName)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"        exists: bool = self._p.recvBool()\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return False\n" \
		"\n" \
		"        return exists\n" \
		"\n" \
		"    def addDatabase(self, dbName: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_DATABASE)\n" \
		"        self._p.sendString(dbName)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        ok: bool = self._p.recvBool()\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return False\n" \
		"        \n" \
		"        return ok\n" \
		"    \n" \
		"    def getCurrentDbName(self) -> str:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return \"\"\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_DATABASE)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"        \n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"        dbName = self._p.recvString(sz)\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return \"\"\n" \
		"        \n" \
		"        return dbName\n" \
		"\n" \
		"    def variablesKeys(self) -> list:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return []\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_KEYS)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"        keys: list = self._p.recvList()\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return []\n" \
		"        \n" \
		"        return keys\n" \
		"\n" \
		"    def getAllVariables(self) -> dict:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return {}\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_JSON)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"        var = self._p.recvJSON(sz)\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return {}\n" \
		"\n" \
		"        return var\n" \
		"\n" \
		"    def existsVariable(self, key: str) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_VARIABLE)\n" \
		"        self._p.sendString(key)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        exists: bool = self._p.recvBool()\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return False\n" \
		"        \n" \
		"        return exists\n" \
		"\n" \
		"    def getVariable(self, key: str) -> Any:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return Any()\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE_JSON)\n" \
		"        self._p.sendString(key)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"        var = self._p.recvJSON(sz)\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return Any\n" \
		"        \n" \
		"        return var\n" \
		"\n" \
		"    def dataGrabbingRegister(self, chanID: FlowChanID) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_REGISTER_CHAN)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        \n" \
		"        # NO RESPONSE\n" \
		"        return self._p.sendEndOfTransaction()\n" \
		"\n" \
		"    def dataGrabbingUnRegister(self, chanID: FlowChanID) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_UNREGISTER_CHAN)\n" \
		"        self._p.sendChanID(chanID)\n" \
		"        \n" \
		"        # NO RESPONSE\n" \
		"        return self._p.sendEndOfTransaction()\n" \
		"\n" \
		"    def grabLastChannelData(self, chanID: FlowChanID) -> bytearray:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return bytearray()\n" \
		"        \n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE)\n" \
		"        self._p.sendString(chanID)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"\n" \
		"        sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"        data: bytearray = self._p.recvBuffer(sz)\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return bytearray()\n" \
		"        \n" \
		"        return data\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def updateChannels(self) -> bool:\n" \
		"        if (self._sck is None) or (not self._sck.isConnected()):\n" \
		"            print(\"SocketDevice is NOT initialized yet\")\n" \
		"            return False\n" \
		"        \n" \
		"        if len(self._channelsNames) > 0:\n" \
		"            self._resetChannels()\n" \
		"        \n" \
		"        chansNames = self._getChannelsList()\n" \
		"\n" \
		"        for chanName in chansNames:\n" \
		"            ch: FlowChannel = self._getChannelProperties(chanName)\n" \
		"            self._channelsNames[ch.name] = ch\n" \
		"            self._channelsIndexes[ch.chanID] = ch\n" \
		"\n" \
		"        return True\n" \
		"\n" \
		"    def _getChannelsList(self) -> list:\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHANS_LIST)\n" \
		"        self._p.sendEndOfTransaction();\n" \
		"        \n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"        chansNames: list = self._p.recvList()\n" \
		"\n" \
		"        if len(chansNames) == 0:\n" \
		"            print(\"Channels list is EMPTY\")\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return []\n" \
		"            \n" \
		"        return chansNames\n" \
		"\n" \
		"    def _getChannelProperties(self, name: str) -> FlowChannel:\n" \
		"        ch: FlowChannel = FlowChannel()\n" \
		"        ch.isPublishingEnabled = False\n" \
		"\n" \
		"        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHAN_PROPS)\n" \
		"        self._p.sendString(name)\n" \
		"        self._p.sendEndOfTransaction()\n" \
		"\n" \
		"        self._sck.waitForReadyRead()\n" \
		"\n" \
		"        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()\n" \
		"        \n" \
		"        ch.chan_t = self._p.recvChanType()\n" \
		"        ch.chanID = self._p.recvChanID()\n" \
		"\n" \
		"        sz: ctypes.c_uint32 = self._p.recvSize()\n" \
		"        ch.hashID = self._p.recvString(sz)\n" \
		"\n" \
		"        if ch.chan_t == FlowChannel_T.StreamingChannel:\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.udm = self._p.recvString(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.mime = self._p.recvString(sz)\n" \
		"\n" \
		"            ch.t = self._p.recvVariantType()\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.min = self._p.recvJSON(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.max = self._p.recvJSON(sz)\n" \
		"\n" \
		"            sz = self._p.recvSize()\n" \
		"            ch.hasHeader = self._p.recvJSON(sz)\n" \
		"\n" \
		"        if not self._p.recvEndOfTransaction():\n" \
		"            ProtocolRecvError(self._sck, \"CANNOT RECV recvEndOfTransaction\")\n" \
		"            return FlowChannel()\n" \
		"        \n" \
		"        return ch\n" \
		"\n" \
		"    def _getChannelHeader(self) -> bytearray:\n" \
		"        return bytearray()\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"    def _onOpenUnixSocket(self) -> bool:\n" \
		"        return True\n" \
		"\n" \
		"    def _onOpenTcpSocket(self) -> bool:\n" \
		"        return True\n" \
		"    \n" \
		"    def _onLogin(self) -> bool:\n" \
		"        return True\n" \
		"\n" \
		"    def _onOpen(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    def _onClose(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    def _onDisconnected(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"    def _onTick(self) -> None:\n" \
		"        pass\n" \
		"\n" \
		"####################################################\n"
	},
	{
		false,
		"templates/PY3/Classes/Class.cli.template/",
		"impl",
		185,
		"\n" \
		"class $$TYPE$$:\n" \
		"    def __init__(self) -> None:\n" \
		"        $#CONSTRUCTOR_IMPL#$\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		true,
		"templates/CPP/",
		"Sk",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/",
		"Pieces",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/",
		"Qt",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/",
		"Programs",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/",
		"Classes",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/",
		"Pieces",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Classes/",
		"ObjectClass.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Classes/",
		"FlatClass.cli.template",
		0,
		nullptr
	},
	{
		false,
		"templates/CPP/Sk/Classes/ObjectClass.cli.template/",
		"impl",
		361,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkObject)\n" \
		"{\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Classes/ObjectClass.cli.template/",
		"decl",
		532,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include \"Core/Object/skobject.h\"\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkObject\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkObject);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        $#PRIVATE#$\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		true,
		"templates/PY3/Programs/",
		"BaseProgram.cli.template",
		0,
		nullptr
	},
	{
		false,
		"templates/PY3/Programs/BaseProgram.cli.template/",
		"main",
		193,
		"#!/usr/bin/env python3\n" \
		"\n" \
		"$#IMPORT#$\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"if __name__ == \"__main__\":\n" \
		"    $#MAIN#$\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"IMPORT\" : \"\",\n" \
		"        \"IMPL\" : \"\",\n" \
		"        \"MAIN\" : \"\"\n" \
		"    }\n" \
		"}"
	},
	{
		true,
		"templates/PY3/Sketches/",
		"PublisherFlowSat.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/PY3/Sketches/",
		"SubscriberFlowSat.cli.template",
		0,
		nullptr
	},
	{
		false,
		"templates/CPP/Sk/Classes/FlatClass.cli.template/",
		"impl",
		217,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"$$TYPE$$::$$TYPE$$()\n" \
		"{\n" \
		"    setObjectName(\"$$TYPE$$\");\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Classes/FlatClass.cli.template/",
		"decl",
		472,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/Object/skflatobject.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlatObject\n" \
		"{\n" \
		"    public:\n" \
		"        $$TYPE$$();\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        $#PRIVATE#$\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/PY3/Sketches/SubscriberFlowSat.cli.template/",
		"sketch",
		1509,
		"#!PySketch/sketch-executor.py\n" \
		"\n" \
		"####################################################\n" \
		"#SKETCH\n" \
		"\n" \
		"from PySketch.abstractflow import FlowChannel\n" \
		"from PySketch.flowproto import FlowChanID\n" \
		"from PySketch.robotmod import RobotModule\n" \
		"\n" \
		"module = None\n" \
		"\n" \
		"import time\n" \
		"\n" \
		"srcChanName = None\n" \
		"src = None\n" \
		"\n" \
		"def setup() -> bool:\n" \
		"    global srcChanName\n" \
		"    global module\n" \
		"\n" \
		"    module = RobotModule()\n" \
		"\n" \
		"    if not module.connect():\n" \
		"        return False\n" \
		"\n" \
		"    module.setNewChanCallBack(onChannelAdded)\n" \
		"    module.setGrabDataCallBack(onDataGrabbed)\n" \
		"\n" \
		"    srcChanName = \"$$INPUT_CHAN$$\"\n" \
		"\n" \
		"    return True\n" \
		"\n" \
		"def loop() -> bool:\n" \
		"    global module\n" \
		"    module.tick()\n" \
		"    return module.isConnected()\n" \
		"\n" \
		"####################################################\n" \
		"#CALLBACKs\n" \
		"\n" \
		"def onChannelAdded(ch: FlowChannel):\n" \
		"    global src\n" \
		"    global module\n" \
		"\n" \
		"    if ch.name == srcChanName:\n" \
		"        src = ch\n" \
		"        print(f\"Subscribing {srcChanName} ..\")\n" \
		"        module.subscribeChannel(src.chanID)\n" \
		"\n" \
		"chrono_start = 0\n" \
		"\n" \
		"def onDataGrabbed(chanID: FlowChanID, data: bytearray):\n" \
		"    global src\n" \
		"    global module\n" \
		"    global chrono_start\n" \
		"\n" \
		"    # On Python, grabbing as data come, is not a good choice; because they could come very fast\n" \
		"    if (time.perf_counter() - chrono_start) < 0.50:\n" \
		"        return\n" \
		"\n" \
		"    chrono_start = time.perf_counter()\n" \
		"\n" \
		"    if chanID == src.chanID:\n" \
		"        # data is the coming content buffer as bytearray type\n" \
		"        pass\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"INPUT_CHAN\" : \"Input\",\n" \
		"    }\n" \
		"}\n"
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"App.gui.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"PublisherFlowSat.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"App.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"FlatApp.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"SubscriberFlowSat.cli.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"PublisherFlowSat.gui.template",
		0,
		nullptr
	},
	{
		true,
		"templates/CPP/Sk/Programs/",
		"SubscriberFlowSat.gui.template",
		0,
		nullptr
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.gui.template/",
		"impl",
		2280,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkObject)\n" \
		"{\n" \
		"    ui = nullptr;\n" \
		"\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    buildUI();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildUI()\n" \
		"{\n" \
		"    ui = new SkDoubleWindow(SkSize(640, 480), \"$$TITLE$$\");\n" \
		"    ui->setObjectName(\"Window\");\n" \
		"    ui->enableDestroyOnClose(false);\n" \
		"\n" \
		"    Attach(ui, closed, this, quit, SkAttachMode::SkOneShotQueued);\n" \
		"    ui->show();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, fastTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, slowTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, oneSecTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, quit)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    ObjectMessage(\"Quitting ..\");\n" \
		"    skGuiApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"TITLE\" : \"Main Window\"\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n" \
		"\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.gui.template/",
		"main",
		568,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkGuiApp *skGuiApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skGuiApp = new SkGuiApp(argc, argv);\n" \
		"    skGuiApp->setTheme(SkGuiTheme::$$GUI_THEME$$);\n" \
		"    skGuiApp->init(1, $$SLOWZONE$$, $$TICK_MODE$$, false);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skGuiApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skGuiApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\"\n" \
		"        \"GUI_THEME\" : \"FLTH_GLEAM\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.gui.template/",
		"skmake",
		938,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\", \"$$SK_SRC$$/LibSkGui\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\", \"-lfltk\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.gui.template/",
		"decl",
		825,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skguiapp.h>\n" \
		"#include <GUI/Groups/skdoublewindow.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkObject\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkObject);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(quit);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"    protected:\n" \
		"        SkDoubleWindow *ui;\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"        void buildUI();\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.gui.template/",
		"qtproj",
		411,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkGui\n" \
		"include($$SK_SRC$$/LibSkGui/LibSkGui.pri)\n" \
		"include($$SK_SRC$$/LibSkGui/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.cli.template/",
		"impl",
		2073,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkObject)\n" \
		"{\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"    SlotSet(exitFromKernelSignal);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    cout << \"Hello World\\n\";\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, fastTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, slowTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, oneSecTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, exitFromKernelSignal)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (Arg_Int != SIGINT)\n" \
		"        return;\n" \
		"\n" \
		"    ObjectMessage(\"Forcing application exit with CTRL+C\");\n" \
		"    skApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.cli.template/",
		"main",
		465,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkApp *skApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skApp = new SkApp(argc, argv);\n" \
		"    skApp->init($$FASTZONE$$, $$SLOWZONE$$, $$TICK_MODE$$);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.cli.template/",
		"skmake",
		905,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.cli.template/",
		"decl",
		781,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skapp.h>\n" \
		"#include <Core/Object/skobject.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkObject\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkObject);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"        Slot(exitFromKernelSignal);\n" \
		"\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/App.cli.template/",
		"qtproj",
		289,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/",
		"impl",
		4352,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"#include \"Core/System/skosenv.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkFlowAsync)\n" \
		"{\n" \
		"    inputChanName = \"$$INPUT_CHAN$$\";\n" \
		"    inputChan = -1;\n" \
		"\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(inputData);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"    SlotSet(onDisconnection);\n" \
		"    SlotSet(exitFromKernelSignal);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"\n" \
		"    Attach(this, channelDataAvailable, this, inputData, SkDirect);\n" \
		"    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);\n" \
		"    Attach(this, disconnected, this, onDisconnection, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    buildASync();\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::buildASync()\n" \
		"{\n" \
		"    AssertKiller(!osEnv()->existsEnvironmentVar(\"ROBOT_ADDRESS\"));\n" \
		"    SkString robotAddress = osEnv()->getEnvironmentVar(\"ROBOT_ADDRESS\");\n" \
		"\n" \
		"    if (robotAddress.startsWith(\"local:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"local:\")];\n" \
		"        ObjectWarning(\"Selected Robot LOCAL-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        AssertKiller(!localConnect(robotAddress.c_str()));\n" \
		"    }\n" \
		"\n" \
		"    else if (robotAddress.startsWith(\"tcp:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"tcp:\")];\n" \
		"        ObjectWarning(\"Selected Robot TCP-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        SkStringList robotAddressParsed;\n" \
		"        robotAddress.split(\":\", robotAddressParsed);\n" \
		"        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);\n" \
		"\n" \
		"        if (robotAddressParsed.count() == 1)\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));\n" \
		"\n" \
		"        else\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));\n" \
		"    }\n" \
		"\n" \
		"    SkString userName = \"guest\";\n" \
		"    SkString userPasswd = \"password\";\n" \
		"\n" \
		"    cout << \"\\nRobot login:\\nUsername: \";\n" \
		"    cin >> userName;\n" \
		"\n" \
		"    cout << \"Password: \";\n" \
		"    SkStdInput::unSetTermAttr(ECHO);\n" \
		"    cin >> userPasswd;\n" \
		"    SkStdInput::setTermAttr(ECHO);\n" \
		"\n" \
		"    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::onChannelAdded(SkFlowChanID chanID)\n" \
		"{\n" \
		"    SkFlowChannel *ch = channel(chanID);\n" \
		"    AssertKiller(!ch);\n" \
		"\n" \
		"    if (ch->name == inputChanName)\n" \
		"    {\n" \
		"        ObjectMessage(\"Input channel is READY\");\n" \
		"\n" \
		"        inputChan = ch->chanID;\n" \
		"        subscribeChannel(ch);\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelRemoved(SkFlowChanID chanID)\n" \
		"{\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, inputData)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (nextData())\n" \
		"    {\n" \
		"        SkFlowChannelData &d = getCurrentData();\n" \
		"\n" \
		"        if (d.chanID == inputChan)\n" \
		"        {}//d.data is the coming content buffer\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, onDisconnection)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, exitFromKernelSignal)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (Arg_Int != SIGINT)\n" \
		"        return;\n" \
		"\n" \
		"    ObjectMessage(\"Forcing application exit with CTRL+C\");\n" \
		"    skApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"INPUT_CHAN\" : \"InputChan\"\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/",
		"main",
		465,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkApp *skApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skApp = new SkApp(argc, argv);\n" \
		"    skApp->init($$FASTZONE$$, $$SLOWZONE$$, $$TICK_MODE$$);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/",
		"skmake",
		905,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/",
		"decl",
		1122,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skapp.h>\n" \
		"#include <Core/System/Network/FlowNetwork/skflowasync.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlowAsync\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkFlowAsync);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(inputData);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"        Slot(onDisconnection);\n" \
		"        Slot(exitFromKernelSignal);\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        SkString inputChanName;\n" \
		"        SkFlowChanID inputChan;\n" \
		"\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"        void buildASync();\n" \
		"\n" \
		"        void onChannelAdded(SkFlowChanID chanID)                        override;\n" \
		"        void onChannelRemoved(SkFlowChanID chanID)                      override;\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/",
		"qtproj",
		289,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/",
		"impl",
		4815,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"#include \"Core/System/skosenv.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkFlowAsync)\n" \
		"{\n" \
		"    ui = nullptr;\n" \
		"\n" \
		"    inputChanName = \"$$INPUT_CHAN$$\";\n" \
		"    inputChan = -1;\n" \
		"\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(inputData);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"    SlotSet(onDisconnection);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"\n" \
		"    Attach(this, channelDataAvailable, this, inputData, SkDirect);\n" \
		"    Attach(this, disconnected, this, onDisconnection, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    buildUI();\n" \
		"    buildASync();\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::buildASync()\n" \
		"{\n" \
		"    AssertKiller(!osEnv()->existsEnvironmentVar(\"ROBOT_ADDRESS\"));\n" \
		"    SkString robotAddress = osEnv()->getEnvironmentVar(\"ROBOT_ADDRESS\");\n" \
		"\n" \
		"    if (robotAddress.startsWith(\"local:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"local:\")];\n" \
		"        ObjectWarning(\"Selected Robot LOCAL-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        AssertKiller(!localConnect(robotAddress.c_str()));\n" \
		"    }\n" \
		"\n" \
		"    else if (robotAddress.startsWith(\"tcp:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"tcp:\")];\n" \
		"        ObjectWarning(\"Selected Robot TCP-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        SkStringList robotAddressParsed;\n" \
		"        robotAddress.split(\":\", robotAddressParsed);\n" \
		"        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);\n" \
		"\n" \
		"        if (robotAddressParsed.count() == 1)\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));\n" \
		"\n" \
		"        else\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));\n" \
		"    }\n" \
		"\n" \
		"    SkString userName = \"guest\";\n" \
		"    SkString userPasswd = \"password\";\n" \
		"\n" \
		"    cout << \"\\nRobot login:\\nUsername: \";\n" \
		"    cin >> userName;\n" \
		"\n" \
		"    cout << \"Password: \";\n" \
		"    SkStdInput::unSetTermAttr(ECHO);\n" \
		"    cin >> userPasswd;\n" \
		"    SkStdInput::setTermAttr(ECHO);\n" \
		"\n" \
		"    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildUI()\n" \
		"{\n" \
		"    ui = new SkDoubleWindow(SkSize(640, 480), \"$$TITLE$$\");\n" \
		"    ui->setObjectName(\"Window\");\n" \
		"    ui->enableDestroyOnClose(false);\n" \
		"\n" \
		"    Attach(ui, closed, this, quit, SkAttachMode::SkOneShotQueued);\n" \
		"    ui->show();\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelAdded(SkFlowChanID chanID)\n" \
		"{\n" \
		"    SkFlowChannel *ch = channel(chanID);\n" \
		"    AssertKiller(!ch);\n" \
		"\n" \
		"    if (ch->name == inputChanName)\n" \
		"    {\n" \
		"        ObjectMessage(\"Input channel is READY\");\n" \
		"\n" \
		"        inputChan = ch->chanID;\n" \
		"        subscribeChannel(ch);\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelRemoved(SkFlowChanID chanID)\n" \
		"{\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, inputData)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (nextData())\n" \
		"    {\n" \
		"        SkFlowChannelData &d = getCurrentData();\n" \
		"\n" \
		"        if (d.chanID == inputChan)\n" \
		"        {}//d.data is the coming content buffer\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    SlotImpl($$TYPE$$, fastTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (outputEnabled)\n" \
		"    {\n" \
		"        //HERE WE CAN PUBLISH\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, slowTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, oneSecTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, onDisconnection)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, quit)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    ObjectMessage(\"Quitting ..\");\n" \
		"    skGuiApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"TITLE\" : \"Main Window\",\n" \
		"        \"INPUT_CHAN\" : \"InputChan\"\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/",
		"main",
		569,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkGuiApp *skGuiApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skGuiApp = new SkGuiApp(argc, argv);\n" \
		"    skGuiApp->setTheme(SkGuiTheme::$$GUI_THEME$$);\n" \
		"    skGuiApp->init(1, $$SLOWZONE$$, $$TICK_MODE$$, false);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skGuiApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skGuiApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\",\n" \
		"        \"GUI_THEME\" : \"FLTH_GLEAM\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/",
		"skmake",
		938,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\", \"$$SK_SRC$$/LibSkGui\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\", \"-lfltk\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/",
		"decl",
		1200,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skguiapp.h>\n" \
		"#include <GUI/Groups/skdoublewindow.h>\n" \
		"#include <Core/System/Network/FlowNetwork/skflowasync.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlowAsync\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkFlowAsync);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(inputData);\n" \
		"        Slot(quit);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"        Slot(onDisconnection);\n" \
		"\n" \
		"    protected:\n" \
		"        SkDoubleWindow *ui;\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        SkString inputChanName;\n" \
		"        SkFlowChanID inputChan;\n" \
		"\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"        void buildUI();\n" \
		"        void buildASync();\n" \
		"\n" \
		"        void onChannelAdded(SkFlowChanID chanID)                        override;\n" \
		"        void onChannelRemoved(SkFlowChanID chanID)                      override;\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/",
		"qtproj",
		411,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkGui\n" \
		"include($$SK_SRC$$/LibSkGui/LibSkGui.pri)\n" \
		"include($$SK_SRC$$/LibSkGui/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/PY3/Sketches/PublisherFlowSat.cli.template/",
		"sketch",
		1704,
		"#!PySketch/sketch-executor.py\n" \
		"\n" \
		"####################################################\n" \
		"#SKETCH\n" \
		"\n" \
		"from PySketch.abstractflow import FlowChannel\n" \
		"from PySketch.flowproto import FlowChanID, Variant_T\n" \
		"from PySketch.robotmod import RobotModule\n" \
		"\n" \
		"module = None\n" \
		"\n" \
		"out = None\n" \
		"outEnabled = False\n" \
		"\n" \
		"import time\n" \
		"\n" \
		"def setup() -> bool:\n" \
		"    global module\n" \
		"\n" \
		"    module = RobotModule()\n" \
		"\n" \
		"    if not module.connect():\n" \
		"        return False\n" \
		"\n" \
		"    module.setNewChanCallBack(onChannelAdded)\n" \
		"    module.setStartChanPubReqCallBack(onPublishStartReq)\n" \
		"    module.setStopChanPubReqCallBack(onPublishStopReq)\n" \
		"\n" \
		"    print(\"Creating $$OUTPUT_CHAN$$ ..\")\n" \
		"    module.addStreamingChannel(Variant_T.T_STRING, \"$$OUTPUT_CHAN$$\")\n" \
		"\n" \
		"    return True\n" \
		"\n" \
		"def loop() -> bool:\n" \
		"    global module\n" \
		"\n" \
		"    if out is None:\n" \
		"        module.tick()\n" \
		"        return module.isConnected()\n" \
		"\n" \
		"    if outEnabled:\n" \
		"        data = b\"TEST\"\n" \
		"        module.publish(out.chanID, data)\n" \
		"\n" \
		"    # a pause is required to let free the CPU\n" \
		"    time.sleep(0.05)\n" \
		"\n" \
		"    return module.isConnected()\n" \
		"\n" \
		"####################################################\n" \
		"#CALLBACKs\n" \
		"\n" \
		"def onChannelAdded(ch: FlowChannel):\n" \
		"    global out\n" \
		"\n" \
		"    if ch.name == \"$$OUTPUT_CHAN$$\":\n" \
		"        out = ch\n" \
		"        print(f\"$$OUTPUT_CHAN$$ created [ChanID: {out.chanID}]\")\n" \
		"\n" \
		"def onPublishStartReq(ch: FlowChannel):\n" \
		"    global outEnabled\n" \
		"\n" \
		"    if ch.name == \"$$OUTPUT_CHAN$$\":\n" \
		"        outEnabled = True\n" \
		"        print(f\"Activated publish\")\n" \
		"\n" \
		"def onPublishStopReq(ch: FlowChannel):\n" \
		"    global outEnabled\n" \
		"\n" \
		"    if ch.name == \"$$OUTPUT_CHAN$$\":\n" \
		"        outEnabled = False\n" \
		"        print(f\"Deactivated publish\")\n" \
		"\n" \
		"####################################################\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"OUTPUT_CHAN\" : \"Output\",\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/FlatApp.cli.template/",
		"impl",
		455,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"$$TYPE$$::$$TYPE$$()\n" \
		"{\n" \
		"    setObjectName(\"$$TYPE$$\");\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"int $$TYPE$$::exec()\n" \
		"{\n" \
		"    FlatMessage(\"Hello World\");\n" \
		"    return 0;\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/FlatApp.cli.template/",
		"main",
		120,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    return a->exec();\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/FlatApp.cli.template/",
		"skmake",
		837,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/FlatApp.cli.template/",
		"decl",
		493,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/Object/skflatobject.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlatObject\n" \
		"{\n" \
		"    public:\n" \
		"        $$TYPE$$();\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"\n" \
		"        int exec();\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        $#PRIVATE#$\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/FlatApp.cli.template/",
		"qtproj",
		245,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkFlat\n" \
		"include($$SK_SRC$$/LibSkFlat/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/",
		"impl",
		4557,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"#include \"Core/System/skosenv.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkFlowAsync)\n" \
		"{\n" \
		"    outputChanName = \"$$OUTPUT_CHAN$$\";\n" \
		"    outputChan = -1;\n" \
		"    outputEnabled = false;\n" \
		"\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"    SlotSet(onDisconnection);\n" \
		"    SlotSet(exitFromKernelSignal);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);\n" \
		"    Attach(this, disconnected, this, onDisconnection, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    buildASync();\n" \
		"    addStreamingChannel(outputChan, T_BYTEARRAY, outputChanName.c_str());\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::buildASync()\n" \
		"{\n" \
		"    AssertKiller(!osEnv()->existsEnvironmentVar(\"ROBOT_ADDRESS\"));\n" \
		"    SkString robotAddress = osEnv()->getEnvironmentVar(\"ROBOT_ADDRESS\");\n" \
		"\n" \
		"    if (robotAddress.startsWith(\"local:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"local:\")];\n" \
		"        ObjectWarning(\"Selected Robot LOCAL-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        AssertKiller(!localConnect(robotAddress.c_str()));\n" \
		"    }\n" \
		"\n" \
		"    else if (robotAddress.startsWith(\"tcp:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"tcp:\")];\n" \
		"        ObjectWarning(\"Selected Robot TCP-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        SkStringList robotAddressParsed;\n" \
		"        robotAddress.split(\":\", robotAddressParsed);\n" \
		"        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);\n" \
		"\n" \
		"        if (robotAddressParsed.count() == 1)\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));\n" \
		"\n" \
		"        else\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));\n" \
		"    }\n" \
		"\n" \
		"    SkString userName = \"guest\";\n" \
		"    SkString userPasswd = \"password\";\n" \
		"\n" \
		"    cout << \"\\nRobot login:\\nUsername: \";\n" \
		"    cin >> userName;\n" \
		"\n" \
		"    cout << \"Password: \";\n" \
		"    SkStdInput::unSetTermAttr(ECHO);\n" \
		"    cin >> userPasswd;\n" \
		"    SkStdInput::setTermAttr(ECHO);\n" \
		"\n" \
		"    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::onChannelAdded(SkFlowChanID chanID)\n" \
		"{\n" \
		"    SkFlowChannel *ch = channel(chanID);\n" \
		"\n" \
		"    if (ch->name == outputChanName)\n" \
		"        ObjectMessage(\"Output channel is READY\");\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelRemoved(SkFlowChanID chanID)\n" \
		"{\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelPublishStartRequest(SkFlowChanID chanID)\n" \
		"{\n" \
		"    if (chanID == outputChan)\n" \
		"        outputEnabled = true;\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelPublishStopRequest(SkFlowChanID chanID)\n" \
		"{\n" \
		"    if (chanID == outputChan)\n" \
		"        outputEnabled = false;\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, fastTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (outputEnabled)\n" \
		"    {\n" \
		"        //HERE WE CAN PUBLISH\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, slowTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, oneSecTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, onDisconnection)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, exitFromKernelSignal)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (Arg_Int != SIGINT)\n" \
		"        return;\n" \
		"\n" \
		"    ObjectMessage(\"Forcing application exit with CTRL+C\");\n" \
		"    skApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"INPUT_CHAN\" : \"InputChan\"\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/",
		"main",
		534,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkGuiApp *skGuiApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skGuiApp = new SkGuiApp(argc, argv);\n" \
		"    skGuiApp->setTheme(SkGuiTheme::$$GUI_THEME$$);\n" \
		"    skApp->init($$FASTZONE$$, $$SLOWZONE$$, $$TICK_MODE$$);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skGuiApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skGuiApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/",
		"skmake",
		905,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/",
		"decl",
		1348,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skapp.h>\n" \
		"#include <Core/System/Network/FlowNetwork/skflowasync.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlowAsync\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkFlowAsync);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"        Slot(onDisconnection);\n" \
		"        Slot(exitFromKernelSignal);\n" \
		"\n" \
		"    protected:\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        SkString outputChanName;\n" \
		"        SkFlowChanID outputChan;\n" \
		"        bool outputEnabled;\n" \
		"        SkString userName;\n" \
		"        SkString userPasswd;\n" \
		"\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"        void buildASync();\n" \
		"\n" \
		"        void onChannelAdded(SkFlowChanID chanID)                        override;\n" \
		"        void onChannelRemoved(SkFlowChanID chanID)                      override;\n" \
		"\n" \
		"        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;\n" \
		"        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/",
		"qtproj",
		289,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/",
		"impl",
		4674,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"#include \"Core/System/skosenv.h\"\n" \
		"\n" \
		"ConstructorImpl($$TYPE$$, SkFlowAsync)\n" \
		"{\n" \
		"    ui = nullptr;\n" \
		"\n" \
		"    outputChanName = \"$$OUTPUT_CHAN$$\";\n" \
		"    outputChan = -1;\n" \
		"    outputEnabled = false;\n" \
		"\n" \
		"    SlotSet(init);\n" \
		"    SlotSet(fastTick);\n" \
		"    SlotSet(slowTick);\n" \
		"    SlotSet(oneSecTick);\n" \
		"    SlotSet(onDisconnection);\n" \
		"\n" \
		"    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);\n" \
		"    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);\n" \
		"    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);\n" \
		"    Attach(this, disconnected, this, onDisconnection, SkQueued);\n" \
		"\n" \
		"    $#CONSTRUCTOR_IMPL#$\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, init)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    SkStringList acceptedKeys;\n" \
		"    buildCLI(acceptedKeys);\n" \
		"    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));\n" \
		"\n" \
		"    if (osEnv()->existsAppArgument(\"--help\"))\n" \
		"    {\n" \
		"        cout << osEnv()->cmdLineHelp();\n" \
		"        skApp->quit();\n" \
		"        return;\n" \
		"    }\n" \
		"\n" \
		"    buildUI();\n" \
		"    buildASync();\n" \
		"\n" \
		"    addStreamingChannel(outputChan, T_BYTEARRAY, outputChanName.c_str());\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::buildASync()\n" \
		"{\n" \
		"    AssertKiller(!osEnv()->existsEnvironmentVar(\"ROBOT_ADDRESS\"));\n" \
		"    SkString robotAddress = osEnv()->getEnvironmentVar(\"ROBOT_ADDRESS\");\n" \
		"\n" \
		"    if (robotAddress.startsWith(\"local:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"local:\")];\n" \
		"        ObjectWarning(\"Selected Robot LOCAL-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        AssertKiller(!localConnect(robotAddress.c_str()));\n" \
		"    }\n" \
		"\n" \
		"    else if (robotAddress.startsWith(\"tcp:\"))\n" \
		"    {\n" \
		"        robotAddress = &robotAddress.c_str()[strlen(\"tcp:\")];\n" \
		"        ObjectWarning(\"Selected Robot TCP-address [$ROBOT_ADDRESS]: \" << robotAddress);\n" \
		"\n" \
		"        SkStringList robotAddressParsed;\n" \
		"        robotAddress.split(\":\", robotAddressParsed);\n" \
		"        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);\n" \
		"\n" \
		"        if (robotAddressParsed.count() == 1)\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));\n" \
		"\n" \
		"        else\n" \
		"            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));\n" \
		"    }\n" \
		"\n" \
		"    SkString userName = \"guest\";\n" \
		"    SkString userPasswd = \"password\";\n" \
		"\n" \
		"    cout << \"\\nRobot login:\\nUsername: \";\n" \
		"    cin >> userName;\n" \
		"\n" \
		"    cout << \"Password: \";\n" \
		"    SkStdInput::unSetTermAttr(ECHO);\n" \
		"    cin >> userPasswd;\n" \
		"    SkStdInput::setTermAttr(ECHO);\n" \
		"\n" \
		"    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildUI()\n" \
		"{\n" \
		"    ui = new SkDoubleWindow(SkSize(640, 480), \"$$TITLE$$\");\n" \
		"    ui->setObjectName(\"Window\");\n" \
		"    ui->enableDestroyOnClose(false);\n" \
		"\n" \
		"    Attach(ui, closed, this, quit, SkAttachMode::SkOneShotQueued);\n" \
		"    ui->show();\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelAdded(SkFlowChanID chanID)\n" \
		"{\n" \
		"    SkFlowChannel *ch = channel(chanID);\n" \
		"\n" \
		"    if (ch->name == outputChanName)\n" \
		"        ObjectMessage(\"Output channel is READY\");\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelRemoved(SkFlowChanID chanID)\n" \
		"{\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelPublishStartRequest(SkFlowChanID chanID)\n" \
		"{\n" \
		"    if (chanID == outputChan)\n" \
		"        outputEnabled = true;\n" \
		"}\n" \
		"\n" \
		"void $$TYPE$$::onChannelPublishStopRequest(SkFlowChanID chanID)\n" \
		"{\n" \
		"    if (chanID == outputChan)\n" \
		"        outputEnabled = false;\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"$#IMPL#$\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, fastTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    if (outputEnabled)\n" \
		"    {\n" \
		"        //HERE WE CAN PUBLISH\n" \
		"    }\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, slowTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, oneSecTick)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"SlotImpl($$TYPE$$, onDisconnection)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"}\n" \
		"\n" \
		"SlotImpl($$TYPE$$, quit)\n" \
		"{\n" \
		"    SilentSlotArgsWarning();\n" \
		"\n" \
		"    ObjectMessage(\"Quitting ..\");\n" \
		"    skGuiApp->quit();\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)\n" \
		"{\n" \
		"    acceptedKeys.append(\"--help\");\n" \
		"\n" \
		"    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"    osEnv()->addCliAppHelp(\"--help\", \"prints this output and exit\");\n" \
		"}\n" \
		"\n" \
		"//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //\n" \
		"\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"TITLE\" : \"Main Window\",\n" \
		"        \"INPUT_CHAN\" : \"InputChan\"\n" \
		"    },\n" \
		"   \"defaultsForBlocks\" : {\n" \
		"        \"CONSTRUCTOR_IMPL\" : \"\",\n" \
		"        \"IMPL\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/",
		"main",
		569,
		"#include \"$$LOWER_TYPE$$.h\"\n" \
		"\n" \
		"SkGuiApp *skGuiApp = nullptr;\n" \
		"\n" \
		"int main(int argc, char *argv[])\n" \
		"{\n" \
		"    skGuiApp = new SkGuiApp(argc, argv);\n" \
		"    skGuiApp->setTheme(SkGuiTheme::$$GUI_THEME$$);\n" \
		"    skGuiApp->init(1, $$SLOWZONE$$, $$TICK_MODE$$, false);\n" \
		"\n" \
		"    $$TYPE$$ *a = new $$TYPE$$;\n" \
		"    Attach(skGuiApp->started_SIG, pulse, a, init, SkOneShotQueued);\n" \
		"\n" \
		"    return skGuiApp->exec();\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"FASTZONE\" : \"10000\",\n" \
		"        \"SLOWZONE\" : \"250000\",\n" \
		"        \"TICK_MODE\" : \"SK_TIMEDLOOP_SLEEPING\",\n" \
		"        \"GUI_THEME\" : \"FLTH_GLEAM\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/",
		"skmake",
		938,
		"{\n" \
		"    \"target\" : \"$$BINARY_NAME$$.bin\",\n" \
		"    \"platform\" : \"$$ARCH$$\",\n" \
		"    \"compiler\" : \"$$COMPILER$$\",\n" \
		"    \"buildPath\" : \"$$BUILD_DIR$$\",\n" \
		"    \"debugger\" : \"$$DEBUGGER$$\",\n" \
		"    \"buildOptions\" : $$BUILD_OPT$$,\n" \
		"    \"sourcesPaths\" : [\"$$SK_SRC$$/LibSkFlat\", \"$$SK_SRC$$/LibSkCore\", \"$$SK_SRC$$/LibSkGui\"],\n" \
		"    \"includeOptPaths\" : $$INCLUDE_OPT_PATHS$$,\n" \
		"    \"dynamicDeps\" : $$DYN_DEPS$$,\n" \
		"    \"defines\" : $$DEFINES$$,\n" \
		"    \"libs\" : $$LIBS$$\n" \
		"}\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"        \"BINARY_NAME\" : \"out\",\n" \
		"        \"ARCH\" : \"X86_64\",\n" \
		"        \"COMPILER\" : \"/usr/bin/g++\",\n" \
		"        \"DEBUGGER\" : \"/usr/bin/gdb\",\n" \
		"        \"BUILD_DIR\" : \"./build\",\n" \
		"        \"BUILD_OPT\" : [\"-pipe\", \"-g\", \"-std=gnu++11\", \"-W\", \"-fPIC\"],\n" \
		"        \"INCLUDE_OPT_PATHS\" : [],\n" \
		"        \"DYN_DEPS\" : [],\n" \
		"        \"DEFINES\" : [\"-DENABLE_SKAPP\", \"-DSPECIALK_APPLICATION\", \"-DENABLE_AES\"],\n" \
		"        \"LIBS\" :  [\"-pthread\", \"-L/usr/lib\", \"-lm\", \"-lz\", \"-lcrypto\", \"-lfltk\"]\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/",
		"decl",
		1371,
		"#ifndef $$UPPER_TYPE$$_H\n" \
		"#define $$UPPER_TYPE$$_H\n" \
		"\n" \
		"#include <Core/App/skguiapp.h>\n" \
		"#include <GUI/Groups/skdoublewindow.h>\n" \
		"#include <Core/System/Network/FlowNetwork/skflowasync.h>\n" \
		"\n" \
		"$#HEADER#$\n" \
		"\n" \
		"class $$TYPE$$ extends SkFlowAsync\n" \
		"{\n" \
		"    public:\n" \
		"        Constructor($$TYPE$$, SkFlowAsync);\n" \
		"\n" \
		"        $#PUBLIC#$\n" \
		"        $#SLOTS#$\n" \
		"        $#SIGNALS#$\n" \
		"\n" \
		"        Slot(init);\n" \
		"        Slot(quit);\n" \
		"        Slot(fastTick);\n" \
		"        Slot(slowTick);\n" \
		"        Slot(oneSecTick);\n" \
		"        Slot(onDisconnection);\n" \
		"\n" \
		"    protected:\n" \
		"        SkDoubleWindow *ui;\n" \
		"        $#PROTECTED#$\n" \
		"\n" \
		"    private:\n" \
		"        SkString outputChanName;\n" \
		"        SkFlowChanID outputChan;\n" \
		"        bool outputEnabled;\n" \
		"\n" \
		"        $#PRIVATE#$\n" \
		"\n" \
		"        void buildCLI(SkStringList &acceptedKeys);\n" \
		"        void buildUI();\n" \
		"        void buildASync();\n" \
		"\n" \
		"        void onChannelAdded(SkFlowChanID chanID)                        override;\n" \
		"        void onChannelRemoved(SkFlowChanID chanID)                      override;\n" \
		"\n" \
		"        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;\n" \
		"        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;\n" \
		"\n" \
		"};\n" \
		"\n" \
		"#endif // $$UPPER_TYPE$$_H\n" \
		"\n" \
		"$%$\n" \
		"\n" \
		"{\n" \
		"    \"defaultsForVariables\" : {\n" \
		"    },\n" \
		"    \"defaultsForBlocks\" : {\n" \
		"        \"HEADER\" : \"\",\n" \
		"        \"SLOTS\" : \"\",\n" \
		"        \"SIGNALS\" : \"\",\n" \
		"        \"PUBLIC\" : \"\",\n" \
		"        \"PROTECTED\" : \"\",\n" \
		"        \"PRIVATE\" : \"\"\n" \
		"    }\n" \
		"}\n"
	},
	{
		false,
		"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/",
		"qtproj",
		411,
		"TARGET = $$BINARY_NAME$$.bin\n" \
		"TEMPLATE = app\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkCore\n" \
		"include($$SK_SRC$$/LibSkCore/LibSkCore.pri)\n" \
		"include($$SK_SRC$$/LibSkCore/module-src.pri)\n" \
		"\n" \
		"INCLUDEPATH += $$SK_SRC$$/LibSkGui\n" \
		"include($$SK_SRC$$/LibSkGui/LibSkGui.pri)\n" \
		"include($$SK_SRC$$/LibSkGui/module-src.pri)\n" \
		"\n" \
		"SOURCES += \\\n" \
		"    $$LOWER_TYPE$$.cpp \\\n" \
		"    main.cpp\n" \
		"\n" \
		"HEADERS += \\\n" \
		"    $$LOWER_TYPE$$.h\n" \
		"\n" \
		"DISTFILES += \\\n" \
		"    skmake.json\n"
	}
};

#define TEMPLATES_INDEX	"{\"templates/CPP\":0,\"templates/CPP/Pieces\":17,\"templates/CPP/Qt\":18,\"templates/CPP/Sk\":16,\"templates/CPP/Sk/Classes\":20,\"templates/CPP/Sk/Classes/FlatClass.cli.template\":23,\"templates/CPP/Sk/Classes/FlatClass.cli.template/decl\":31,\"templates/CPP/Sk/Classes/FlatClass.cli.template/impl\":30,\"templates/CPP/Sk/Classes/ObjectClass.cli.template\":22,\"templates/CPP/Sk/Classes/ObjectClass.cli.template/decl\":25,\"templates/CPP/Sk/Classes/ObjectClass.cli.template/impl\":24,\"templates/CPP/Sk/Pieces\":21,\"templates/CPP/Sk/Programs\":19,\"templates/CPP/Sk/Programs/App.cli.template\":35,\"templates/CPP/Sk/Programs/App.cli.template/decl\":48,\"templates/CPP/Sk/Programs/App.cli.template/impl\":45,\"templates/CPP/Sk/Programs/App.cli.template/main\":46,\"templates/CPP/Sk/Programs/App.cli.template/qtproj\":49,\"templates/CPP/Sk/Programs/App.cli.template/skmake\":47,\"templates/CPP/Sk/Programs/App.gui.template\":33,\"templates/CPP/Sk/Programs/App.gui.template/decl\":43,\"templates/CPP/Sk/Programs/App.gui.template/impl\":40,\"templates/CPP/Sk/Programs/App.gui.template/main\":41,\"templates/CPP/Sk/Programs/App.gui.template/qtproj\":44,\"templates/CPP/Sk/Programs/App.gui.template/skmake\":42,\"templates/CPP/Sk/Programs/FlatApp.cli.template\":36,\"templates/CPP/Sk/Programs/FlatApp.cli.template/decl\":64,\"templates/CPP/Sk/Programs/FlatApp.cli.template/impl\":61,\"templates/CPP/Sk/Programs/FlatApp.cli.template/main\":62,\"templates/CPP/Sk/Programs/FlatApp.cli.template/qtproj\":65,\"templates/CPP/Sk/Programs/FlatApp.cli.template/skmake\":63,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template\":34,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/decl\":69,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/impl\":66,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/main\":67,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/qtproj\":70,\"templates/CPP/Sk/Programs/PublisherFlowSat.cli.template/skmake\":68,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template\":38,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/decl\":74,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/impl\":71,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/main\":72,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/qtproj\":75,\"templates/CPP/Sk/Programs/PublisherFlowSat.gui.template/skmake\":73,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template\":37,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/decl\":53,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/impl\":50,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/main\":51,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/qtproj\":54,\"templates/CPP/Sk/Programs/SubscriberFlowSat.cli.template/skmake\":52,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template\":39,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/decl\":58,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/impl\":55,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/main\":56,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/qtproj\":59,\"templates/CPP/Sk/Programs/SubscriberFlowSat.gui.template/skmake\":57,\"templates/PY3\":1,\"templates/PY3/Classes\":3,\"templates/PY3/Classes/Class.cli.template\":7,\"templates/PY3/Classes/Class.cli.template/impl\":15,\"templates/PY3/Pieces\":4,\"templates/PY3/Programs\":2,\"templates/PY3/Programs/BaseProgram.cli.template\":26,\"templates/PY3/Programs/BaseProgram.cli.template/main\":27,\"templates/PY3/PySketch.lib\":5,\"templates/PY3/PySketch.lib/abstractflow.py\":12,\"templates/PY3/PySketch.lib/flowasync.py\":10,\"templates/PY3/PySketch.lib/flowproto.py\":9,\"templates/PY3/PySketch.lib/flowsync.py\":14,\"templates/PY3/PySketch.lib/robotmod.py\":8,\"templates/PY3/PySketch.lib/sketch-executor.py\":11,\"templates/PY3/PySketch.lib/socketdevice.py\":13,\"templates/PY3/Sketches\":6,\"templates/PY3/Sketches/PublisherFlowSat.cli.template\":28,\"templates/PY3/Sketches/PublisherFlowSat.cli.template/sketch\":60,\"templates/PY3/Sketches/SubscriberFlowSat.cli.template\":29,\"templates/PY3/Sketches/SubscriberFlowSat.cli.template/sketch\":32}"

#endif // TEMPLATES_H
