#include "$$LOWER_TYPE$$.h"
#include "Core/System/skosenv.h"

ConstructorImpl($$TYPE$$, SkFlowAsync)
{
    inputChanName = "$$INPUT_CHAN$$";
    inputChan = -1;

    SlotSet(init);
    SlotSet(inputData);
    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
    SlotSet(onDisconnection);
    SlotSet(exitFromKernelSignal);

    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);

    Attach(this, channelDataAvailable, this, inputData, SkDirect);
    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
    Attach(this, disconnected, this, onDisconnection, SkQueued);

    $#CONSTRUCTOR_IMPL#$
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl($$TYPE$$, init)
{
    SilentSlotArgsWarning();

    SkStringList acceptedKeys;
    buildCLI(acceptedKeys);
    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));

    if (osEnv()->existsAppArgument("--help"))
    {
        cout << osEnv()->cmdLineHelp();
        skApp->quit();
        return;
    }

    buildASync();
}

void $$TYPE$$::buildASync()
{
    AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
    SkString robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);

        AssertKiller(!localConnect(robotAddress.c_str()));
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        if (robotAddressParsed.count() == 1)
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));

        else
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));
    }

    SkString userName = "guest";
    SkString userPasswd = "password";

    cout << "\nRobot login:\nUsername: ";
    cin >> userName;

    cout << "Password: ";
    SkStdInput::unSetTermAttr(ECHO);
    cin >> userPasswd;
    SkStdInput::setTermAttr(ECHO);

    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void $$TYPE$$::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == inputChanName)
    {
        ObjectMessage("Input channel is READY");

        inputChan = ch->chanID;
        subscribeChannel(ch);
    }
}

void $$TYPE$$::onChannelRemoved(SkFlowChanID chanID)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

$#IMPL#$

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl($$TYPE$$, inputData)
{
    SilentSlotArgsWarning();

    if (nextData())
    {
        SkFlowChannelData &d = getCurrentData();

        if (d.chanID == inputChan)
        {}//d.data is the coming content buffer
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl($$TYPE$$, onDisconnection)
{
    SilentSlotArgsWarning();
}

SlotImpl($$TYPE$$, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (Arg_Int != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void $$TYPE$$::buildCLI(SkStringList &acceptedKeys)
{
    acceptedKeys.append("--help");

    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

    osEnv()->addCliAppHelp("--help", "prints this output and exit");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

$%$

{
    "defaultsForVariables" : {
        "INPUT_CHAN" : "InputChan"
    },
    "defaultsForBlocks" : {
        "CONSTRUCTOR_IMPL" : "",
        "IMPL" : ""
    }
}
