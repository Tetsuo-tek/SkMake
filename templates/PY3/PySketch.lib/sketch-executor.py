#!/usr/bin/env python3

import sys
import os

if __name__ == "__main__":

    #########################

    def svltLoop() -> None:
        setup_function = None
        loop_function = None
    
        if len(sys.argv) < 2:
            print("Sketch path NOT specified")
            return

        sketchPath = sys.argv[1]

        if not os.path.isfile(sketchPath):
            print(f"Sketch NOT found: '{sketchPath}'")
            return
        
        sketchAbsPath = os.path.abspath(sketchPath)
        print(f"Importing sketch: '{sketchAbsPath}'.")

        sys.path.append(os.path.dirname(sketchAbsPath))

        try:
            sketchName = os.path.splitext(os.path.basename(sketchPath))[0]
            sketch = __import__(sketchName)

            if not hasattr(sketch, "setup"):
                print("setup() function NOT found!")
                return
            
            setup_function = getattr(sketch, "setup")

            if not hasattr(sketch, "loop"):
                print("loop() function NOT found!")
                return
                
            loop_function = getattr(sketch, "loop")
            
            close_function = None

            if hasattr(sketch, "close"):
                close_function = getattr(sketch, "close")

        except (ImportError, AttributeError):
            print(f"CANNOT import the Sketch from: '{sketchPath}'.")
            return

        try:
            if not setup_function():
                return
            
            print(f"Sketch STARTED: {sketchName}")

            while True:
                if not loop_function():
                    break

            print("Closing ..")


        except KeyboardInterrupt:
            print("Forcing exit with CTRL-C")

        finally:
            if close_function is not None:
                close_function()

            print("\nHALTED")
            
    #########################

    svltLoop()

####################################################