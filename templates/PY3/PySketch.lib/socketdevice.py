import socket
import select
import fcntl
import termios
import sys
import errno
import ctypes
import struct

####################################################

class SocketDevice:
    def __init__(self):
        self.sock = None

    #unix local connection
    def localConnect(self, path: str) -> bool:
        try:
            self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.sock.connect(path)
            return True

        except socket.error as e:
            print("Connection failed: {}".format(e))
        
        return False
    
    #tcp/ip connection
    def tcpConnect(self, host: str, port: ctypes.c_uint16) -> bool:
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((host, port))
            return True

        except socket.error as e:
            print("Connection failed: {}".format(e))

        return False
    
    def disconnect(self):
        try:
            if self.sock is not None:
                self.sock.close()
                #self.sock = None

        except socket.error as e:
            print(f"Disconnect failed: {e}")
        
    def isConnected(self):
        try:
            data = self.sock.recv(1, socket.MSG_PEEK | socket.MSG_DONTWAIT)
            
            if len(data) == 0 and errno != errno.EAGAIN:
                return False

        except socket.error as e:
            err = e.args[0]

            if err == errno.EAGAIN:
                return True
            
            else:
                print('Socket error: {}'.format(str(e)))
                return False
        
        return True
    
####################################################

    def canWrite(self):
        try:
            r, w, e = select.select([], [self.sock], [self.sock], 0)
            
            if self.sock in w:
                return True
            
            elif self.sock in e:
                print('Error occurred on socket')
        
        except select.error as e:
            print(f'select failed: {e}')

        return False
    
    def waitForReadyRead(self, timeout=0.2):
        try:
            r, w, e = select.select([self.sock], [], [self.sock], timeout)
            
            if self.sock in r:
                return True
            
            elif self.sock in e:
                print('Error occurred on socket')
        
        except select.error as e:
            print(f'select failed: {e}')

        return False

####################################################

    def bytesAvailable(self) -> int:
        try:
            # Ottiene la quantità di dati disponibili sul socket
            bytes_available = fcntl.ioctl(self.sock.fileno(), termios.FIONREAD, bytes(4))
            return int.from_bytes(bytes_available, byteorder=sys.byteorder)

        except socket.error as e:
            print(f"Cannot get bytes available: {e}")

        return 0
    
####################################################

    def readString(self, sz: int = 0) -> str:
        return self.read(sz).decode('utf-8')

    def writeString(self, text: str, sz: int = 0) -> None:
        encoded = text.encode('utf-8')
        self.write(encoded, sz)

####################################################

    def readUInt8(self) -> ctypes.c_uint8:
        byte = self.read(1)
        val, = struct.unpack('<B', byte)
        return val

    def readInt8(self) -> ctypes.c_int8:
        byte = self.read(1)
        val, = struct.unpack('<b', byte)
        return val

    def readUInt16(self) -> ctypes.c_uint16:
        bytes = self.read(2)
        val, = struct.unpack('<H', bytes)
        return val

    def readInt16(self) -> ctypes.c_int16:
        bytes = self.read(2)
        val, = struct.unpack('<h', bytes)
        return val

    def readUInt32(self) -> ctypes.c_uint32:
        bytes = self.read(4)
        val, = struct.unpack('<I', bytes)
        return val

    def readInt32(self) -> ctypes.c_int32:
        bytes = self.read(4)
        val, = struct.unpack('<i', bytes)
        return val

    def readUInt64(self) -> ctypes.c_uint64:
        bytes = self.read(4)
        val, = struct.unpack('<Q', bytes)
        return val

    def readInt64(self) -> ctypes.c_int64:
        bytes = self.read(4)
        val, = struct.unpack('<q', bytes)
        return val

    def readFloat(self) -> ctypes.c_float:
        bytes = self.read(4)
        val, = struct.unpack('<f', bytes)
        return val

    def readDouble(self) -> ctypes.c_double:
        bytes = self.read(8)
        val, = struct.unpack('<d', bytes)
        return val

####################################################

    def writeUInt8(self, val: ctypes.c_uint8) -> None:
        binary_data = struct.pack("<B", val)
        self.write(binary_data)

    def writeInt8(self, val: ctypes.c_int8) -> None:
        binary_data = struct.pack("<b", val)
        self.write(binary_data)

    def writeUInt16(self, val: ctypes.c_uint16) -> None:
        binary_data = struct.pack("<H", val)
        self.write(binary_data)

    def writeInt16(self, val: ctypes.c_int16) -> None:
        binary_data = struct.pack("<h", val)
        self.write(binary_data)

    def writeUInt32(self, val: ctypes.c_uint32) -> None:
        binary_data = struct.pack("<I", val)
        self.write(binary_data)

    def writeInt32(self, val: ctypes.c_int32) -> None:
        binary_data = struct.pack("<i", val)
        self.write(binary_data)

    def writeUInt64(self, val: ctypes.c_uint64) -> None:
        binary_data = struct.pack("<Q", val)
        self.write(binary_data)

    def writeInt64(self, val: ctypes.c_int64) -> None:
        binary_data = struct.pack("<q", val)
        self.write(binary_data)

    def writeFloat(self, val: ctypes.c_float) -> None:
        binary_data = struct.pack("<f", val)
        self.write(binary_data)

    def writeDouble(self, val: ctypes.c_double) -> None:
        binary_data = struct.pack("<d", val)
        self.write(binary_data)

####################################################

    def read(self, sz: int) -> bytearray:
        if sz == 0:
            #sz = self.bytesAvailable();
            print(f"Read failed [sz==0]")
            return bytearray()
        
        try:
            data = bytearray()
            remaining = sz

            while remaining > 0:
                chunk = self.sock.recv(remaining)

                if not chunk:
                    print("CANNOT read chunk")
                    break

                data += chunk
                remaining -= len(chunk)

            return data
        
        except socket.error as e:
            print(f"Read failed: {e}")

        return bytearray()

    def write(self, data: bytearray) -> bool: #int:
        try:
            self.sock.sendall(data)
            return True

        except socket.error as e:
            print(f"Write failed: {e}")
        
        return False

####################################################

