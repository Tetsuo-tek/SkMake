
import ctypes

from typing import Any
from flowproto import FlowCommand, FlowChanID, FlowChannel_T
from abstractflow import FlowChannel, AbstractFlow, ProtocolRecvError

####################################################

class FlowSync(AbstractFlow):
    def __init__(self) -> None:
        super().__init__()

    def existsOptionalPairDb(self, dbName: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()
        exists: bool = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False

        return exists

    def addDatabase(self, dbName: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        ok: bool = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False
        
        return ok
    
    def getCurrentDbName(self) -> str:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return ""
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_DATABASE)
        self._p.sendEndOfTransaction()
        
        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        sz: ctypes.c_uint32 = self._p.recvSize()
        dbName = self._p.recvString(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return ""
        
        return dbName

    def variablesKeys(self) -> list:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return []
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_KEYS)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()
        keys: list = self._p.recvList()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return []
        
        return keys

    def getAllVariables(self) -> dict:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return {}
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_JSON)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        sz: ctypes.c_uint32 = self._p.recvSize()
        var = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return {}

        return var

    def existsVariable(self, key: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_VARIABLE)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        exists: bool = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False
        
        return exists

    def getVariable(self, key: str) -> Any:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return Any()
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE_JSON)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        sz: ctypes.c_uint32 = self._p.recvSize()
        var = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return Any
        
        return var

    def dataGrabbingRegister(self, chanID: FlowChanID) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_REGISTER_CHAN)
        self._p.sendChanID(chanID)
        
        # NO RESPONSE
        return self._p.sendEndOfTransaction()

    def dataGrabbingUnRegister(self, chanID: FlowChanID) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_UNREGISTER_CHAN)
        self._p.sendChanID(chanID)
        
        # NO RESPONSE
        return self._p.sendEndOfTransaction()

    def grabLastChannelData(self, chanID: FlowChanID) -> bytearray:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return bytearray()
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE)
        self._p.sendString(chanID)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        sz: ctypes.c_uint32 = self._p.recvSize()
        data: bytearray = self._p.recvBuffer(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return bytearray()
        
        return data

####################################################

    def updateChannels(self) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDevice is NOT initialized yet")
            return False
        
        if len(self._channelsNames) > 0:
            self._resetChannels()
        
        chansNames = self._getChannelsList()

        for chanName in chansNames:
            ch: FlowChannel = self._getChannelProperties(chanName)
            self._channelsNames[ch.name] = ch
            self._channelsIndexes[ch.chanID] = ch

        return True

    def _getChannelsList(self) -> list:
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHANS_LIST)
        self._p.sendEndOfTransaction();
        
        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()
        chansNames: list = self._p.recvList()

        if len(chansNames) == 0:
            print("Channels list is EMPTY")

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return []
            
        return chansNames

    def _getChannelProperties(self, name: str) -> FlowChannel:
        ch: FlowChannel = FlowChannel()
        ch.isPublishingEnabled = False

        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHAN_PROPS)
        self._p.sendString(name)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()
        
        ch.chan_t = self._p.recvChanType()
        ch.chanID = self._p.recvChanID()

        sz: ctypes.c_uint32 = self._p.recvSize()
        ch.hashID = self._p.recvString(sz)

        if ch.chan_t == FlowChannel_T.StreamingChannel:
            sz = self._p.recvSize()
            ch.udm = self._p.recvString(sz)

            sz = self._p.recvSize()
            ch.mime = self._p.recvString(sz)

            ch.t = self._p.recvVariantType()

            sz = self._p.recvSize()
            ch.min = self._p.recvJSON(sz)

            sz = self._p.recvSize()
            ch.max = self._p.recvJSON(sz)

            sz = self._p.recvSize()
            ch.hasHeader = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return FlowChannel()
        
        return ch

    def _getChannelHeader(self) -> bytearray:
        return bytearray()

####################################################

    def _onOpenUnixSocket(self) -> bool:
        return True

    def _onOpenTcpSocket(self) -> bool:
        return True
    
    def _onLogin(self) -> bool:
        return True

    def _onOpen(self) -> None:
        pass

    def _onClose(self) -> None:
        pass

    def _onDisconnected(self) -> None:
        pass

    def _onTick(self) -> None:
        pass

####################################################
