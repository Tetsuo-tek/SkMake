import ctypes

from socketdevice import SocketDevice
from flowproto import FlowChanID, FlowChannel_T, FlowCommand, FlowProto, Variant_T
from abc import ABC, abstractmethod

####################################################

def ProtocolRecvError(sck: SocketDevice, msg: str) -> None:
    print("[FATAL] =>", msg)
    if sck.isConnected():
        sck.disconnect()

####################################################

class FlowChannel:
    def __init__(self) -> None:
        self.chan_t: FlowChannel_T = FlowChannel_T.ChannelNotValid
        self.chanID: FlowChanID = -1
        self.hashID: str = ""
        self.name: str = ""
        self.udm: str = ""
        self.mime: str = ""
        self.t: Variant_T = Variant_T.T_NULL
        self.min = None
        self.max = None
        self.hasHeader: bool = False
        self.isPublishingEnabled: bool = False
        self.header: bytearray = b""
        self.mpPath: str = ""

####################################################

class AbstractFlow(ABC):
    def __init__(self) -> None:
        self._sck = None
        self._unixPath = ""
        self._tcpAddress = ""
        self._tcpPort = 0
        self._userName = ""
        self._userToken = ""
        self._isAuthorized = False
        self._channelsNames = {}
        self._channelsIndexes = {}
        self._p = FlowProto()

####################################################

    def localConnect(self, path: str) -> bool:
        if self._sck is not None:
            print("SocketDecice is ALREADY initialized")
            return False
        
        self._sck = SocketDevice()

        if not self._sck.localConnect(path):
            self._sck = None
            print("CANNOT connect to FsService:", path)
            return False

        self._unixPath = path
        self._tcpAddress = ""
        self._tcpPort = 0

        self._open()
        return True

    def tcpConnect(self, host: str, port: ctypes.c_uint16) -> bool:
        if self._sck is not None:
            print("SocketDecice is ALREADY initialized")
            return False
        
        self._sck = SocketDevice()

        if not self._sck.tcpConnect(host, port):
            self._sck = None
            print("CANNOT connect to FsService: {}:{}".format(host, port))
            return False

        self._tcpAddress = host
        self._tcpPort = port
        self._unixPath = ""

        self._open()
        return True

    def _open(self) -> None:
        if self.channelsCount() > 0:
            self._resetChannels()

        if self.isUnixSocket():
            self._onOpenUnixSocket()
        else:
            self._onOpenTcpSocket()

        self._p.setup(self._sck)

        self._onOpen()
        #print("Connected")

    def _resetChannels(self) -> None:
        self._channelsNames = {}
        self._channelsIndexes = {}

    def close(self) -> None:
        if self._sck is None:
            print("SocketDecice is NOT initialized yet")
            return
        
        self._sck.disconnect()
        self._onClose()

        #print("Disconnected") < da inserire nel tick all'interno di onDisconnected()
    
####################################################

    def tick(self) -> None:
        if self._sck is None:
            return
        
        if not self._sck.isConnected():
            self._onDisconnected()
            self._sck = None
            return
        
        self._sck.waitForReadyRead(0.002)
        self._onTick()

####################################################

    def login(self, userName: str, userToken: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False

        self._p.sendStartOfTransaction(FlowCommand.FCMD_LOGIN)
        self._p.sendString(userName)
        self._p.sendString(userToken)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd: FlowCommand = self._p.recvStartOfCmdTransaction()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False

        self._userName = userName
        self._userToken = userToken
        self._isAuthorized = True

        return self._onLogin()


####################################################

    def setCurrentDbName(self, dbName: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

    def setVariable(self, key: str, val) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_VARIABLE_JSON)
        self._p.sendString(key)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

        #NO RESPONSE
        return True
    
    def delVariable(self, key: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_VARIABLE)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

    def flushAll(self) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_FLUSHALL)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

####################################################

    def isUnixSocket(self) -> bool:
        return (len(self._unixPath) > 0)

    def isTcpSocket(self) -> bool:
        return (self._tcpPort != 0)

    def getUnixPath(self) -> str:
        return self._unixPath

    def getTcpAddress(self) -> str:
        return self._tcpAddress

    def getTcpPort(self) -> ctypes.c_uint16:
        return self._tcpPort

####################################################

    def isConnected(self) -> bool:
        if self._sck is None:
            return False
        
        return self._sck.isConnected()

    def isAuthorized(self) -> bool:
        return self._isAuthorized

####################################################

    def containsChannel(self, name: str) -> bool:
        return (name in self._channelsNames)

    def channels(self) -> list:
        return list(self._channelsNames.keys())

    def channelByName(self, name: str) -> FlowChannel:
        try:
            return self._channelsNames[name]
        except KeyError:
            return None

    def channelByID(self, chanID: FlowChanID) -> FlowChannel:
        try:
            return self._channelsIndexes[chanID]
        except KeyError:
            return None

    def channelID(self, name: str) -> FlowChanID:
        if name in self._channelsNames:
            print("Channel UNKNOWN:", name)
            return -1
        
        ch: FlowChannel = self._channelsNames[name]
        return ch.chanID

    def channelsCount(self) -> int:
        return len(self._channelsIndexes)

####################################################

    @abstractmethod
    def _onOpenUnixSocket(self) -> bool:
        pass

    @abstractmethod
    def _onOpenTcpSocket(self) -> bool:
        pass
    
    @abstractmethod
    def _onLogin(self) -> bool:
        return

    @abstractmethod
    def _onOpen(self) -> None:
        pass

    @abstractmethod
    def _onClose(self) -> None:
        pass

    @abstractmethod
    def _onDisconnected(self) -> None:
        pass

    @abstractmethod
    def _onTick(self) -> None:
        pass

####################################################