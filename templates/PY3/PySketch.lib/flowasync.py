import ctypes
import struct
import json

from collections import deque
from flowproto import FlowResponse, FlowCommand, FlowChanID, FlowChannel_T, Variant_T
from abstractflow import FlowChannel, AbstractFlow, ProtocolRecvError

####################################################

class FlowChannelData:
    def __init__(self) -> None:
        self.chanID: FlowChanID = -1
        self.data: bytearray = b""

####################################################

class FlowAsync(AbstractFlow):
    def __init__(self) -> None:
        super().__init__()
        self._maxDataQueueCount = 1000
        self._subscribedChannels = []
        self._channelsData = deque()
        self._currentData = FlowChannelData()
        self._lastResponse = FlowResponse.FRSP_NORSP
        self._currentDbName = ""
        self._onNewChanCb = None
        self._onDelChanCb = None
        self._onStartChanPubReqCb = None
        self._onStopChanPubReqCb = None
        self._onGrabDataCb = None
        self._onServiceRequest = None

####################################################

    def setNewChanCallBack(self, cb) -> None:
        self._onNewChanCb = cb

    def setDelChanCallBack(self, cb) -> None:
        self._onDelChanCb = cb

    def setStartChanPubReqCallBack(self, cb) -> None:
        self._onStartChanPubReqCb = cb

    def setStopChanPubReqCallBack(self, cb) -> None:
        self._onStopChanPubReqCb = cb

    def setGrabDataCallBack(self, cb) -> None:
        self._onGrabDataCb = cb

    def setRequestCallBack(self, cb) -> None:
        self._onServiceRequest = cb

####################################################

    def checkService(self) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_ASYNC)
        self._p.sendEndOfTransaction();

        # NO RESPONSE
        return True

####################################################

    def addStreamingChannel(self, t: Variant_T, name: str, mime: str="", udm: str="") -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        l = []
        l.append(t.value)
        l.append(name)
        l.append(mime)
        l.append(udm)
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_STREAMING_CHAN)
        self._p.sendJSON(l)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"REQUESTED to ADD a new StreamingChannel: {name}")
        return True
    
    def addServiceChannel(self, name: str) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        l = []
        l.append(name)

        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_SERVICE_CHAN)
        self._p.sendJSON(l)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"REQUESTED to ADD a new ServiceChannel: {name}")
        return True

    def removeChannel(self, chanID: FlowChanID) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            print(f"Channel NOT found: {chanID}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"REQUESTED to REMOVE a channel: {self._channelsIndexes[chanID].name}")
        return True

    def setChannelHeader(self, chanID: FlowChanID, data: bytearray):
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            print(f"Channel NOT found: {chanID}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_CHAN_HEADER)
        self._p.sendChanID(chanID)
        self._p.sendBuffer(data)
        self._p.sendEndOfTransaction()

        print(f"Setup HEADER [{len(data)} B] for a channel: {self._channelsIndexes[chanID].name}");
        return True
    
####################################################

    def attach(self, sourceID: FlowChanID, targetID: FlowChanID) -> bool:
        if sourceID not in self._channelsIndexes:
            print(f"Channel NOT found: {sourceID}")
            return False
        
        if targetID not in self._channelsIndexes:
            print(f"Channel NOT found: {targetID}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)
        self._p.sendChanID(sourceID)
        self._p.sendChanID(targetID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"REQUESTED to ATTACH channels: {self._channelsIndexes[sourceID].name} -> {self._channelsIndexes[targetID].name}")
        return True
    
    def detach(self, sourceID: FlowChanID, targetID: FlowChanID) -> bool:
        if sourceID not in self._channelsIndexes:
            print(f"Channel NOT found: {sourceID}")
            return False
        
        if targetID not in self._channelsIndexes:
            print(f"Channel NOT found: {targetID}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)
        self._p.sendChanID(sourceID)
        self._p.sendChanID(targetID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"REQUESTED to DETACH channels: {self._channelsIndexes[sourceID].name} -> {self._channelsIndexes[targetID].name}")
        return True

####################################################

    def sendServiceRequest(self, chanID: FlowChanID, cmd: str, val) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            print(f"Channel NOT found: {chanID}")
            return False
        
        if self.channelByID(chanID).chan_t != FlowChannel_T.ServiceChannel:
            print(f"Channel is NOT a service: {chanID}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXEC_SERVICE_REQUEST)
        self._p.sendChanID(chanID)
        self._p.sendString(cmd)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"ServiceChannel REQUEST sent: {self._channelsIndexes[chanID].name}")
        return True
    
    def sendServiceResponse(self, chanID: FlowChanID, hash: str, val):
        self._p.sendStartOfTransaction(FlowCommand.FCMD_RETURN_SERVICE_RESPONSE)
        self._p.sendChanID(chanID)
        self._p.sendString(hash)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

####################################################

    def publish(self, chanID: FlowChanID, b: bytearray) -> bool:
        return self._publishPacketizedData(chanID, b)
    
    def publishString(self, chanID: FlowChanID, text: str) -> bool:        
        return self._publishPacketizedData(chanID, text.encode('utf-8'))
    
    def publishJSON(self, chanID: FlowChanID, val) -> bool:
        try:
            text = json.dumps(val)

        except json.JSONEncodeError as e:
            print("Error during JSON encoding:", e)
            return False
        
        return self._publishPacketizedData(chanID, text.encode('utf-8'))

    def publishUInt8(self, chanID: FlowChanID, val: ctypes.c_uint8) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<B", val))

    def publishInt8(self, chanID: FlowChanID, val: ctypes.c_int8) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<b", val))

    def publishUInt16(self, chanID: FlowChanID, val: ctypes.c_uint16) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<H", val))

    def publishInt16(self, chanID: FlowChanID, val: ctypes.c_int16) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<h", val))

    def publishUInt32(self, chanID: FlowChanID, val: ctypes.c_uint32) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<I", val))

    def publishInt32(self, chanID: FlowChanID, val: ctypes.c_int32) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<i", val))

    def publishUInt64(self, chanID: FlowChanID, val: ctypes.c_uint64) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<Q", val))

    def publishInt64(self, chanID: FlowChanID, val: ctypes.c_int64) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<q", val))

    def publishFloat(self, chanID: FlowChanID, val: ctypes.c_float) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<f", val))

    def publishDouble(self, chanID: FlowChanID, val: ctypes.c_double) -> bool:
        return self._publishPacketizedData(chanID, struct.pack("<d", val))

####################################################

    def _publishPacketizedData(self, chanID: FlowChanID, data: bytearray) -> bool:
        if (self._sck is None) or (not self._sck.isConnected()):
            print("SocketDecice is NOT initialized yet")
            return False
        
        # NO RESPONSE
        self._p.sendStartOfTransaction(FlowCommand.FCMD_PUBLISH)
        self._p.sendChanID(chanID)
        self._p.sendBuffer(data)
        return self._p.sendEndOfTransaction()
        
####################################################

    def isSubscribed(self, chanID: FlowChanID) -> bool:
        return (chanID in self._subscribedChannels)

    def subscribeChannel(self, chanID: FlowChanID) -> bool:
        if FlowChanID in self._subscribedChannels:
            print(f"Channel is ALREADY subscribed: {self._channelsIndexes[chanID].name}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SUBSCRIBE_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        print(f"Sent SUBSCRIBE for a channel: {self._channelsIndexes[chanID].name}")
        return True

    def unsubscribeChannel(self, chanID: FlowChanID) -> bool:
        if FlowChanID in self._subscribedChannels:
            print(f"Channel is ALREADY subscribed: {self._channelsIndexes[chanID].name}")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_UNSUBSCRIBE_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()
        
        # NO RESPONSE
        print(f"Sent UNSUBSCRIBE for a channel: {self._channelsIndexes[chanID].nam}")
        return True

    def hasNextData(self) -> bool:
        return len(self._channelsData) > 0

    def nextData(self) -> bool:
        if not self.hasNextData():
            return False
        
        self._currentData = self._channelsData.popleft()
        return True

    def getCurrentData(self) -> FlowChannelData:
        return self._currentData
    
####################################################

    def getCurrentDbName(self) -> str:
        return self._currentDbName

####################################################

    def _onOpenUnixSocket(self) -> bool:
        return True

    def _onOpenTcpSocket(self) -> bool:
        return True
    
    def _onLogin(self) -> bool:
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_ASYNC)
        self._p.sendEndOfTransaction();

        # NO RESPONSE
        return True

    def _onOpen(self) -> None:
        pass

    def _onClose(self) -> None:
        pass

    def _onDisconnected(self) -> None:
        self._subscribedChannels = []
        self._channelsData = deque()
        self._currentData = FlowChannelData()
        self._lastResponse = FlowResponse.FRSP_NORSP

    def _onTick(self) -> None:
        while self._sck.bytesAvailable() > 0:
            sz = self._sck.bytesAvailable()
            self._analyze()
            if sz == self._sck.bytesAvailable():
                break
    
        if self._onGrabDataCb is not None:
            while self.nextData():
                currentData = self.getCurrentData()
                self._onGrabDataCb(currentData.chanID, currentData.data)

####################################################

    def _analyze(self) -> None:
        if self._lastResponse == FlowResponse.FRSP_NORSP and self._sck.bytesAvailable() > ctypes.sizeof(ctypes.c_int16):
            self._lastResponse = self._p.recvStartOfRspTransaction()
        
        ##

        if self._lastResponse == FlowResponse.FRSP_CHK_FLOW:
            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
        
            self._lastResponse = FlowResponse.FRSP_NORSP

        ##

        elif self._lastResponse == FlowResponse.FRSP_CURRENTDB_CHANGED:
            sz: ctypes.c_uint32 = self._p.recvSize()
            self._currentDbName = self._p.recvString(sz)

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return

            self._lastResponse = FlowResponse.FRSP_NORSP

        ##

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_ADDED:
            ch: FlowChannel = FlowChannel()
            ch.isPublishingEnabled = False

            ch.chan_t = self._p.recvChanType()
            ch.chanID = self._p.recvChanID()

            sz: ctypes.c_uint32 = self._p.recvSize()
            ch.name = self._p.recvString(sz)

            sz = self._p.recvSize()
            ch.hashID = self._p.recvString(sz)

            if ch.chanID in self._channelsIndexes:
                ProtocolRecvError(self._sck, f"Channel is ALREADY stored: {ch.name} [ChanID: {ch.chanID}]")
                return
            
            if ch.chan_t == FlowChannel_T.StreamingChannel:
                sz = self._p.recvSize()
                ch.udm = self._p.recvString(sz)

                sz = self._p.recvSize()
                ch.mime = self._p.recvString(sz)

                ch.t = self._p.recvVariantType()

                sz = self._p.recvSize()
                ch.min = self._p.recvJSON(sz)

                sz = self._p.recvSize()
                ch.max = self._p.recvJSON(sz)

                sz = self._p.recvSize()
                ch.hasHeader = self._p.recvJSON(sz)

                if ch.hasHeader:
                    sz = self._p.recvSize()
                    ch.header = self._p.recvBuffer(sz)

                sz = self._p.recvSize()
                ch.mpPath = self._p.recvString(sz)

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            self._channelsNames[ch.name] = ch
            #print(type(chanID))
            self._channelsIndexes[ch.chanID] = ch

            '''
            print("ADDED channel: [ID: {}; name: {}; val_T: {}; mime_T: {}; udm: {}; http: {}; hashID: {}]"
                    .format(ch.chanID, ch.name, ch.t.name, ch.mime, ch.udm, ch.mpPath, ch.hashID))
            '''

            if self._onNewChanCb is not None:
                self._onNewChanCb(ch)

            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_REMOVED:
            chanID: FlowChanID = self._p.recvChanID()

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            if chanID not in self._channelsIndexes:
                print("Channel is NOT stored: [ChanID: {}]".format(chanID))
                return
            
            ch: FlowChannel = self._channelsIndexes[chanID]

            if chanID in self._subscribedChannels:
                print(f"AUTOMATIC UNSUBSCRIBE for a removed channel: [ChanID: {ch.chanID}; name: {ch.name}]")
                self._subscribedChannels.remove(chanID)

            '''
            print("REMOVED channel: [ID: {}; name: {}; hashID: {}]" .format(ch.chanID, ch.name, ch.hashID))
            '''

            del self._channelsNames[ch.name]
            del self._channelsIndexes[ch.chanID]

            if self._onDelChanCb is not None:
                self._onDelChanCb(ch)

            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_HEADER:
            chanID: FlowChanID = self._p.recvChanID()

            if chanID not in self._channelsIndexes:
                print("Channel is NOT stored: [ChanID: {}]".format(chanID))
                return
            
            ch: FlowChannel = self._channelsIndexes[chanID]

            sz = self._p.recvSize()
            ch.header = self._p.recvBuffer(sz)
            ch.hasHeader = (len(ch.header) > 0)

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_SEND_RESPONSE_TO_REQUESTER:
            sz: ctypes.c_uint32 = self._p.recvSize()
            val = self._p.recvJSON(sz)

            #print(f"!!!!!!! {val}")

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_SEND_REQUEST_TO_SERVICE:
            chanID: FlowChanID = self._p.recvChanID()

            sz: ctypes.c_uint32 = self._p.recvSize()
            hash = self._p.recvString(sz)

            sz = self._p.recvSize()
            cmdName = self._p.recvString(sz)

            sz = self._p.recvSize()
            val = self._p.recvJSON(sz)

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            if self._onServiceRequest is not None:
                self._onServiceRequest(chanID, hash, cmdName, val)
                
            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_START:
            chanID: FlowChanID = self._p.recvChanID()

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return

            if chanID not in self._channelsIndexes:
                print(f"Channel is NOT stored: [ChanID: {chanID}]")
                return
            
            ch: FlowChannel = self._channelsIndexes[chanID]
            ch.isPublishingEnabled = True

            if self._onStartChanPubReqCb is not None:
                self._onStartChanPubReqCb(ch)
            '''
            print("Server has requested for channel-publish START: [ID: {}; name: {}]" .format(ch.chanID, ch.name))
            '''

            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_STOP:
            chanID: FlowChanID = self._p.recvChanID()

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return

            if chanID not in self._channelsIndexes:
                print(f"Channel is NOT stored: [ChanID: {chanID}]")
                return
            
            ch: FlowChannel = self._channelsIndexes[chanID]
            ch.isPublishingEnabled = False

            if self._onStopChanPubReqCb is not None:
                self._onStopChanPubReqCb(ch)
            '''
            print("Server has requested for channel-publish STOP: [ID: {}; name: {}]" .format(ch.chanID, ch.name))
            '''
            self._lastResponse = FlowResponse.FRSP_NORSP

        elif self._lastResponse == FlowResponse.FRSP_SUBSCRIBED_DATA:
            chanID: FlowChanID = self._p.recvChanID()
            
            sz = self._p.recvSize()

            if sz == 0:
                print("Receiving subscribed data size is ZERO")
                return

            flowData = FlowChannelData()
            flowData.chanID = chanID
            flowData.data = self._p.recvBuffer(sz)

            if not self._p.recvEndOfTransaction():
                ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
                return
            
            self._channelsData.append(flowData)

            self._lastResponse = FlowResponse.FRSP_NORSP

####################################################

