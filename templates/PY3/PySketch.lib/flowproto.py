
import ctypes
import json

from enum import  Enum, auto
from socketdevice import SocketDevice
from typing import NewType, Any

FlowChanID = NewType('FlowChanID', ctypes.c_int16)

class FlowChannel_T(Enum):
    ChannelNotValid = 0
    StreamingChannel = auto()
    ServiceChannel = auto()

class Variant_T(Enum):
    T_NULL = 0
    T_BOOL = auto()
    T_INT8 = auto()
    T_UINT8 = auto()
    T_INT16 = auto()
    T_UINT16 = auto()
    T_INT32 = auto()
    T_UINT32 = auto()
    T_FLOAT = auto()
    T_INT64 = auto()
    T_UINT64 = auto()
    T_DOUBLE = auto()
    T_BYTEARRAY = auto()
    T_STRING = auto()
    T_LIST = auto()
    T_PAIR = auto()
    T_MAP = auto()
    T_RAWPOINTER = auto()

class FlowCommand(Enum):
    FCMD_NOCMD = 0

    FCMD_LOGIN = auto()                     # ONLY SYNC (ASYNC STARTs AS SYNC)

    FCMD_SET_ASYNC = auto()                 # ONLY SYNC
    FCMD_CHK_SERVICE = auto()               # ONLY ASYNC

    FCMD_GET_CHANS_LIST = auto()            # ONLY SYNC
    FCMD_GET_CHAN_PROPS = auto()            # ONLY SYNC

    #FCMD_GET_CHAN_HEADER = auto()           # ONLY SYNC
    FCMD_SET_CHAN_HEADER = auto()           # ONLY ASYNC

    FCMD_EXISTS_DATABASE = auto()           # ONLY SYNC
    FCMD_ADD_DATABASE = auto()              # *
    FCMD_SET_DATABASE = auto()              # *
    FCMD_GET_DATABASE = auto()              # ONLY SYNC
    FCMD_GET_VARIABLES_KEYS = auto()        # ONLY SYNC
    FCMD_GET_VARIABLES = auto()             # ONLY SYNC
    FCMD_GET_VARIABLES_JSON = auto()        # ONLY SYNC
    FCMD_EXISTS_VARIABLE = auto()           # ONLY SYNC
    FCMD_SET_VARIABLE = auto()              # *
    FCMD_SET_VARIABLE_JSON = auto()         # *
    FCMD_GET_VARIABLE = auto()              # ONLY SYNC
    FCMD_GET_VARIABLE_JSON = auto()         # ONLY SYNC
    FCMD_DEL_VARIABLE = auto()              # *
    FCMD_FLUSHALL = auto()                  #

    FCMD_ADD_STREAMING_CHAN = auto()        # ONLY ASYNC
    FCMD_ADD_SERVICE_CHAN = auto()          # ONLY ASYNC
    FCMD_DEL_CHAN = auto()                  # ONLY ASYNC

    FCMD_ATTACH_CHAN = auto()               # ONLY ASYNC
    FCMD_DETACH_CHAN = auto()               # ONLY ASYNC

    FCMD_EXEC_SERVICE_REQUEST = auto()      # *
    FCMD_RETURN_SERVICE_RESPONSE = auto()   # ONLY ASYNC

    FCMD_GRAB_REGISTER_CHAN = auto()        # ONLY SYNC
    FCMD_GRAB_UNREGISTER_CHAN = auto()      # ONLY SYNC
    FCMD_GRAB_LASTDATA_CHAN = auto()        # ONLY SYNC

    FCMD_PUBLISH = auto()                   # ONLY ASYNC

    FCMD_SUBSCRIBE_CHAN = auto()            # ONLY ASYNC
    FCMD_UNSUBSCRIBE_CHAN = auto()          # ONLY ASYNC

    FCMD_QUIT = auto()

class FlowResponse(Enum):
    FRSP_NORSP = 0

    FRSP_CHK_FLOW = auto()

    # SENT ONLY TO THE CONNECTION THAT CHANGED DB
    FRSP_CURRENTDB_CHANGED = auto()

    # SENT ONLY TO ALL
    FRSP_CHANNEL_ADDED = auto()
    FRSP_CHANNEL_REMOVED = auto()
    FRSP_CHANNEL_HEADER = auto()

    # SENT ONLY TO THE SERVICE
    FRSP_SEND_REQUEST_TO_SERVICE = auto()
    
    # SENT ONLY TO THE REQUESTER
    FRSP_SEND_RESPONSE_TO_REQUESTER = auto()

    # SENT ONLY TO THE CHANNEL-OWNER
    FRSP_CHANNEL_PUBLISH_START = auto()
    FRSP_CHANNEL_PUBLISH_STOP = auto()

    # SENT ONLY TO ALL SUBSCRIBERS
    FRSP_SUBSCRIBED_DATA = auto()

    FRSP_OK = auto()
    FRSP_KO = auto()

class FlowProto:
    def __init__(self) -> None:
        self._currentSendCmd = FlowCommand.FCMD_NOCMD
        self._currentSendRsp = FlowResponse.FRSP_NORSP
        self._currentRecvCmd = FlowCommand.FCMD_NOCMD
        self._currentRecvRsp = FlowResponse.FRSP_NORSP
        self._buffer = bytearray()
        self._sck = None
    
    def setup(self, sck: SocketDevice) -> None:
        self._sck = sck

####################################################
# SEND (buffered)

    def sendStartOfTransaction(self, cmd: FlowCommand) -> bool:
        if self._buffer:
            print(f"CMD transaction ALREADY open [sending]: {self.commandToString(self._currentSendCmd)}")
            return False
        
        self._currentSendCmd = cmd
        self._buffer += cmd.value.to_bytes(2, byteorder='little')

        return True

    def sendStartOfTransaction(self, rsp: FlowResponse) -> bool:
        if self._buffer:
            print(f"RSP transaction ALREADY open [sending]: {self.responseToString(self._currentSendRsp)}")
            return False
        
        self._currentSendRsp = rsp
        self._buffer += rsp.value.to_bytes(2, byteorder='little')

        return True
    
    def sendChanType(self, t: FlowChannel_T) -> None:
        self._buffer += t.value.to_bytes(1, byteorder='little')
    
    def sendVariantType(self, t: Variant_T) -> None:
        self._buffer += t.value.to_bytes(2, byteorder='little')
    
    def sendChanID(self, chanID: FlowChanID) -> None:
        self._buffer += chanID.to_bytes(2, byteorder='little')
    
    def sendSize(self, sz: ctypes.c_uint32) -> None:
        self._buffer += sz.to_bytes(4, byteorder='little')
    
    def sendBool(self, v: bool) -> None:
        self._buffer += v.to_bytes(1, byteorder='little')
    
    def sendString(self, text: str) -> None:
        sz = len(text)
        self.sendSize(sz)

        if sz == 0:
            return
        
        self._buffer += text.encode('utf-8')
    
    def sendJSON(self, val) -> None:
        self.sendString(json.dumps(val))
    
    def sendBuffer(self, b: bytearray) -> None:
        sz = len(b)
        self.sendSize(sz)

        if sz == 0:
            return
        
        self._buffer += b
    
    def sendEndOfList(self) -> None:
        val = 0
        self._buffer += val.to_bytes(4, byteorder='little')
    
    def sendEndOfMap(self) -> None:
        val = 0
        self._buffer += val.to_bytes(4, byteorder='little')

    def sendEndOfTransaction(self) -> bool:
        isConnected = self._sck.isConnected()
        canWrite = self._sck.canWrite()

        if not isConnected or not canWrite:
            print(f"CANNOT send protocol-transaction [isOpen: {isConnected}; canWrite: {canWrite}]")
            return False
        
        val = 0
        self._buffer += val.to_bytes(4, byteorder='little')
        
        ok = self._sck.write(self._buffer)
        self._buffer.clear()

        self._currentSendCmd = FlowCommand.FCMD_NOCMD
        self._currentSendRsp = FlowResponse.FRSP_NORSP

        return ok



####################################################
# RECV (unbuffered)

    def recvStartOfCmdTransaction(self) -> FlowCommand:
        if self._currentRecvCmd != FlowCommand.FCMD_NOCMD:
            print(f"CMD transaction ALREADY open [receiving]: {self.commandToString(self._currentRecvCmd)}")
            return FlowCommand.FCMD_NOCMD
        
        cmd = FlowCommand(self._sck.readUInt16())
        self._currentRecvCmd = cmd
        return cmd
    
    def recvStartOfRspTransaction(self) -> FlowResponse:
        if self._currentRecvRsp != FlowResponse.FRSP_NORSP:
            print(f"RSP transaction ALREADY open [receiving]: {self.commandToString(self._currentRecvRsp)}")
            return FlowResponse.FRSP_NORSP
        
        rsp = FlowResponse(self._sck.readUInt16())
        self._currentRecvRsp = rsp
        return rsp

    def recvChanType(self) -> FlowChannel_T:
        return FlowChannel_T(self._sck.readUInt8())

    def recvVariantType(self) -> Variant_T:
        return Variant_T(self._sck.readUInt16())

    def recvChanID(self) -> FlowChanID:
        return self._sck.readInt16()

    def recvSize(self) -> ctypes.c_uint32:
        return self._sck.readUInt32()

    def recvBool(self) -> bool:
        return bool(self._sck.readUInt8())

    def recvString(self, sz: ctypes.c_uint32) -> str:
        if sz == 0:
            return ""
        
        return self._sck.readString(sz)
    
    def recvJSON(self, sz) -> Any:
        text = self.recvString(sz)

        if len(text) == 0:
            return None
    
        try:
            return json.loads(text)

        except json.JSONDecodeError as e:
            print("Error during JSON decoding:", e)

    def recvBuffer(self, sz: ctypes.c_uint32) -> bytearray:
        if sz == 0:
            return ""
        
        return self._sck.read(sz)
    
    def recvList(self) -> list:
        sz: ctypes.c_uint32 = 0
        l = []

        while True:
            sz = self.recvSize()

            if sz > 0:
                item: str = self.recvString(sz)

                if not item:
                    return l
                
                l.append(item)
            
            else:
                return l                

    def recvEndOfTransaction(self) -> bool:
        ret = self._sck.readUInt32()

        if ret != 0:
            print("FAILED to RECV EndOfTransaction [CurrentRsp: {}; ret: {}!=0]".format(self.commandToString(self._currentRecvCmd), ret))
            return False

        self._currentRecvCmd = FlowCommand.FCMD_NOCMD
        self._currentRecvRsp = FlowResponse.FRSP_NORSP

        return True

####################################################
# REQ

####################################################
# ANSWER

####################################################

    def commandToString(self, cmd: FlowCommand) -> str:
        return cmd.name
    
    def commandToBin(cmd : str) -> FlowCommand:
        return FlowCommand[str]

    def responseToString(self, rsp: FlowResponse) -> str:
        return rsp.name
    
    def responseToBin(cmd : str) -> FlowResponse:
        return FlowResponse[str]
