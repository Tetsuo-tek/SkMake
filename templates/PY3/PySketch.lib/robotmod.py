from flowasync import FlowAsync
from flowsync import FlowSync

import platform

####################################################

class RobotModule(FlowAsync):
    def __init__(self) -> None:
        super().__init__()
        self._userName = "User1"
        self._passwd = "password"
        self._address = "127.0.0.1"
        #self._address = "192.168.2.51"
        self._port = 9000
        
        print("Operating System:", platform.system())
        print("OS Release:", platform.release())
        print("OS Version:", platform.version())
        print("Machine:", platform.machine())
        print("Processor Architecture:", platform.architecture())
        print("Network Name:", platform.node())
        print("Python Version:", platform.python_version())

####################################################

    def newSyncClient(self) -> FlowSync:
        sync = FlowSync()
        ok: bool = False

        if self._port == 0:
            #print(f"Connecting to local-unix FlowService [SYNC]: {self._address}")
            ok = sync.localConnect(self._address)
        
        else:
            #print(f"Connecting to tcp/ip FlowService [SYNC]: {self._address}:{self._port}")
            ok = sync.tcpConnect(self._address, self._port)
        
        if ok:
            ok = sync.login(self._userName, self._passwd)

        if not ok:
            return FlowSync()
        
        else:
            sync.setCurrentDbName("Main")

        return sync
    
####################################################

    def connect(self) -> bool:
        ok = self.tcpConnect(self._address, self._port)
        
        if ok:
            print(f"Trying to login: {self._userName}")
            ok = self.login(self._userName, self._passwd)
        
        if ok:
            self.setCurrentDbName("Main")

        return ok
    
    def disconnect(self) -> None:
        if self.isConnected():
            self.close()

####################################################
